# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file 'mainwindow.ui'
#
# Created by: PyQt5 UI code generator 5.10
#
# WARNING! All changes made in this file will be lost!

from PyQt5 import QtCore, QtGui, QtWidgets
import pyqtgraph as pg
import numpy as np
import json
import pgLinearRegionItem
import pgInfiniteLine
import os

class Ui_MainWindow(object):
    def setupUi(self, MainWindow):
        with open('config.json', 'r', encoding='utf-8-sig') as reader:
            self.config = json.loads(reader.read())

        MainWindow.setObjectName("MainWindow")
        MainWindow.resize(1200, 800)
        # MainWindow.setGeometry(10,100,1200,700)

        self.centralWidget = QtWidgets.QWidget(MainWindow)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Maximum, QtWidgets.QSizePolicy.Maximum)
        sizePolicy.setHorizontalStretch(1)
        sizePolicy.setVerticalStretch(1)
        # sizePolicy.setHeightForWidth(self.centralWidget.sizePolicy().hasHeightForWidth())
        self.centralWidget.setSizePolicy(sizePolicy)
        self.centralWidget.setObjectName("centralWidget")

        self.MainLayout = QtWidgets.QGridLayout(self.centralWidget)
        self.MainLayout.setContentsMargins(5, 5, 5, 5)
        self.MainLayout.setSpacing(5)
        self.MainLayout.setObjectName("MainLayout")

        # ======= Layout of player
        self.playerLayout = QtWidgets.QVBoxLayout()
        self.playerLayout.setSpacing(6)
        self.playerLayout.setObjectName("playerLayout")
        # self.playerLayout.setGeometry(QtCore.QRect(0,0,300,500))
        self.playerLayout.setSizeConstraint(QtWidgets.QLayout.SetFixedSize)        
        if self.config['SmallerScreen']:
            print('go to "Smaller Screen" mode')
            playerWidth = 230
        else:
            playerWidth = self.config['playerwidth']

        self.clrPlayListBtn = QtWidgets.QPushButton('Clear List')
        self.clrCurrentSongBtn = QtWidgets.QPushButton('Clear This')
        clrSongLayout = QtWidgets.QHBoxLayout()
        clrSongLayout.addWidget(self.clrPlayListBtn)
        clrSongLayout.addWidget(self.clrCurrentSongBtn)
        # self.playerLayout.addWidget(self.clrPlayListBtn)
        self.playerLayout.addLayout(clrSongLayout)

        self.playlistView = QtWidgets.QListView(self.centralWidget)
        self.playlistView.setAcceptDrops(True)
        self.playlistView.setProperty("showDropIndicator", True)
        self.playlistView.setDragDropMode(QtWidgets.QAbstractItemView.DropOnly)
        self.playlistView.setAlternatingRowColors(True)
        self.playlistView.setUniformItemSizes(True)
        self.playlistView.setObjectName("playlistView")
        # self.playlistView.setMaximumWidth(400)
        self.playlistView.setFixedWidth(playerWidth)
        if self.config['SmallerScreen']:
            self.playlistView.setMinimumHeight(230)
        else:
            self.playlistView.setMinimumHeight(330)
        self.playerLayout.addWidget(self.playlistView)

        self.timerclipGrp = QtWidgets.QGroupBox('Set Tstart')
        self.timerclipGrp.setFixedWidth(playerWidth)
        timerclipLayout = QtWidgets.QVBoxLayout()
        timerlayout1 = QtWidgets.QHBoxLayout()
        # ti hour
        tihourLabel = QtWidgets.QLabel('h')
        # if not self.config['SmallerScreen']:
        timerlayout1.addWidget(tihourLabel)
        self.Set_ti_hour = QtWidgets.QSpinBox()
        self.Set_ti_hour.setSingleStep(1)
        self.Set_ti_hour.setValue(0)
        self.Set_ti_hour.setRange(0, 99)
        # if not self.config['SmallerScreen']:
        timerlayout1.addWidget(self.Set_ti_hour)
        # ti mininute
        timinLabel = QtWidgets.QLabel('m')
        timerlayout1.addWidget(timinLabel)
        self.Set_ti_min = QtWidgets.QSpinBox()
        self.Set_ti_min.setSingleStep(1)
        self.Set_ti_min.setValue(0)
        self.Set_ti_min.setRange(0, 999)
        timerlayout1.addWidget(self.Set_ti_min)
        # ti sec
        timinLabel = QtWidgets.QLabel('s')
        timerlayout1.addWidget(timinLabel)
        self.Set_ti_sec = QtWidgets.QDoubleSpinBox()
        self.Set_ti_sec.setSingleStep(1)
        self.Set_ti_sec.setValue(0)
        self.Set_ti_sec.setDecimals(3)
        self.Set_ti_sec.setRange(0, 99999)
        timerlayout1.addWidget(self.Set_ti_sec)
        timerclipLayout.addLayout(timerlayout1)
        # # spacer
        # setTstart_spacer0 = QtWidgets.QSpacerItem(10,20)
        # timerclipLayout.addItem(setTstart_spacer0,0,8,1,1)
        timerlayout2 = QtWidgets.QHBoxLayout()
        # go backward a little step
        self.stepback = QtWidgets.QPushButton()
        icon4_3 = QtGui.QIcon()
        icon4_3.addPixmap(QtGui.QPixmap("images/previous_outline_16x19.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.stepback.setIcon(icon4_3)
        self.stepback.setFixedWidth(50)
        if self.config['SmallerScreen']:
            self.stepback.setFixedHeight(10)
        timerlayout2.addWidget(self.stepback)
        # go forward a little step
        self.stepfor = QtWidgets.QPushButton()
        icon4_4 = QtGui.QIcon()
        icon4_4.addPixmap(QtGui.QPixmap("images/next_outline_16x19.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.stepfor.setIcon(icon4_4)
        self.stepfor.setFixedWidth(50)
        if self.config['SmallerScreen']:
            self.stepfor.setFixedHeight(10)
        timerlayout2.addWidget(self.stepfor)
        # # spacer
        # setTstart_spacer1 = QtWidgets.QSpacerItem(10,10)
        # timerlayout2.addItem(setTstart_spacer1,1,4,1,1)
        # go previous page
        self.PreviousSeg = QtWidgets.QPushButton()
        # self.PreviousSeg.setFixedWidth(150)
        icon4_2 = QtGui.QIcon()
        icon4_2.addPixmap(QtGui.QPixmap("images/control-skip-180.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.PreviousSeg.setIcon(icon4_2)
        self.PreviousSeg.setFixedWidth(50)
        if self.config['SmallerScreen']:
            self.PreviousSeg.setFixedHeight(10)
        timerlayout2.addWidget(self.PreviousSeg)
        # go next seg
        self.NextSeg = QtWidgets.QPushButton()
        icon4_1 = QtGui.QIcon()
        icon4_1.addPixmap(QtGui.QPixmap("images/control-skip.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.NextSeg.setIcon(icon4_1)
        self.NextSeg.setFixedWidth(50)
        if self.config['SmallerScreen']:
            self.NextSeg.setFixedHeight(10)
        timerlayout2.addWidget(self.NextSeg)
        timerclipLayout.addLayout(timerlayout2)
        # # spacer
        # setTstart_spacer1 = QtWidgets.QSpacerItem(50,20)
        # timerclipLayout.addItem(setTstart_spacer1,1,8,1,1)
        # set duration
        durationcliplayout = QtWidgets.QHBoxLayout()
        durationlabel = QtWidgets.QLabel('duration(s)')
        durationlabel.setFixedWidth(61)
        durationcliplayout.addWidget(durationlabel)
        self.Set_duration = QtWidgets.QSpinBox()
        self.Set_duration.setSingleStep(5)
        self.Set_duration.setValue(15)
        self.Set_duration.setRange(1, 99999)
        self.Set_duration.setFixedWidth(55)
        durationcliplayout.addWidget(self.Set_duration)
        # clipWav
        self.SoundClipBtn = QtWidgets.QPushButton('ClipWAV')
        # self.SoundClipBtn.setFixedWidth(130)
        durationcliplayout.addWidget(self.SoundClipBtn)
        timerclipLayout.addLayout(durationcliplayout)

        self.timerclipGrp.setLayout(timerclipLayout)
        self.playerLayout.addWidget(self.timerclipGrp)

        # ====== Play Refresh Sound
        refreshSndGrpBox = QtWidgets.QGroupBox('Play Refresh Sound')
        refreshSndGrpBox.setFixedWidth(playerWidth)
        # print(refreshSndGrpBox.contentsMargins())
        # print(refreshSndGrpBox.getContentsMargins())
        refreshSndGrpBox.setContentsMargins(0, 0, 0, 0)
        refreshSndLayout = QtWidgets.QGridLayout()
        self.refreshMode = QtWidgets.QCheckBox('Refresh Mode')
        # self.refreshMode.setChecked(True)
        self.RefreshButton1 = QtWidgets.QPushButton()
        self.RefreshButton1.setText("NormalVesicular")
        self.RefreshButton2 = QtWidgets.QPushButton()
        self.RefreshButton2.setText("NormalTracheal")
        self.RefreshButton3 = QtWidgets.QPushButton()
        self.RefreshButton3.setText("Crackle")
        self.RefreshButton4 = QtWidgets.QPushButton()
        self.RefreshButton4.setText("Rhonchi")
        self.RefreshButton5 = QtWidgets.QPushButton()
        self.RefreshButton5.setText("Stridor")
        self.RefreshButton6 = QtWidgets.QPushButton()
        self.RefreshButton6.setText("Wheeze")
        refreshSndLayout.addWidget(self.refreshMode,0,0,1,2)
        refreshSndLayout.addWidget(self.RefreshButton1,1,0)
        refreshSndLayout.addWidget(self.RefreshButton2,1,1)
        refreshSndLayout.addWidget(self.RefreshButton3,2,0)
        refreshSndLayout.addWidget(self.RefreshButton4,2,1)
        refreshSndLayout.addWidget(self.RefreshButton5,3,0)
        refreshSndLayout.addWidget(self.RefreshButton6,3,1)
        refreshSndGrpBox.setLayout(refreshSndLayout)
        self.playerLayout.addWidget(refreshSndGrpBox)
        
        self.playSndGrpBox = QtWidgets.QGroupBox("00:00:00")
        self.playSndGrpBox.setFixedWidth(playerWidth)
        playSndLayout = QtWidgets.QGridLayout()
        self.previousButton = QtWidgets.QPushButton(self.centralWidget)
        self.previousButton.setText("")
        self.previousButton.setIcon(icon4_2)
        self.previousButton.setObjectName("previousButton")
        self.previousButton.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.previousButton.setToolTip("Previous Song")
        playSndLayout.addWidget(self.previousButton,0,0)
        # self.horizontalLayout_4.addWidget(self.previousButton)
        # self.currentTimeLabel = QtWidgets.QLabel(self.centralWidget)
        # self.currentTimeLabel.setMinimumSize(QtCore.QSize(30, 0))
        # self.currentTimeLabel.setFixedSize(QtCore.QSize(50, 20))
        # self.currentTimeLabel.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        # self.currentTimeLabel.setObjectName("currentTimeLabel")
        # playSndLayout.addWidget(self.currentTimeLabel,0,1)
        # self.horizontalLayout_4.addWidget(self.currentTimeLabel)
        # self.timeSlider = QtWidgets.QSlider(self.centralWidget)
        # self.timeSlider.setOrientation(QtCore.Qt.Horizontal)
        # self.timeSlider.setObjectName("timeSlider")
        # self.timeSlider.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        # self.timeSlider.setMaximumWidth(380)
        # self.timeSlider.setFixedWidth(150)
        # self.timeSlider.setMinimumWidth(120)
        # self.horizontalLayout_4.addWidget(self.timeSlider)
        # playSndLayout.addWidget(self.timeSlider,0,2,1,3)
        # self.totalTimeLabel = QtWidgets.QLabel(self.centralWidget)
        # self.totalTimeLabel.setMinimumSize(QtCore.QSize(20, 0))
        # self.totalTimeLabel.setFixedSize(QtCore.QSize(100, 20))
        # self.totalTimeLabel.setAlignment(QtCore.Qt.AlignLeading|QtCore.Qt.AlignLeft|QtCore.Qt.AlignVCenter)
        # self.totalTimeLabel.setObjectName("totalTimeLabel")
        # playSndLayout.addWidget(self.totalTimeLabel,0,5)
 
        self.nextButton = QtWidgets.QPushButton(self.centralWidget)
        self.nextButton.setText("")
        self.nextButton.setIcon(icon4_1)
        self.nextButton.setObjectName("nextButton")
        self.nextButton.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.nextButton.setToolTip("Next Song")
        playSndLayout.addWidget(self.nextButton,0,1)
        # self.horizontalLayout_5.addWidget(self.nextButton)      
        self.playButton = QtWidgets.QPushButton(self.centralWidget)
        self.playButton.setText("")
        icon1 = QtGui.QIcon()
        icon1.addPixmap(QtGui.QPixmap("images/control.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.playButton.setIcon(icon1)
        self.playButton.setObjectName("playButton")
        self.playButton.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        self.playButton.setToolTip("Play from current position to end")
        playSndLayout.addWidget(self.playButton,0,2)
        # self.horizontalLayout_5.addWidget(self.playButton)        
        self.playZoomButton = QtWidgets.QPushButton(self.centralWidget)
        self.playZoomButton.setText("")
        icon1_1 = QtGui.QIcon()
        icon1_1.addPixmap(QtGui.QPixmap("images/control_zoom.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.playZoomButton.setIcon(icon1_1)
        self.playZoomButton.setObjectName("playZoomButton")
        self.playZoomButton.setToolTip("Play ROI +/- 0.5s repeatedly")
        self.playZoomButton.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        playSndLayout.addWidget(self.playZoomButton,0,3)
        # self.horizontalLayout_5.addWidget(self.playZoomButton)
        self.playZoomROIButton = QtWidgets.QPushButton(self.centralWidget)
        self.playZoomROIButton.setText("")
        icon1_2 = QtGui.QIcon()
        icon1_2.addPixmap(QtGui.QPixmap("images/control_zoomROI.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.playZoomROIButton.setIcon(icon1_2)
        self.playZoomROIButton.setObjectName("playZoomROIButton")
        self.playZoomROIButton.setToolTip("Play ROI repeatedly")
        self.playZoomROIButton.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        playSndLayout.addWidget(self.playZoomROIButton,0,4)
        # self.horizontalLayout_5.addWidget(self.playZoomROIButton)
        self.pauseButton = QtWidgets.QPushButton(self.centralWidget)
        self.pauseButton.setText("")
        icon2 = QtGui.QIcon()
        icon2.addPixmap(QtGui.QPixmap("images/control-pause.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        self.pauseButton.setIcon(icon2)
        self.pauseButton.setObjectName("pauseButton")
        self.pauseButton.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        playSndLayout.addWidget(self.pauseButton,0,5)
        # self.stopButton = QtWidgets.QPushButton(self.centralWidget)
        # self.stopButton.setText("")
        # icon3 = QtGui.QIcon()
        # icon3.addPixmap(QtGui.QPixmap("images/control-stop-square.png"), QtGui.QIcon.Normal, QtGui.QIcon.Off)
        # self.stopButton.setIcon(icon3)
        # self.stopButton.setObjectName("stopButton")
        # self.stopButton.setSizePolicy(QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Fixed)
        # playSndLayout.addWidget(self.stopButton,1,5)
        
        self.playSndGrpBox.setLayout(playSndLayout)
        self.playerLayout.addWidget(self.playSndGrpBox) 
        self.MainLayout.addLayout(self.playerLayout,0,2)

        # ====== Tag/Display Information: project name, tagger's name, sitePhoto, tagging action
        # tagInfolayout = QtGui.QGridLayout()
        tagInfolayout = QtGui.QVBoxLayout()
        # tagInfolayout.setHorizontalSpacing(3)
        # row_tagInfo = 0
        taginfowidth = 220
        
        # ----- Group of Display Setting of Spectrogram
        SpectCtrlGrpBox = QtWidgets.QGroupBox("Spectrogram/Waveform Setting")
        SpectCtrlGrpBox.setFixedWidth(taginfowidth)        
        SpectCtrlGrpBox.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)

        SpectCtrlLayout = QtGui.QGridLayout()
        cnt = 0
        # --- Spectrogram nfft
        SpectnfftLabel = QtWidgets.QLabel("nfft:")
        SpectCtrlLayout.addWidget(SpectnfftLabel,cnt,0)
        self.Spectnfftmode = QtWidgets.QComboBox()
        # self.Spectnfftmode.addItems(self.config['SpectroParam']['nfftmode'])
        self.Spectnfftmode.addItems(["fineFreqReso", "balanced", "fineTimeReso"])
        self.Spectnfftmode.setCurrentIndex(1)
        SpectCtrlLayout.addWidget(self.Spectnfftmode,cnt,1,1,2)
        self.Spectnfft = QtWidgets.QComboBox()
        self.Spectnfft.addItems(["32", "64", "128", "256", "512", "1024", "2048", "4096", "8192"])
        self.Spectnfft.setCurrentIndex(2)
        SpectCtrlLayout.addWidget(self.Spectnfft,cnt,3,1,2)        
        cnt += 1
        # --- Spectrogram noverlap
        SpectnoverlapLabel = QtWidgets.QLabel("Time Pixel(AU):")
        if not self.config['SmallerScreen']:
            SpectCtrlLayout.addWidget(SpectnoverlapLabel,cnt,0,1,3)
        self.Spectnoverlap = QtWidgets.QSpinBox()
        self.Spectnoverlap.setRange(1, 99)
        self.Spectnoverlap.setSingleStep(2)
        self.Spectnoverlap.setValue(75)
        if not self.config['SmallerScreen']:
            SpectCtrlLayout.addWidget(self.Spectnoverlap,cnt,3,1,2)
            cnt += 1
        # --- Spectrogram pad_to
        Spectpad_toLabel = QtWidgets.QLabel("Freq. Pixel(AU):")
        if not self.config['SmallerScreen']:
            SpectCtrlLayout.addWidget(Spectpad_toLabel,cnt,0,1,3)
        self.Spectpad_to = QtWidgets.QSpinBox()
        self.Spectpad_to.setRange(10, 900)
        self.Spectpad_to.setSingleStep(20)
        self.Spectpad_to.setValue(100)
        if not self.config['SmallerScreen']:
            SpectCtrlLayout.addWidget(self.Spectpad_to,cnt,3,1,2)
            cnt += 1
        
        if not self.config['SmallerScreen']:
            # --- Spectrogram vmin
            SpectvminLabel = QtWidgets.QLabel("Vmin*10(dB):")
            SpectCtrlLayout.addWidget(SpectvminLabel,cnt,0,1,3)
            self.Spectvmin = QtWidgets.QSpinBox()
            self.Spectvmin.setRange(-200, 0)
            self.Spectvmin.setSingleStep(5)
            self.Spectvmin.setValue(-105)
            SpectCtrlLayout.addWidget(self.Spectvmin,cnt,3,1,2)
            cnt += 1
            # --- Spectrogram vmax
            SpectvminLabel = QtWidgets.QLabel("Vmax*10(dB):")
            SpectCtrlLayout.addWidget(SpectvminLabel,cnt,0,1,3)
            self.Spectvmax = QtWidgets.QSpinBox()
            self.Spectvmax.setRange(-80, 50)
            self.Spectvmax.setSingleStep(5)
            self.Spectvmax.setValue(-15)
            SpectCtrlLayout.addWidget(self.Spectvmax,cnt,3,1,2)
            cnt += 1
        # --- Spectrogram max. freq
        SpectfmaxLabel = QtWidgets.QLabel("freq. max(Hz):")
        SpectCtrlLayout.addWidget(SpectfmaxLabel,cnt,0,1,3)
        self.Spectfmax = QtWidgets.QSpinBox()
        self.Spectfmax.setRange(0, 2000)
        self.Spectfmax.setSingleStep(100)
        self.Spectfmax.setValue(1800)
        SpectCtrlLayout.addWidget(self.Spectfmax,cnt,3,1,2)
        cnt += 1
        # --- transparency of waveform
        WAValphaLabel = QtWidgets.QLabel("wave tranparency:")
        SpectCtrlLayout.addWidget(WAValphaLabel,cnt,0,1,3)
        self.WAValpha = QtWidgets.QSpinBox()
        self.WAValpha.setRange(0, 255)
        self.WAValpha.setSingleStep(5)
        self.WAValpha.setValue(30)
        SpectCtrlLayout.addWidget(self.WAValpha,cnt,3,1,2)
        cnt += 1
        # --- Update Spectrogram
        self.SpectUpdate = QtWidgets.QPushButton("Update")
        self.SpectUpdate.setFixedWidth(80)
        SpectCtrlLayout.addWidget(self.SpectUpdate,cnt,0,1,3)
        # --- restoring default setting @ update
        self.SpectAuto = QtWidgets.QCheckBox("Auto")
        self.SpectAuto.setFixedWidth(100)
        SpectCtrlLayout.addWidget(self.SpectAuto,cnt,3,1,2)
        self.SpectAuto.setChecked(True)
        cnt += 1
        # --- zoom Spectrogram
        zoomrow = QtWidgets.QHBoxLayout()
        self.Spectzoom = QtWidgets.QCheckBox("Zoom around")
        self.Spectzoom.setFixedWidth(120)
        # SpectCtrlLayout.addWidget(self.Spectzoom,cnt,0)
        zoomrow.addWidget(self.Spectzoom)
        self.SpectzoomROI = QtWidgets.QCheckBox("only ROI")
        self.SpectzoomROI.setFixedWidth(80)
        # SpectCtrlLayout.addWidget(self.SpectzoomROI,cnt,3,1,2)
        zoomrow.addWidget(self.SpectzoomROI)
        SpectCtrlLayout.addLayout(zoomrow,cnt,0,1,5)
        cnt += 1

        SpectCtrlGrpBox.setFixedHeight(cnt*30)
        SpectCtrlGrpBox.setLayout(SpectCtrlLayout)
        # tagInfolayout.addWidget(SpectCtrlGrpBox,row_tagInfo,0)
        tagInfolayout.addWidget(SpectCtrlGrpBox)
        # row_tagInfo += 1

        # ------ Trial Info: GroupBox of project title, tagger's name, sitePhoto
        trialInfoGrpBox = QtWidgets.QGroupBox("Trial Infomation")
        trialInfoGrpBox.setFixedWidth(taginfowidth)
        if self.config['SmallerScreen']:
            trialInfoGrpBox.setFixedHeight(80)
        else:
            trialInfoGrpBox.setFixedHeight(180)
        trialInfoGrpBox.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)

        trialInfoLayout = QtGui.QGridLayout()
        # --- Project Name
        self.ProjNameLabel = QtWidgets.QLabel("Project Name:")
        self.ProjNameLabel.setFixedWidth(190)
        trialInfoLayout.addWidget(self.ProjNameLabel,0,0,1,2)
        # self.ProjNameInput = QtWidgets.QLineEdit()
        # self.ProjNameInput.setMinimumWidth(200)
        # --- Tagger Name Label
        TaggerNameLabel = QtWidgets.QLabel("Tagger:")
        TaggerNameLabel.setFixedWidth(50)
        trialInfoLayout.addWidget(TaggerNameLabel,1,0)
        # --- Tagger Name Input
        self.taggerNameInput = QtWidgets.QLineEdit("")
        self.taggerNameInput.setMinimumWidth(150)
        trialInfoLayout.addWidget(self.taggerNameInput,1,1)
        # ------ Evaluation            
        self.judgement1_btn = QtWidgets.QPushButton('Accept') \
                        if self.config['Examining']['goCheckSeverity'] \
                        else QtWidgets.QPushButton('AI XD')
        self.judgement2_btn = QtWidgets.QPushButton('======') \
                        if self.config['Examining']['goCheckSeverity'] \
                        else QtWidgets.QPushButton('Tag XD')
        self.judgement3_btn = QtWidgets.QPushButton('Reject') \
                        if self.config['Examining']['goCheckSeverity'] \
                        else QtWidgets.QPushButton(': )')
        if not self.config['SmallerScreen']:
            # ------ photo of site
            self.sitePhotoLabel = QtWidgets.QLabel()
            self.sitePhotoLabel.setFixedHeight(110)
            self.sitePhotoLabel.setFixedWidth(110)
            self.sitePhotoLabel.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
            self.pixmap = QtGui.QPixmap()
            trialInfoLayout.addWidget(self.sitePhotoLabel,2,0,3,2)
            # ------ Evaluation Result            
            if 'Examining' in self.config.keys():                
                trialInfoLayout.addWidget(self.judgement1_btn,2,2)                
                trialInfoLayout.addWidget(self.judgement2_btn,3,2)                
                trialInfoLayout.addWidget(self.judgement3_btn,4,2)

        trialInfoGrpBox.setLayout(trialInfoLayout)
        # tagInfolayout.addWidget(trialInfoGrpBox,row_tagInfo,0)
        tagInfolayout.addWidget(trialInfoGrpBox)
        # row_tagInfo += 1

        # ------ spaceitem
        self.tagButtonSelect_input = self.config["TagButtons"]
        # if not self.config['SmallerScreen']:            
        #     space1 = QtWidgets.QSpacerItem(100,int(25+0.7*(self.config['plotheight']-600-(len(self.tagButtonSelect_input)-5)*25))) #+(len(self.tagButtonSelect_input)-3)*5)
        # else:
        #     space1 = QtWidgets.QSpacerItem(100,int(25+0.7*(self.config['plotheight']-500-(len(self.tagButtonSelect_input)-3)*70)))
        # tagInfolayout.addItem(space1,row_tagInfo,0)
        # tagInfolayout.addSpacerItem(space1)
        if self.config['spacer1_height_factor']:
            tagInfolayout.addStretch(self.config['spacer1_height_factor'])
            # row_tagInfo += 1

        # ------ GroupBox of Tagging Action
        TagActGrpBox = QtWidgets.QGroupBox("Tagging Action")
        TagActGrpBox.setFixedHeight(90)
        TagActGrpBox.setFixedWidth(taginfowidth)
        TagActGrpBox.setAlignment(QtCore.Qt.AlignLeft|QtCore.Qt.AlignTop)
        TagActLayout = QtGui.QGridLayout()
        TagActLayout.setVerticalSpacing(2)

        self.addmodiTagButton = QtWidgets.QRadioButton("add/modify")        
        self.addmodiTagButton.setChecked(True)
        # self.addmodiTagButton.setFixedHeight(50)
        self.DelTagButton = QtWidgets.QRadioButton("delete")
        # self.DelTagButton.setFixedHeight(50)
        self.ReviewOnlyChkBox = QtWidgets.QCheckBox("Review Only")
        self.ReviewOnlyChkBox.setChecked(True)
        # self.ModiPreviousDataBox = QtWidgets.QCheckBox("Modify Old Data")
        # self.ModiPreviousDataBox.setChecked(False)        
        self.ShowAITagChkBox = QtWidgets.QCheckBox("AI's Tag")
        TagActLayout.addWidget(self.addmodiTagButton,0,0,1,3)
        TagActLayout.addWidget(self.DelTagButton,0,2,1,2)
        TagActLayout.addWidget(self.ReviewOnlyChkBox,1,0,1,3)
        TagActLayout.addWidget(self.ShowAITagChkBox,1,2,1,2)
        # TagActLayout.addWidget(self.ModiPreviousDataBox,1,1,1,2)
        # TagActLayout.addWidget(self.ModifyTagButton,0,2)

        # TagActGrpBox.setFixedHeight(50)
        TagActGrpBox.setLayout(TagActLayout)
        # tagInfolayout.addWidget(TagActGrpBox,row_tagInfo,0)
        tagInfolayout.addWidget(TagActGrpBox)
        # row_tagInfo += 1

        # ------ GroupBox of Tagging Type
        self.TagTypGrpbox = QtWidgets.QGroupBox("Tagging Type")
        # self.TagTypGrpbox.setMinimumHeight(250)
        self.TagTypGrpbox.setFixedHeight(self.config['tagbtngrp_height'])
        # self.TagTypGrpbox.setFixedWidth(230)
        self.TagTypGrpbox.setMaximumWidth(270)
        TagTypLayout = QtGui.QGridLayout()
        # TagTypLayout.setHorizontalSpacing(5)       
        # --- Buttons of Golden, Grade1, Uncertain
        self.GoldenTypButton = QtWidgets.QRadioButton("Golden")
        self.GoldenTypButton.setMaximumWidth(100)
        self.Grade1TypButton = QtWidgets.QRadioButton("Grade1")
        self.Grade1TypButton.setChecked(True)
        self.Grade1TypButton.setMaximumWidth(100)
        self.UncertainTypButton = QtWidgets.QRadioButton("Uncertain")
        self.UncertainTypButton.setMaximumWidth(100)
        TagTypLayout.addWidget(self.GoldenTypButton,0,0)
        TagTypLayout.addWidget(self.Grade1TypButton,1,0)
        TagTypLayout.addWidget(self.UncertainTypButton,2,0)
        # --- Buttons of Labels
        self.tagButtonGrp = []
        self.InspirButton = QtWidgets.QPushButton()
        self.InspirButton.setText("Inspiration")
        self.InspirButton.setObjectName("InspirButton")
        self.tagButtonGrp.append(self.InspirButton)
        self.ExpirButton = QtWidgets.QPushButton()
        self.ExpirButton.setText("Expiration")
        self.ExpirButton.setObjectName("ExpirButton")
        self.tagButtonGrp.append(self.ExpirButton)
        self.WheezeButton = QtWidgets.QPushButton()
        self.WheezeButton.setText("Wheeze")
        self.WheezeButton.setObjectName("WheezeButton")
        self.tagButtonGrp.append(self.WheezeButton)
        self.StridorButton = QtWidgets.QPushButton()
        self.StridorButton.setText("Stridor")
        self.StridorButton.setObjectName("StridorButton")
        self.tagButtonGrp.append(self.StridorButton)        
        self.RhonchiButton = QtWidgets.QPushButton()
        self.RhonchiButton.setText("Rhonchi")
        self.RhonchiButton.setObjectName("RhonchiButton")
        self.tagButtonGrp.append(self.RhonchiButton)        
        self.ContinuousButton = QtWidgets.QPushButton()
        self.ContinuousButton.setText("Continuous")
        self.ContinuousButton.setObjectName("ContinuousButton")
        self.tagButtonGrp.append(self.ContinuousButton)
        self.OthersButton = QtWidgets.QPushButton()
        self.OthersButton.setText("Others")
        self.OthersButton.setObjectName("OthersButton")
        self.tagButtonGrp.append(self.OthersButton)
        self.RalesButton = QtWidgets.QPushButton()
        self.RalesButton.setText("Rales")
        self.RalesButton.setObjectName("RalesButton")
        self.tagButtonGrp.append(self.RalesButton)   
        self.UncertainButton = QtWidgets.QPushButton()
        self.UncertainButton.setText("Uncertain")
        self.UncertainButton.setObjectName("UncertainButton")
        self.tagButtonGrp.append(self.UncertainButton)
        self.NoiseButton = QtWidgets.QPushButton()
        self.NoiseButton.setText("Noise")
        self.NoiseButton.setObjectName("NoiseButton")
        self.tagButtonGrp.append(self.NoiseButton)
        self.nonContinuousButton = QtWidgets.QPushButton()
        self.nonContinuousButton.setText("nonContinuous")
        self.nonContinuousButton.setObjectName("nonContinuousButton")
        self.tagButtonGrp.append(self.nonContinuousButton)
        self.NormalButton = QtWidgets.QPushButton()
        self.NormalButton.setText("Normal")
        self.NormalButton.setObjectName("NormalButton")
        self.tagButtonGrp.append(self.NormalButton)
        self.NBCButton = QtWidgets.QPushButton()
        self.NBCButton.setText("NBC")
        self.NBCButton.setObjectName("NBCButton")
        self.tagButtonGrp.append(self.NBCButton)
        self.unknownButton = QtWidgets.QPushButton()
        self.unknownButton.setText("Unknown")
        self.unknownButton.setObjectName("unknownButton")
        self.tagButtonGrp.append(self.unknownButton)
        self.WheezeGoldenPos = []
        self.WheezeGrade1Pos = []
        self.WheezeUncertainPos = []
        self.RalesGoldenPos = []
        self.RalesGrade1Pos = []
        self.RalesUncertainPos = []
        self.RhonchiGoldenPos = []
        self.RhonchiGrade1Pos = []
        self.RhonchiUncertainPos = []
        self.StridorGoldenPos = []
        self.StridorGrade1Pos = []
        self.StridorUncertainPos = []
        self.ContinuousPos = []
        self.OthersPos = []     
        self.InspirGoldenPos = []
        self.InspirGrade1Pos = []
        self.ExpirGoldenPos = []
        self.ExpirGrade1Pos = []
        self.UncertainPos = []
        self.NoisePos = []
        self.nonContinuousPos = []
        self.NormalPos = []
        self.NBCPos = []
        self.UnknownPos = []
        self.TagPosData = [self.InspirGoldenPos, self.InspirGrade1Pos,\
            self.ExpirGoldenPos, self.ExpirGrade1Pos,\
            self.WheezeGoldenPos, self.WheezeGrade1Pos, self.WheezeUncertainPos,\
            self.StridorGoldenPos, self.StridorGrade1Pos, self.StridorUncertainPos,\
            self.RhonchiGoldenPos, self.RhonchiGrade1Pos, self.RhonchiUncertainPos, \
            self.ContinuousPos,\
            self.OthersPos,\
            self.RalesGoldenPos, self.RalesGrade1Pos, self.RalesUncertainPos,\
            self.UncertainPos, self.NoisePos, self.nonContinuousPos, self.NormalPos, self.NBCPos, self.UnknownPos ]
        self.extraBtns = []
        self.extraTagPos = []
        ExtraTagsName = ["Velum", "Oral_or_Tongue", "noBreath"]
        if "UserDefinedTags" in self.config.keys():
            for tag in self.config['UserDefinedTags']:
                ExtraTagsName.append(tag)
        for tag in ExtraTagsName:
            self.extraBtns.append(QtWidgets.QPushButton())
            self.extraBtns[-1].setText(tag)
            self.extraTagPos.append([])
        self.tagButtonGrp.extend(self.extraBtns)
        self.TagPosData.extend(self.extraTagPos)
        self.tagName = ["Inspiration_Golden", "Inspiration_Grade1",
        "Expiration_Golden", "Expiration_Grade1",
        "Wheeze_Golden", "Wheeze_Grade1", "Wheeze_Uncertain",
        "Stridor_Golden", "Stridor_Grade1", "Stridor_Uncertain",
        "Rhonchi_Golden", "Rhonchi_Grade1", "Rhonchi_Uncertain",
        "Continuous",
        "Others",
        "Rales_Golden", "Rales_Grade1", "Rales_Uncertian",
        "Uncertain", "Noise", "nonContinuous", "Normal", "NBC", "Unknown"]
        # ,"Inspiration", "Expiration", "Wheeze", "Stridor", "Rhonchi", "Rales"]
        # self.tagName = self.tagName[:(len(self.TagPosData))]
        self.tagName.extend(ExtraTagsName)
        self.preTagPosData = [[] for i in range(len(self.TagPosData))]
        cnt_btn = 0
        self.TagTypIdx = [] # idx of tagName to be used
        self.tagButtonSelect_input = self.config["TagButtons"]
        self.tagButtonSelect = []
        self.tagButtonSelect_name = []
        # self.TagTypGrpbox.setFixedHeight(150 + 30*(len(self.tagButtonSelect_input)-3))
        # TagActGrpBox.setMinimumHeight(150)
        for tagButton in self.tagButtonGrp:
            if tagButton.text() in self.tagButtonSelect_input:
                TagTypLayout.addWidget(tagButton,cnt_btn,1)
                self.tagButtonSelect_name.append(tagButton.text())
                self.tagButtonSelect.append(tagButton)
                if tagButton.text()=='Inspiration':
                    self.TagTypIdx.extend([0,1])
                elif tagButton.text()=='Expiration':
                    self.TagTypIdx.extend([2,3])
                elif tagButton.text()=='Wheeze':
                    self.TagTypIdx.extend([4,5,6])
                elif tagButton.text()=='Stridor':
                    self.TagTypIdx.extend([7,8,9])
                elif tagButton.text()=='Rhonchi':
                    self.TagTypIdx.extend([10,11,12])
                elif tagButton.text()=='Continuous':
                    self.TagTypIdx.extend([13])
                elif tagButton.text()=='Others':
                    self.TagTypIdx.extend([14])
                elif tagButton.text()=='Rales':
                    self.TagTypIdx.extend([15,16,17])
                elif tagButton.text()=='Uncertain':
                    self.TagTypIdx.extend([18])
                elif tagButton.text()=='Noise':
                    self.TagTypIdx.extend([19])
                elif tagButton.text()=='nonContinuous':
                    self.TagTypIdx.extend([20])
                elif tagButton.text()=='Normal':
                    self.TagTypIdx.extend([21])
                else: 
                    # print(f'searching new tagButton')
                    tag_kw = tagButton.text()
                    idx = [ i for i in range(len(self.tagName))
                            if self.tagName[i].split('_')[0] == tag_kw
                                or
                                self.tagName[i] == tag_kw]
                    self.TagTypIdx.extend(idx)
                    # print(f'\ttagButton:{tag_kw}  idx:{idx}')
                cnt_btn += 1
        # print(f'TagTypIdx:{self.TagTypIdx}')
        # --- munual comment & tag
        self.myTagBtn = QtWidgets.QPushButton('My Tag')
        TagTypLayout.addWidget(self.myTagBtn,cnt_btn-2,0)
        self.myCommentBtn = QtWidgets.QPushButton('Comment')
        TagTypLayout.addWidget(self.myCommentBtn,cnt_btn-1,0)
        self.TagTypGrpbox.setLayout(TagTypLayout)
        # print(f'tagtypIdx:{self.TagTypIdx}\ntagname:{[self.tagName[i] for i in self.TagTypIdx]}\n')        
        # tagInfolayout.addWidget(self.TagTypGrpbox,row_tagInfo,0,1,1)
        tagInfolayout.addWidget(self.TagTypGrpbox)
        # row_tagInfo += 1

        # ------ GroupBox of Save Screen / Spectrogram
        savscr_GrpBox = QtWidgets.QHBoxLayout()
        # savscr_GrpBox.setFixedWidth(taginfowidth)
        # savscr_GrpBox.setFixedHeight(25)

        self.ScreenShotButton = QtWidgets.QPushButton('ScreenShot')
        # self.ScreenShotButton.setFixedHeight(25)
        # self.ScreenShotButton.setFixedWidth(150)
        savscr_GrpBox.addWidget(self.ScreenShotButton)
        self.savSpectroBtn = QtWidgets.QPushButton('Save Spectrogram')
        # self.savSpectroBtn.setFixedHeight(25)
        # self.savSpectroBtn.setFixedWidth(150)
        savscr_GrpBox.addWidget(self.savSpectroBtn)
 
        # tagInfolayout.addWidget(self.ScreenShotButton,row_tagInfo,0)
        # tagInfolayout.addWidget(self.ScreenShotButton)
        tagInfolayout.addLayout(savscr_GrpBox)
        # row_tagInfo += 1      
        
        # ------ spaceitem at bottom
        # if self.config['SmallerScreen']:
        #     space2 = QtWidgets.QSpacerItem(100,120+(300-self.config['plotheight']-(len(self.tagButtonSelect)-3)*30))
        # else:
        #     space2 = QtWidgets.QSpacerItem(100,140+(600-self.config['plotheight']-(len(self.tagButtonSelect)-3)*30)) #max(0,5-len(self.tagButtonSelect_input))*25)
        # tagInfolayout.addItem(space2,row_tagInfo,0)
        # tagInfolayout.addSpacerItem(space2)
        # tagInfolayout.addStretch(max(15-(len(self.tagButtonSelect)-3)*9, 0))
        if self.config['spacer2_height_factor']:
            tagInfolayout.addStretch(self.config['spacer2_height_factor'])
        # row_tagInfo += 1

        self.MainLayout.addLayout(tagInfolayout,0,0)
        
        # ====== Layout of spectrogram, waveform, tag position
        WaveformLayout = QtWidgets.QGridLayout()

        pg.setConfigOptions(imageAxisOrder='row-major')
        self.win = pg.GraphicsLayoutWidget()
        self.win.setMinimumWidth(self.config['plotwidth'])
        # ------ spectrogram of whole WAV
        # --- plot of spectrogram
        self.p1 = self.win.addPlot(row=0, col=1, rowspan=1, colspan=1)
        self.p1.setContentsMargins(0,0,0,0)
        # self.p1.hideAxis('left')
        self.p1.setMinimumHeight(370)
        # self.p1.setMaximumHeight(500)
        # if self.config['SmallerScreen']:
        #     self.p1.setMaximumHeight(min(500,self.config['plotheight']))
        # else:
        #     self.p1.setMaximumHeight(self.config['plotheight'])
        self.p1.setFixedHeight(self.config['spectrogram_height'])
        # self.p1.setMinimumWidth(800)
        # self.p1.setFixedHeight(550)
        self.p1.setMouseEnabled(x=False, y=False)    
        self.p1infLine = pgInfiniteLine.InfiniteLine(pos=1, angle=90, movable=True)
        # ------ plot of tag
        self.pTagPos = []
        self.imgTagPos = []
        self.imgPreTagPos = []
        self.untagColor = 20
        for i in range(len(self.tagButtonSelect)+1):
            self.pTagPos.append(self.win.addPlot(row=1+i, col=1))
            # print(self.pTagPos[i].geometry())
            self.pTagPos[i].setMouseEnabled(x=False, y=False)
            # self.pTagPos[i].hideAxis('left')
            if i < len(self.tagButtonSelect):
                self.pTagPos[i].hideAxis('bottom')
            # if self.config['SmallerScreen']:
            #     self.pTagPos[i].setFixedHeight(15)
            # else:
            #     if len(self.tagButtonSelect) > 8:
            #         bar_hight = 22
            #     elif len(self.tagButtonSelect) > 5:
            #         bar_hight = 24
            #     elif len(self.tagButtonSelect) == 5:
            #         bar_hight = 25
            #     else:
            #         bar_hight = 26
            #     self.pTagPos[i].setFixedHeight(bar_hight)
            imaY = 200
            ima = np.ones((imaY, imaY*4))*self.untagColor
            self.imgTagPos.append(pg.ImageItem())
            self.pTagPos[i].addItem(self.imgTagPos[i])
            self.imgTagPos[i].setImage(ima)
            self.imgPreTagPos.append(pg.ImageItem())        
        WaveformLayout.addWidget(self.win,1,0)
        self.MainLayout.addLayout(WaveformLayout,0,1)

        # ------ Display of Message 
        self.MsgView = QtWidgets.QTextEdit()
        # self.MsgView.setText('Please keyin your id first!')
        self.MsgView.setMaximumHeight(30)
        msgLayout = QtWidgets.QGridLayout()
        msgLayout.addWidget(self.MsgView)
        self.MainLayout.addLayout(msgLayout,1,0,1,3)
        # QtGui.QGridLayout(self.centralWidget)

        MainWindow.setCentralWidget(self.centralWidget)
        self.menuBar = QtWidgets.QMenuBar(MainWindow)
        self.menuBar.setGeometry(QtCore.QRect(0, 0, 484, 22))
        self.menuBar.setObjectName("menuBar")
        self.menuFIle = QtWidgets.QMenu(self.menuBar)
        self.menuFIle.setObjectName("menuFIle")
        MainWindow.setMenuBar(self.menuBar)
        self.statusBar = QtWidgets.QStatusBar(MainWindow)
        self.statusBar.setObjectName("statusBar")
        MainWindow.setStatusBar(self.statusBar)
        self.open_file_action = QtWidgets.QAction(MainWindow)
        self.open_file_action.setObjectName("open_file_action")
        self.menuFIle.addAction(self.open_file_action)
        self.menuBar.addAction(self.menuFIle.menuAction())
        self.menuFIle.setTitle("FIle")
        self.open_file_action.setText("Open file...")

        # self.retranslateUi(MainWindow)
        QtCore.QMetaObject.connectSlotsByName(MainWindow)

    # def retranslateUi(self, MainWindow):
    #     _translate = QtCore.QCoreApplication.translate
    #     MainWindow.setWindowTitle(_translate("MainWindow", "Heroic-Faith SoundLabeler Ver.20190929_01"))
    #     self.currentTimeLabel.setText(_translate("MainWindow", "0"))
    #     self.totalTimeLabel.setText(_translate("MainWindow", "0:00"))
    #     self.menuFIle.setTitle(_translate("MainWindow", "FIle"))
    #     self.open_file_action.setText(_translate("MainWindow", "Open file..."))

