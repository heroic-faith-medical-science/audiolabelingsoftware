# -*- mode: python -*-

block_cipher = None


a = Analysis(['main.py'],
             pathex=[],
             binaries=[],
             datas=[],
             hiddenimports=['numba.decorators','scipy.special.cython_special','numpy.core._dtype_ctypes','sklearn.tree._utils', 'sklearn.tree', 'sklearn.neighbors.quad_tree', 'sklearn.neighbors.typedefs', 'pgInfiniteLine.py', 'pgLinearRegionItem.py', 'soundfile.py', 'sklearn.utils._cython_blas', 'librosa', 'numpy', 'scipy', 'PyQt5', 'pyqtgraph', 'ntplib', 'datetime', 'hashlib'],
             hookspath=[],
             runtime_hooks=[],
             excludes=[],
             win_no_prefer_redirects=False,
             win_private_assemblies=False,
             cipher=block_cipher,
             noarchive=False)
pyz = PYZ(a.pure, a.zipped_data,
             cipher=block_cipher)
exe = EXE(pyz,
          a.scripts,
          a.binaries,
          a.zipfiles,
          a.datas,
          [],
          name='main',
          debug=False,
          bootloader_ignore_signals=False,
          strip=False,
          upx=True,
          runtime_tmpdir=None,
          console=True )
