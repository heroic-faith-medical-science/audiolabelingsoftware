import json
import ntplib
from datetime import datetime, timezone
import hashlib
from PyQt5.QtWidgets import QApplication, QLineEdit, QWidget, QLabel
import sys

class lineEditDemo(QWidget):
    def __init__(self, parent=None):
        super(lineEditDemo, self).__init__(parent)
        self.setWindowTitle('key generator of Heroic-Faith Sound Labeler')
        self.resize(510, 150)

        with open('config_keygen.json' , 'r', encoding = 'utf-8-sig') as reader:
            self.config = json.loads(reader.read())

        # int_validato = QIntValidator(50, 100, self)  # 实例化整型验证器，并设置范围为50-100
        idLabel = QLabel(self)
        idLabel.setText('user id:')
        idLabel.move(20,10)
        self.id = QLineEdit(self)  # 整型文本框
        # int_le.setValidator(int_validato)  # 设置验证
        self.id.move(70, 10)

        # # 实例化浮点型验证器，并设置范围为-100到100，并精确2位小数
        # float_validato = QDoubleValidator(-100, 100, 2, self)  
        # float_le = QLineEdit(self)  # 浮点文本框
        # float_le.setValidator(float_validato)  # 设置验证
        # float_le.move(50, 50)

        # re = QRegExp('[a-zA-Z0-9]+$')  # 正则:只允许出现的大小写字母和数字
        # re_validato = QRegExpValidator(re, self)  # 实例化正则验证器
        keyLabel = QLabel(self)
        keyLabel.setText('Key:')
        keyLabel.move(20,55)
        self.keygen = QLineEdit(self)  # 正则文本框
        # re_le.setValidator(re_validato)  # 设置验证
        self.keygen.move(60, 50)
        self.keygen.resize(250, 30)
        self.id.editingFinished.connect(self.genkey)

        self.validtime = QLabel(self)
        self.validtime.move(20,90)
        self.validtime.resize(250,30)

        self.userid = self.config['userid']


    def genkey(self):
        if self.id.text() in self.userid:
            client = ntplib.NTPClient()
            response = client.request('uk.pool.ntp.org', version=3)
            ans = []
            anstmp = datetime.utcfromtimestamp(response.tx_time).isoformat(' ')[:13]+self.id.text()
            ans = hashlib.md5(anstmp.encode('utf-8-sig')).hexdigest()
            # print(ans)
            self.keygen.setText(ans)
            timeup = datetime.utcfromtimestamp(response.tx_time+60*60*(26+8)).isoformat(' ')
            self.validtime.setText(f'This key is valid until {timeup}')
        else:
            self.id.setText('Not a valid id!')

if __name__ == '__main__':
    app = QApplication(sys.argv)
    main = lineEditDemo()
    main.show()
    sys.exit(app.exec_())        
