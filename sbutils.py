import matplotlib.pyplot as plt
import numpy as np
from scipy import signal
import os
import matplotlib
from functools import partial
from PyQt5 import QtCore
import pyqtgraph as pg
import librosa
import shutil
chinese_font = matplotlib.font_manager.FontProperties(fname='C:\Windows\Fonts\mingliu.ttc', size=16)

class utils():
    def __init__(self, sbwin):
        self.sbwin = sbwin
        
    def getSpectrData(self, snd):
        # --- auto-adjustment of fftlen, noverlap and pad_to with various duration
        if self.sbwin.duration <= 0.3:
            fftlen = 64
            noverlap = round(fftlen*.98)
            pad_to = round(fftlen*4)
        elif self.sbwin.duration <= 0.5:
            if self.sbwin.Spectnfftmode.currentText() == 'fineFreqReso':
                fftlen = 256
                noverlap = round(fftlen*.98)
                pad_to = round(fftlen*2)
            elif self.sbwin.Spectnfftmode.currentText() == 'fineTimeReso':
                fftlen = 64
                noverlap = round(fftlen*.96)
                pad_to = round(fftlen*3.5)
            else: # Normal
                fftlen = 128
                noverlap = round(fftlen*.97)
                pad_to = round(fftlen*2.5)
        elif self.sbwin.duration <= 1:
            if self.sbwin.Spectnfftmode.currentText() == 'fineFreqReso':
                fftlen = 256
                noverlap = round(fftlen*.96)
                pad_to = round(fftlen*2.5)
            elif self.sbwin.Spectnfftmode.currentText() == 'fineTimeReso':
                fftlen = 64
                noverlap = round(fftlen*.9)
                pad_to = round(fftlen*5)
            else: # Normal
                fftlen = 128
                noverlap = round(fftlen*.94)
                pad_to = round(fftlen*2.5)
        elif self.sbwin.duration <= 1.5:
            if self.sbwin.Spectnfftmode.currentText() == 'fineFreqReso':
                fftlen = 512
                noverlap = round(fftlen*.96)
                pad_to = round(fftlen*1.5)
            elif self.sbwin.Spectnfftmode.currentText() == 'fineTimeReso':
                fftlen = 128
                noverlap = round(fftlen*.92)
                pad_to = round(fftlen*3.5)
            else: # Normal
                fftlen = 256
                noverlap = round(fftlen*.95)
                pad_to = round(fftlen*2.5)
        elif self.sbwin.duration <= 2.2:
            if self.sbwin.Spectnfftmode.currentText() == 'fineFreqReso':
                fftlen = 512
                noverlap = round(fftlen*.95)
                pad_to = round(fftlen*1.5)
            elif self.sbwin.Spectnfftmode.currentText() == 'fineTimeReso':
                fftlen = 128
                noverlap = round(fftlen*.92)
                pad_to = round(fftlen*2)
            else: # Normal
                fftlen = 256
                noverlap = round(fftlen*.94)
                pad_to = round(fftlen*2)
        elif self.sbwin.duration <= 3:
            if self.sbwin.Spectnfftmode.currentText() == 'fineFreqReso':
                fftlen = 512
                noverlap = round(fftlen*.95)
                pad_to = round(fftlen*1.5)
            elif self.sbwin.Spectnfftmode.currentText() == 'fineTimeReso':
                fftlen = 128
                noverlap = round(fftlen*.84)
                pad_to = round(fftlen*2.5)
            else: # Normal
                fftlen = 256
                noverlap = round(fftlen*.91)
                pad_to = round(fftlen*2.5)
        elif self.sbwin.duration <= 5.5:
            if self.sbwin.Spectnfftmode.currentText() == 'fineFreqReso':
                fftlen = 512
                noverlap = round(fftlen*.93)
                pad_to = round(fftlen*1.4)
            elif self.sbwin.Spectnfftmode.currentText() == 'fineTimeReso':
                fftlen = 128
                noverlap = round(fftlen*.72)
                pad_to = round(fftlen*3)
            else: # Normal
                fftlen = 256
                noverlap = round(fftlen*.86)
                pad_to = round(fftlen*2)
        elif self.sbwin.duration <= 10:
            if self.sbwin.Spectnfftmode.currentText() == 'fineTimeReso':
                fftlen = 256
                noverlap = round(fftlen*.73)
                pad_to = round(fftlen*1.5)
            else: # Normal
                fftlen = 512
                noverlap = round(fftlen*.85)
                pad_to = round(fftlen*1)
        elif self.sbwin.duration <= 16:
            if self.sbwin.Spectnfftmode.currentText() == 'fineTimeReso':
                fftlen = 256
                noverlap = round(fftlen*.65)
                pad_to = round(fftlen*1.5)
            else: # Normal
                fftlen = 512
                noverlap = round(fftlen*.75)
                pad_to = round(fftlen*1.5)
        elif self.sbwin.duration <= 30:
            fftlen = 512
            noverlap = round(fftlen*.6)
            pad_to = round(fftlen*.92)
        else:
            fftlen = 1024
            noverlap = (fftlen - (self.sbwin.idxf-self.sbwin.idxi)//500
                        if fftlen - (self.sbwin.idxf-self.sbwin.idxi)//500 > 0
                        else max(1,round(fftlen*.6*120/self.sbwin.duration)))
            pad_to = round(fftlen*0.92)
        self.sbwin.manulSpectro = True
        if self.sbwin.SpectAuto.isChecked():
            self.sbwin.manulSpectro = False
        # munual adjustment of fftlne, noverlap and pad_to
        if self.sbwin.manulSpectro:
            fftlen = int(self.sbwin.Spectnfft.currentText())
            noverlap = round(fftlen * self.sbwin.Spectnoverlap.value()/100)
            pad_to = round(fftlen * self.sbwin.Spectpad_to.value()/100)
        elif self.sbwin.isvip:  # update parameters on GUI according to preset values
            # nfft
            for i in range(self.sbwin.Spectnfft.count()):
                # print(f'fft sel:{self.sbwin.Spectnfft.itemText(i)}')
                if int(self.sbwin.Spectnfft.itemText(i)) == fftlen:
                    self.sbwin.Spectnfft.setCurrentIndex(i)
                    break
            # noverlap
            self.sbwin.Spectnoverlap.setValue(round(noverlap/fftlen*100,2))
            # pad_to
            self.sbwin.Spectpad_to.setValue(round(pad_to/fftlen*100,2))
        # Sxx, f, t, im = plt.specgram(snd, NFFT=fftlen, Fs=sr, noverlap=noverlap, window=None, pad_to=pad_to, cmap='afmhot',vmin=vmin,vmax=vmax)
        if not self.sbwin.config['SmallerScreen']:
            vmin = float(self.sbwin.Spectvmin.text())
            vmax = float(self.sbwin.Spectvmax.text())
        else:
            vmin = self.sbwin.config['Spectrogram']['vmin']*10
            vmax = self.sbwin.config['Spectrogram']['vmax']*10
        # ti_plt = time.perf_counter()
        Sxx, f, t, im = plt.specgram(snd[self.sbwin.idxi:self.sbwin.idxf], NFFT=fftlen, Fs=self.sbwin.sr, noverlap=noverlap, window=None, pad_to=pad_to, \
            cmap='afmhot',vmin=vmin, vmax=vmax)
        # tf_plt = time.perf_counter()
        # self.sbwin.debugMsg(f'elapsed time of matplotlib:{tf_plt-ti_plt}')
        # while not self.sbwin.manulSpectro and np.shape(logSx)[1] > self.sbwin.p1.geometry().width()/2:
        #     fft *= 2
        #     Sxx, f, t, im = plt.specgram(snd[self.sbwin.idxi:self.sbwin.idxf], NFFT=fftlen, Fs=self.sbwin.sr, noverlap=noverlap, window=None, pad_to=pad_to, \
        #         cmap='afmhot',vmin=float(self.sbwin.Spectvmin.text()), vmax=float(self.sbwin.Spectvmax.text()))
        plt.close()
        self.sbwin.logSx = np.log10(np.where(Sxx!=0, Sxx, 1e-20))
        logSx = self.sbwin.logSx[0:int(len(self.sbwin.logSx)/f[-1]*self.sbwin.Spectfmax.value()),:]
        # logSx = Sxx
        if self.sbwin.isvip:
            print(f'Spectrogram:  fftlen:{fftlen}  noverlap:{noverlap}({noverlap/fftlen:.2f}x)  pad_to:{pad_to}({pad_to/fftlen:.1f}x)')
            print(f'Spectrogram:  f[0]:{f[0]:.1f}Hz  f[-1]:{f[-1]:.1f}Hz  t[0]:{t[0]:.3f}s  t[-1]:{t[-1]:.3f}s  shape_logSx:{np.shape(Sxx)}')
            print(f'ImageSize:{np.shape(logSx)[1]} x {np.shape(logSx)[0]}')
            print(f'log10(Sx) min:{np.min(logSx):.1f}\tmax:{np.max(logSx):.1f}')
        return fftlen, noverlap, pad_to, im, f, Sxx, logSx
    
    def find_local_min_max(self, tt, data, is_plot=False, select_min=True):
        # == delete points with the same value as its following point
        # = all zeros
        # data[data == 0] = np.random.uniform(0, 0.05, data[data == 0].size)
        # data[data == 0] = np.linspace(0.05, 0, data[data == 0].size)
        zero_idx = (data == 0).nonzero()[0]
        cnt = 0
        for i, z in enumerate(zero_idx):
            if i and z == zero_idx[i-1] + 1:
                cnt += 1
            else:
                cnt = 0
            # elif i and i+1 < zero_idx.size and z == zero_idx[i-1]+1 and z + 1 != zero_idx[i+1]:
            #     cnt = -1
            if cnt == 0:
                if z and data[z-1] < 0.1:
                    data[z] = data[z-1] + 0.05
                    needrise = False
                else:
                    data[z] = 0.05
                    needrise = True
                ref_i = data[z]
            if cnt > 0:
                if not needrise:
                    data[z] = ref_i - cnt*0.0001
                else:
                    data[z] = ref_i + 0.001
                    ref_i = data[z]
                    needrise = False
        # = all horizontal line data
        del_inx = np.where(np.diff(data) == 0)[0]
        tt = np.delete(np.array(tt), del_inx)
        data = np.delete(data, del_inx)
        diff_1st = np.diff(data)
        idx_per_sec = data.size/self.sbwin.duration
        # == evaluate of how unstable the envelope
        seg_amount = int(self.sbwin.duration)
        seg_len = diff_1st.size//seg_amount
        LL_interval_extre = int(self.sbwin.config['Envelope']['LL_interval_extre']*(data.size/max(tt)))
        # for i in range(seg_amount):
        #     loc_PV = np.diff(np.sign(diff_1st[seg_len*i:seg_len*(i+1)])).nonzero()[0]
        #     unstable_cnt = np.count_nonzero(np.diff(loc_PV) < LL_interval_extre)
        #     print(f'unstable count of seg {i:2d}({tt[seg_len*i]:4.1f}~{tt[seg_len*(i+1)]:4.1f}sec) = {unstable_cnt}')
        # == candidates of loc_extremum
        # loc_PV = np.diff(np.sign(diff_1st)).nonzero()[0] + 1               # local min & max
        loc_min = (np.diff(np.sign(diff_1st)) > 0).nonzero()[0] + 1        # local min
        loc_max = (np.diff(np.sign(diff_1st)) < 0).nonzero()[0] + 1        # local max 
        if select_min:
            # = lower limit of interval between local minima
            LL_interval_minima = int(self.sbwin.config['Envelope']['LL_interval_minima']*(data.size/max(tt)))
            # = lower limit of interval between local minima to define a meaning I/E
            LL_interval_minima_IE = int(self.sbwin.config['Envelope']['LL_interval_minima_IE']*(data.size/max(tt)))
            # # = lower limit of interval between local extremum
            # interval_extrema_LL = int(self.sbwin.config['Envelope']['interval_extrema_LL']*(data.size/max(tt)))
            # = min. height gap between loc_min and adjacent loc_max
            LL_LVL_gap_orig = self.sbwin.config['Envelope']['LL_LVL_gap_orig']
            # = height baseline for calculating the weight of LL_LVL_gap
            LVL_base_dynamic_weight = self.sbwin.config['Envelope']['LVL_base_dynamic_weight']
            # = height gap threshold to define 'valley'
            LL_LVL_gap_foot = self.sbwin.config['Envelope']['LL_LVL_gap_foot']
            # # = LL of height gap between adjacent loc_mx and next loc_min
            # LL_LVL_gap_refmx = self.sbwin.config['Envelope']['LL_LVL_gap_refmx']
            # = check whether loc_min is surounded by two adjacent loc_mx if loc_min is higher than metapoint_LVL
            LL_metapoint_LVL = self.sbwin.config['Envelope']['LL_metapoint_LVL']
            # = max height for finding foot of low-slope downhill
            LVL_slow_foot = self.sbwin.config['Envelope']['LVL_slow_foot']
            weight_R_peak = 1
            # = threshold of "flat"
            flat_diff_1st = 0.01
            flat_mx_min_gap = 0.075
            flat_final_min_gap = 0.015
            LL_flat_width_idx = self.sbwin.config['Envelope']['LL_flat_width_sec']*idx_per_sec
            # LL_LVL_gap_ends = 0.15
            # == initial UL of local min 
            LVL_UL = 0.5
            append_locmin = []      # to locate foot of a wide flat valley
            if loc_min[0] < loc_max[0]:
                loc_max = np.insert(loc_max, 0, 0)
            # == mask of final loc min/max
            mask_min = np.zeros(loc_min.size, dtype=bool)
            mask_max = np.zeros(loc_max.size, dtype=bool)
            for i, lc in enumerate(loc_min):
                # == dynamic LL_LVL_gap
                LL_LVL_gap = max(LL_LVL_gap_orig*data[lc]/LVL_base_dynamic_weight,
                                    LL_LVL_gap_orig)
                if data[lc] >= LVL_UL:    # level threshold
                    LL_LVL_gap *= 2
                if self.sbwin.low_vol_warning:
                    LL_LVL_gap *= self.sbwin.config['Envelope']['weight_LL_LVL_gap_lowVol']
                # === define meaningful loc_minima
                if loc_min[0] < loc_max[0]:
                    raise ValueError("loc_min[0] < loc_max[0]")
                # === 1st extremum is loc_max ( loc_max[i] --> loc_min[i] --> loc_max[i+1] )
                # == loc_min is between two loc_max
                # = not last local min
                if i <= loc_max.size-2:
                    # = if local min is low enough, it's enough to find only one valid local max
                    if data[lc] <= LL_metapoint_LVL:
                        if data[loc_max[i]]-data[lc] > LL_LVL_gap:
                            mask_min[i] = True
                            mask_max[i] = True
                        elif (i and data[loc_max[i]] > LL_metapoint_LVL
                                and data[loc_min[i-1]] > data[lc]
                                and data[loc_max[i]]-data[loc_min[i-1]] < LL_LVL_gap
                                and data[loc_max[i-1]]-data[lc] > LL_LVL_gap):
                            mask_min[i] = True
                            mask_max[i-1] = True
                        # to define a foot of a slow-slope hill for better segmenting clip
                        elif (i and np.count_nonzero(mask_min)
                                and np.count_nonzero(mask_max)
                                and data[lc] < LVL_slow_foot
                                and loc_min[mask_min][-1] < loc_max[mask_max][-1]):
                            mask_min[i] = True
                        if (data[loc_max[i+1]]-data[lc])*weight_R_peak > LL_LVL_gap:
                            mask_min[i] = True
                            mask_max[i+1] = True
                        elif (i+2 < loc_max.size and data[loc_max[i+1]] > LL_metapoint_LVL
                                and data[loc_min[i+1]] > data[lc]
                                and data[loc_max[i+1]]-data[loc_min[i+1]] < LL_LVL_gap
                                and data[loc_max[i+2]]-data[lc] > LL_LVL_gap):
                            mask_min[i] = True
                            mask_max[i+2] = True
                        # consider a local min valid if it is quite low and the following local max is high enough
                        if (not mask_min[i] and data[lc] < 0.05
                                and data[loc_max[i+1]] >= LL_LVL_gap_orig):
                            mask_min[i] = True
                    # if loc_min is higher (ex. between I and E)
                    # the local min must be between two valid local max
                    # or close to a very high local max
                    else:
                            # check prior local maxima
                            if ((   data[loc_max[i]]-data[lc] > LL_LVL_gap
                                    or
                                    (   i
                                        and data[loc_min[i-1]] > data[lc]
                                        and data[loc_max[i]]-data[loc_min[i-1]] < LL_LVL_gap
                                        and data[loc_max[i-1]]-data[lc] > LL_LVL_gap))
                            # check following local maxima
                                and 
                                (   (data[loc_max[i+1]]-data[lc])*weight_R_peak > LL_LVL_gap
                                    or
                                    (   i+2 < loc_max.size
                                        and data[loc_min[i+1]] > data[lc]
                                        and data[loc_max[i+1]]-data[loc_min[i+1]] < LL_LVL_gap
                                        and data[loc_max[i+2]]-data[lc] > LL_LVL_gap))):
                                mask_min[i] = True
                                if data[loc_max[i]]-data[lc] > LL_LVL_gap:
                                    mask_max[i] = True
                                else:
                                    mask_max[i-1] = True
                                if (data[loc_max[i+1]]-data[lc])*weight_R_peak > LL_LVL_gap:
                                    mask_max[i+1] = True
                                else:
                                    mask_max[i+2] = True
                            # local min is close to only one very high local max 
                            elif (  (   (i+1 < loc_min.size and data[loc_min[i+1]] > data[lc])
                                        and 
                                        (   data[loc_max[i]]-data[lc] > LL_LVL_gap*2
                                            or
                                            (   i
                                                and data[loc_min[i-1]] > data[lc]
                                                and data[loc_max[i]]-data[loc_min[i-1]] < LL_LVL_gap
                                                and data[loc_max[i-1]]-data[lc] > LL_LVL_gap*2)))
                                    or
                                    (   (i and data[loc_min[i-1]] > data[lc])
                                        and
                                        (   data[loc_max[i+1]]-data[lc])*weight_R_peak > LL_LVL_gap*2
                                            or
                                            (   i+2 < loc_max.size
                                                and data[loc_min[i+1]] > data[lc]
                                                and data[loc_max[i+1]]-data[loc_min[i+1]] < LL_LVL_gap
                                                and data[loc_max[i+2]]-data[lc] > LL_LVL_gap*2))):
                                mask_min[i] = True
                                if data[loc_max[i]]-data[lc] > LL_LVL_gap*2:
                                    mask_max[i] = True
                                elif data[loc_max[i-1]]-data[lc] > LL_LVL_gap*2:
                                    mask_max[i-1] = True
                                elif (data[loc_max[i+1]]-data[lc])*weight_R_peak > LL_LVL_gap*2:
                                    mask_max[i+1] = True
                                else:
                                    mask_max[i+2] = True
                # = last extremum is loc_min 
                elif (  i == loc_max.size-1
                        and
                        (   data[loc_max][i]-data[lc] > LL_LVL_gap
                            or (data[-1]-data[lc])*weight_R_peak > LL_LVL_gap
                            or data[lc] <= 0.1
                            or (    i
                                    and data[loc_max[i]]-data[loc_min[i-1]] < LL_LVL_gap
                                    and data[loc_max[i-1]]-data[lc] > LL_LVL_gap))):
                    mask_min[i] = True
                    mask_max[i] = True
                    if data[-1]-data[lc] > LL_LVL_gap:
                        loc_max = np.block([loc_max, data.size-1])
                        mask_max = np.block([mask_max, True])
                if mask_min[i]:
                    ismoved = False
                    lc_orig = lc
                    # = move forward to foot if loc_max ahead is almost as low as loc_min
                    if i+1 < loc_max.size and data[loc_max[i]]-data[lc_orig] < flat_mx_min_gap:
                        while (mask_min[i] and lc < loc_max[i+1]-1
                                and data[lc] - data[lc_orig] < flat_final_min_gap
                                and diff_1st[lc+1] < flat_diff_1st):
                            lc += 1
                            loc_min[i] = lc
                    ismoved = False if lc == lc_orig else True
                    # = move backward to foot if following loc_max is almost as low as loc_min
                    if i+1 < loc_max.size and data[loc_max[i+1]]-data[lc] < flat_mx_min_gap:
                        while (not ismoved and mask_min[i]
                                and lc > loc_max[i]+1
                                and data[lc] - data[lc_orig] < flat_final_min_gap
                                and diff_1st[lc-1] > -flat_diff_1st):
                            lc -= 1
                            loc_min[i] = lc
                    ismoved = False if lc == lc_orig else True
                    # = move to root if only one loc_min in a wide and flat valley
                    if (not ismoved and mask_min[i]
                            and
                            (   (   i+1 < loc_max.size
                                    and
                                    data[loc_max[i+1]]-data[lc_orig] > LL_LVL_gap_foot and
                                    data[loc_max[i]]-data[lc_orig] > LL_LVL_gap_foot)
                                or
                                (   i == loc_max.size-1
                                    and
                                    data[loc_max[i]]-data[lc_orig] > LL_LVL_gap_foot))):
                        lc_end = loc_max[i+1]-1 if i+1 < loc_max.size else data.size-2
                        slowly_change_range = (abs(diff_1st[loc_max[i]+1:lc_end]) < flat_diff_1st)
                        small_gap_range = (data[loc_max[i]+1:lc_end] < data[lc_orig] + flat_final_min_gap)
                        flat_range = np.where(slowly_change_range & small_gap_range)[0] + loc_max[i] + 1
                        if flat_range.size > LL_flat_width_idx:
                            loc_min[i] = flat_range[0]
                            append_locmin.append(flat_range[-1])                    
                    # == check if interval threshold
                    if (len(loc_min[mask_min]) > 1
                            and lc - loc_min[mask_min][-2] <= LL_interval_minima):
                        mask_max[i] = False
                        if data[lc] <= data[loc_min[mask_min][-2]]:
                            k = 0
                            while not mask_min[i-1-k]:
                                k += 1
                            mask_min[i-1-k] = False
                        else:
                            mask_min[i] = False
            loc_min = loc_min[mask_min]
            loc_max = loc_max[mask_max]
            # = append right root of valley
            loc_min = np.block([loc_min, np.array(append_locmin)]).astype('uint16')
            # = sort it first to avoid mismatch
            loc_min = np.sort(loc_min)
            loc_max = np.sort(loc_max)
            # update lower limit of interval_minima
            if loc_min.size > 1:
                LL_interval_minima = max(np.min(np.diff(loc_min))*0.9, LL_interval_minima)
            else:
                return np.array([0, data.size-1]), np.array([])
            # if head of data is low enough, far enough from nearest loc_min
            if (loc_min.size 
                    and 
                    loc_min[0] > LL_interval_minima
                    and
                    (abs(data[0]-data[loc_min[0]]) < 0.1 or data[0] < 0.15)):
                loc_min = np.insert(loc_min, 0, 0)
            # if end of data is low enough, far enough from nearest loc_min
            if (loc_min.size
                    and len(data) - loc_min[-1] > LL_interval_minima
                    and (abs(data[-1]-data[loc_min[-1]]) < 0.1 or data[-1] < 0.15)):
                loc_min = np.block([loc_min, len(data)-1])
            # === define meaningful peaks, candidates of I or E
            loc_M_list = []
            loc_m_list = []
            for lc_m_i, lc_m_f in zip(loc_min[:-1], loc_min[1:]):
                lc_M_candidate = []
                if lc_m_i == loc_min[0]:    # prior to 1st loc_min
                    for lc_M in loc_max:
                        if lc_M > lc_m_i:
                            break
                        if data[lc_M] - data[lc_m_i] > LL_LVL_gap_orig:
                            lc_M_candidate.append(lc_M)
                    if len(lc_M_candidate):
                        loc_M_list.append(lc_M_candidate[np.argmax(data[lc_M_candidate])])
                        lc_M_candidate = []
                    loc_m_list.append(lc_m_i)
                if lc_m_i == loc_min[-2]:   # after last loc_min
                    for lc_M in loc_max:
                        if lc_M < lc_m_f:
                            continue
                        if data[lc_M] - data[lc_m_f] > LL_LVL_gap_orig:
                            lc_M_candidate.append(lc_M)
                    if len(lc_M_candidate):
                        loc_M_list.append(lc_M_candidate[np.argmax(data[lc_M_candidate])])
                        lc_M_candidate = []
                    loc_m_list.append(lc_m_f)
                for lc_M in loc_max:    # between 1st and last loc_min
                    if lc_M > lc_m_f:
                        break
                    # case1. a loc_min is between I and E 
                    #   don't ignore these two loc_min if a loc_max is between them
                    #   and one is higher than the other by > LL_LVL_gap_orig,
                    #   even they are very close to each other
                    # case2. avoid confusing rub noise(a very short peak) with breathing sound
                    if (lc_M > lc_m_i and lc_M < lc_m_f
                        and
                        (   data[lc_M] - data[lc_m_f] > LL_LVL_gap_orig
                            or
                            (data[lc_M] - data[lc_m_i])*weight_R_peak > LL_LVL_gap_orig)
                        and
                        (   lc_m_f - lc_m_i > LL_interval_minima_IE
                            or
                            abs(data[lc_m_f] - data[lc_m_i]) > LL_LVL_gap_orig
                            )):
                        lc_M_candidate.append(lc_M)
                # = a local max bewtween local minima
                if len(lc_M_candidate):
                    loc_M_list.append(lc_M_candidate[np.argmax(data[lc_M_candidate])])
                    loc_m_list.extend([lc_m_i, lc_m_f])
                # # = silence between local minima is too short
                # elif lc_m_f - lc_m_i < LL_interval_minima_IE:
                #     loc_m_list.pop()
            loc_max = np.array(loc_M_list, dtype='int')
            loc_min = np.array(loc_m_list, dtype='int')
            loc_min = np.unique(loc_min)
        if is_plot:
            fig, ax = plt.subplots(3, 1, figsize=(11,7), clear=True)
            ax[0].plot(tt, data, color='grey')
            ax[0].plot(tt[loc_min], data[loc_min], "x", label="min", color='r', markersize=10)
            ax[0].plot(tt[loc_max], data[loc_max], "+", label="max", color='g', markersize=10)
            # ax[0].plot(self.sbwin.t_envelope_sec, self.sbwin.envelope_RR, label='orig', color='b')
            ax[0].legend(loc='upper left')
            ax[0].set_xticks(np.arange(0,tt[-1],1))
            ax[0].set_ylim((0,min(2, np.max(data)*1.05)))
            ax[0].grid(b=True, axis='y')
            ax[1].plot(tt[1:], diff_1st, label='1st der')
            ax[1].plot(tt[2:], np.diff(data, n=2), label='2nd der')
            ax[1].grid(b=True, axis='y')
            ax[1].set_xticks(np.arange(0,tt[-1],1))
            ax[1].legend(loc='upper right')
            ax[2].plot(tt[2:], np.diff(np.sign(diff_1st)), 
                       label=f'cross point of original 1st der\n# of loc_min:{loc_min.size}')
            ax[2].legend(loc='upper right')
            ax[2].set_xticks(np.arange(0,tt[-1],1))
            plt.tight_layout(rect=(0.0,0.0,1,0.98))
            plt.suptitle(f'{os.path.basename(self.sbwin.fnnow)} {self.sbwin.ti}~{self.sbwin.tf}sec',
                        fontproperties=chinese_font)
            plt.show()
            plt.close()
            # fig = plt.figure(figsize=(11,7), clear=True)
            # gs = fig.add_gridspec(3,1)
            # ax = [[] for i in range(2)]
            # ax[0] = fig.add_subplot(gs[:2])
            # ax[1] = fig.add_subplot(gs[2])
            # ax2 = ax[0].twinx()            
            # ax2.plot(tt[1:], diff_1st, label='1st der')
            # ax2.plot(tt[2:], np.diff(data, n=2), label='2nd der')
            # ax2.grid(b=True, axis='y')
            # ax2.legend(loc='upper right')
            # ax[1].plot(np.diff(np.sign(diff_1st)), label='cross point of 1st der')
            # ax[1].legend(loc='upper right')
            # plt.savefig('tmp.png')
        return np.sort(loc_min), np.sort(loc_max)

    def getIEtag(self, lc_mi, lc_mx): 
        # = IEpretag
        # 0: tagcode(1:I,-2:E,-1:silence)
        # 1: T_start_sec(abs)
        # 2: T_end_sec(abs)
        # 3: T_start_idx(of envelope)
        # 4: T_end_idx(of envelope)
        # 5: T_start_idx(of stft)
        # 6: T_end_idx(of stft)
        # 7: gradecode(2:golden, 0:default)
        IEpretag = []
        mx_idx = 0
        Itag = False
        if lc_mi.size and lc_mx.size:   # valid loc_min and loc_max exist
            stft_x_idx_per_sec = self.sbwin.stft_abs_RR.shape[1]/self.sbwin.duration
            stft_y_idx_per_Hz = self.sbwin.stft_abs_RR.shape[0]/2000
            if lc_mi[0] > lc_mx[0]: # 1st extremum is loc_max
                IEpretag.append((1,
                                 self.sbwin.ti,
                                 self.sbwin.ti + self.sbwin.t_envelope_sec[lc_mi[0]],
                                 0,
                                 lc_mi[0],
                                 0,
                                 int(self.sbwin.t_envelope_sec[lc_mi[0]]*stft_x_idx_per_sec),
                                 0))
                mx_idx += 1
                Itag = True
            for i, lc in enumerate(lc_mi[1:]):
                if mx_idx < lc_mx.size and lc_mx[mx_idx] < lc:  # loc_max is ahead of a loc_min
                    ietag = -2 if Itag else 1
                    IEpretag.append((ietag,
                                     self.sbwin.ti + self.sbwin.t_envelope_sec[lc_mi[i]],
                                     self.sbwin.ti + self.sbwin.t_envelope_sec[lc],
                                     lc_mi[i],
                                     lc,
                                     int(self.sbwin.t_envelope_sec[lc_mi[i]] * stft_x_idx_per_sec),
                                     int(self.sbwin.t_envelope_sec[lc] * stft_x_idx_per_sec),
                                     0))
                    mx_idx += 1
                    Itag = False if ietag == -2 else True
                else:   # no loc_max between loc_min
                    IEpretag.append((-1,
                                     self.sbwin.ti + self.sbwin.t_envelope_sec[lc_mi[i]],
                                     self.sbwin.ti + self.sbwin.t_envelope_sec[lc],
                                     lc_mi[i],
                                     lc,
                                     int(self.sbwin.t_envelope_sec[lc_mi[i]] * stft_x_idx_per_sec),
                                     int(self.sbwin.t_envelope_sec[lc] * stft_x_idx_per_sec),
                                     0))
                    Itag = False
                # if lc == lc_mi[-1] and mx_idx == lc_mx.size-1:  # loc_max is behind the last loc_min
                #     ietag = -2 if Itag else 1
                #     IEpretag.append([ietag, self.sbwin.ti + self.sbwin.t_envelope_sec[lc], self.sbwin.ti + self.sbwin.t_envelope_sec[-1]
                #                       , lc, self.sbwin.envelope_RR.size-1])
            # [print(pretag) for pretag in IEpretag]
        else:   # no valid loc_min and loc_max
            IEpretag.append((-1,
                             self.sbwin.ti,
                             self.sbwin.tf,
                             0,
                             self.sbwin.envelope_RR.size-1,
                             0,
                             self.sbwin.stft_abs_RR.shape[1]-1,
                             0))
        IEpretag = np.array(IEpretag, dtype=([('tag','int8'),
                                              ('tisec','float16'),
                                              ('tfsec','float16'),
                                              ('ti_envelope','uint16'),
                                              ('tf_envelope','uint16'),
                                              ('ti_stft','uint16'),
                                              ('tf_stft','uint16'),
                                              ('grade','uint8'),]))
        # === double check IEtag from end by correlation
        # = model
        # 0:head idx
        # 1:model len
        # 2:consecutive_match_cnt
        # 3:type
        model = [-1,5,0,'nonie']    # initial
        IEtagidx_list = np.array([], dtype='uint8')
        # print(f'IEpretag {IEpretag[:,0]}')
        print(f"IEpretag {IEpretag['tag']}")
        # = IE_template
        # 0:model type
        # 1:pretag set
        # 2:auto-correlation value
        # 3:model len
        IE_template = [['IE', [1,-2], 5, 2],
                       ['IE0', [1,-2,-1], 6, 3],
                       ['I0', [1,-1], 2, 2]]
        # ==== matching IE pattern to find model and test pattern candidates
        for m_info in IE_template:
            model_corr = np.correlate(IEpretag['tag'], m_info[1]).astype('uint8')
            model_corr_match = (model_corr == m_info[2]).nonzero()[0]   # start idx of model
            model_corr_match_diff = (np.diff(model_corr_match) == m_info[3]).nonzero()[0]
            model_candidates = model_corr_match
            # = idx of IEtag that IE candidates begin from
            IEtagidx_list = np.block([IEtagidx_list, model_candidates])
            model_consecutive = model_candidates[model_corr_match_diff]
            match_len = len(model_consecutive)
            # == define model
            # 1: latest
            # 2: priority: I0 > IE0 > IE
            model = ([max(model_candidates), m_info[3], match_len, m_info[0]]
                    if len(model_candidates)
                        and 
                        (   max(model_candidates) >= model[0]
                            or
                            (   max(model_candidates) >= len(IEpretag) - 5
                                and m_info[0] == 'IE0'))
                    else model)
            if len(IEpretag)//m_info[3]-1 and len(IEpretag)//m_info[3]:
                print(f'{m_info[0]}: corr:{model_corr}'
                    f' consecutive match:{model_consecutive}({len(model_consecutive)/(len(IEpretag)//m_info[3]-1):.0%})'
                    f' candidates:{model_candidates}({len(model_candidates)/(len(IEpretag)//m_info[3]):.0%})')
            else:
                print(f'{m_info[0]}: corr:{model_corr}'
                    f' consecutive match:{model_consecutive}'
                    f' candidates:{model_candidates}')
        IEtagidx_list = np.unique(IEtagidx_list)
        RR_now_list = []    # RR(sec) list of current clip
        RR_now_score_list = []  # score list of RR(sec) list of current clip
        # ====== examine how match between patterns and models and calcuate RR by stft_abs correlation
        if self.sbwin.config['Envelope']['goIEmodel_corr'] and self.sbwin.config['Envelope']['go2Dmatch']:
            if model[0] != -1:
                # = model info
                model_len_sec = (IEpretag['tfsec'][min(model[0]+model[1]-1,
                                                       IEpretag.size-1)]
                                 - IEpretag['tisec'][model[0]])
                # stft idx for correaltion
                model_ti_idx = IEpretag['ti_stft'][model[0]]
                model_tf_idx = (IEpretag['tf_stft'][min(model[0]+model[1]-1,
                                                        IEpretag.size-1)]
                                if not '0' in model[-1]
                                else 
                                IEpretag['tf_stft'][min(model[0]+model[1]-2,
                                                        IEpretag.size-1)])
                freqband_node_Hz = self.sbwin.config['Envelope']['freqband_node_Hz']
                freqband_node_idx = (np.array(freqband_node_Hz) * stft_y_idx_per_Hz).astype('uint16')
                freqband_weight = self.sbwin.config['Envelope']['freqband_weight']
                # stft_Y idx where frequency band of weight > 0 starts
                freqband_i_idx = int(freqband_node_Hz[np.array(freqband_weight).nonzero()[0][0]]
                                                      *stft_y_idx_per_Hz)
                # stft_Y idx where frequency band of weight > 0 ends
                freqband_f_idx = int(freqband_node_Hz[np.array(freqband_weight).nonzero()[0][-1]+1]
                                                      *stft_y_idx_per_Hz)
                # = get stft ROI
                stft_ROI = np.zeros((freqband_f_idx-freqband_i_idx,
                                     self.sbwin.stft_abs_RR.shape[1]))
                for i, freq in enumerate(zip(freqband_node_idx[:-1],freqband_node_idx[1:])):
                    if freqband_weight[i]:
                        stft_ROI[freq[0]-freqband_i_idx:freq[1]-freqband_i_idx]\
                            = self.sbwin.stft_abs_RR[freq[0]:freq[1], :] * freqband_weight[i]
                model_pattern = stft_ROI[:, model_ti_idx:model_tf_idx]
                # = get model auto-correlation
                # t_start = time.perf_counter()
                unnorm_model_autocorr = (model_pattern*model_pattern).sum()
                norm_model_autocorr = ((model_pattern/model_pattern.max()
                                        *model_pattern/model_pattern.max()).sum())
                # t_end = time.perf_counter()
                print(f'evaluating model '
                       f"{IEpretag['tisec'][model[0]]:.1f}~{IEpretag['tfsec'][min(model[0]+model[1]-1,IEpretag.size-1)]:.1f}sec,"
                       f' len = {model[1]}, type:{model[3]},'
                       f' norm/unnorm autocorr = {norm_model_autocorr:.1f} / {unnorm_model_autocorr:.1f}')
                    #    f' consumption time = {(t_end-t_start)*1000:.1f}ms')
                print_score_headline = False
                # == scan IE sets to find matched IE
                # = define tp's end of each best-matched-IE search loop
                # end segment for stft correlation must NOT be silence segment
                for j in range(len(IEtagidx_list)):
                    if j == 0:
                        # quit if only model in the list
                        if not (IEtagidx_list == model[0]).nonzero()[0]:
                            break
                        # stft_x_idx of test pattern's end
                        tp_tf_idx = int(model_ti_idx if IEpretag['tag'][model[0]-1] != -1
                                       else
                                       IEpretag['tf_stft'][model[0]-2])
                        # timestamp(sec) of envelope idx of test pattern's end
                        tp_tf_sec = IEpretag['tisec'][model[0]]
                        # idx of IEpretag NEXT to test pattern's end
                        tp_f_IEpretag_idx = model[0]
                        # a reference idx in IEtagidx_list for searching test pattern
                        ref_i = IEtagidx_list[(IEtagidx_list == model[0]).nonzero()[0]-1]
                    elif len(tp_score):
                        best_tp_idx = np.argmin(np.array(tp_score)[:,0])
                        # == correct IEpretag of IE set prior to model
                        #    if vaild IE set is found
                        if tp_score[best_tp_idx][0] < self.sbwin.config['Envelope']['th_model_score']:
                            IEpretag['grade'][tp_score[best_tp_idx][2]:tp_f_IEpretag_idx] = 2
                            for i in range(tp_score[best_tp_idx][2],tp_f_IEpretag_idx):
                                if i == tp_score[best_tp_idx][2]:
                                    IEpretag['tag'][i] = 1
                                elif IEpretag['tag'][i] == 1:
                                    IEpretag['tag'][i] = -2
                            RR_now_list.append( tp_tf_sec
                                                -IEpretag['tisec'][tp_score[best_tp_idx][2]]  )
                            RR_now_score_list.append(tp_score[best_tp_idx][0])
                        tp_tf_idx = (tp_score[best_tp_idx][1]
                                     if IEpretag['tag'][tp_score[best_tp_idx][2]-1] != -1
                                     else IEpretag['tf_stft'][tp_score[best_tp_idx][2]-2])
                        tp_tf_sec = IEpretag['tisec'][tp_score[best_tp_idx][2]]
                        # quit if already checked the 1st seg
                        if not tp_score[best_tp_idx][2] or j == len(IEtagidx_list)-1:
                            break
                        ref_i = IEtagidx_list[(IEtagidx_list == tp_score[best_tp_idx][2]).nonzero()[0]-1]
                        if not ref_i.size or tp_score[best_tp_idx][0] > self.sbwin.config['Envelope']['th_model_score']:
                            break
                        print('------- next IE --------')
                        tp_f_IEpretag_idx = tp_score[best_tp_idx][2]
                    tp_score = []
                    # = define head of tp
                    for i in range(int(ref_i-2), min(int(ref_i+2),tp_f_IEpretag_idx-1)):
                        # skip negative or silence segment
                        if i < 0 or IEpretag['tag'][i] == -1:
                            continue
                        # = skip if tp is not IE0 but model is
                        if (    model[3] == 'IE0'
                                and
                                (np.count_nonzero(IEpretag['tag'][i:tp_f_IEpretag_idx] == 1)
                                 +
                                 np.count_nonzero(IEpretag['tag'][i:tp_f_IEpretag_idx] == -2)
                                 < 2)):
                            continue
                        # = stft idx of test pattern's start
                        tp_ti_idx = int(IEpretag['ti_stft'][i])
                        tp_ti_sec = IEpretag['tisec'][i]
                        tp_len_sec = tp_tf_sec - tp_ti_sec
                        # = test pattern(tp)
                        test_pattern = stft_ROI[:, tp_ti_idx:tp_tf_idx]
                        # = resample test pattern to equalize the length of tp and model
                        # t_start1 = time.perf_counter()
                        deno = min(model_pattern.shape[1], test_pattern.shape[1])
                        numerator = max(model_pattern.shape[1], test_pattern.shape[1])
                        tp_padzeros = False
                        model_padzeros = False
                        tp_re_list = []
                        # length ratio < threshold --> resample tp
                        if numerator / deno < self.sbwin.config['Envelope']['th_zoom_ratio_model_tp']:
                            # zoom
                            tp_re_list.append(signal.resample_poly(test_pattern,
                                                                 model_pattern.shape[1],
                                                                 test_pattern.shape[1],
                                                                 axis=1))
                            tp_re_list[-1] -= tp_re_list[-1].min()
                            # = add two more tp that are truncated from head and from end
                            #   try to avoid the inaccurate pattern range defined by envelope
                            if test_pattern.shape[1] > model_pattern.shape[1]:
                                tp_re_list.append(test_pattern[:,:model_pattern.shape[1]])
                                tp_re_list.append(test_pattern[:,-model_pattern.shape[1]:])
                            model_pattern_re = model_pattern.copy()
                        # length ratio >= threshold and model_len > tp_len --> pad zeros to tail of tp
                        elif model_pattern.shape[1] > test_pattern.shape[1]:
                            tp_re_list.append(np.pad(test_pattern,
                                                   ((0,0),
                                                    (0,model_pattern.shape[1]-test_pattern.shape[1]))))
                            model_pattern_re = model_pattern.copy()
                            tp_padzeros = True
                        # length ratio >= threshold and model_len < tp_len --> pad zeros to tail of model
                        else:
                            model_pattern_re = np.pad(model_pattern,
                                                      ((0,0),
                                                      (0,test_pattern.shape[1]-model_pattern.shape[1])))
                            tp_re_list.append(test_pattern)
                            model_padzeros = True
                        # t_end1 = time.perf_counter()
                        score_IE_norm_corr = 100.
                        score_IE_unnorm_corr = 100.
                        weight_norm_unnorm_IE_corr = self.sbwin.config['Envelope']['weight_norm_unnorm_IE_corr']
                        th_norm_unnorm_IE_corr = self.sbwin.config['Envelope']['th_norm_unnorm_IE_corr']
                        weight_corr_unnorm_model_tp = self.sbwin.config['Envelope']['weight_corr_unnorm_model_tp']
                        for test_pattern_re in tp_re_list:
                            # if tp_padzeros or model_padzeros:
                            #     cri_applied_final = 'H_'
                            #     test_pattern_re_final = test_pattern_re
                            #     norm_tp_model_corr_final = 1e-2
                            #     unnorm_tp_model_corr_final = 1e-2
                            #     norm_tp_autocorr_final = 1e-2
                            #     unnorm_tp_autocorr_final = 1e-2
                            #     IE_unnorm_corr_devi_final = 100.
                            #     IE_norm_corr_devi_final = 100.
                            #     continue
                            # = correlation between normalized model and normalized test pattern
                            # t_start = time.perf_counter()
                            norm_tp_model_corr = ((model_pattern_re/model_pattern_re.max()
                                                *test_pattern_re/test_pattern_re.max()).sum())
                            # = correlation between model and test pattern
                            unnorm_tp_model_corr = (model_pattern_re*test_pattern_re).sum()
                            # t_end = time.perf_counter()
                            # print((f'resampling {(t_end1-t_start1)*1000:.1f}ms  '
                            #        f'tp model correlation {(t_end-t_start)*1000:.1f}ms'))
                            # = auto-correlation of norm/unnorm tp
                            norm_tp_autocorr = (test_pattern/test_pattern.max()*test_pattern/test_pattern.max()).sum()
                            unnorm_tp_autocorr = (test_pattern*test_pattern).sum()
                            # === score of deviation between tp and model
                            # IE_norm_corr_devi = abs(norm_tp_model_corr.max()/max(norm_model_autocorr, norm_tp_autocorr)-1)
                            IE_norm_corr_devi_sqrt = abs(norm_tp_model_corr/(norm_model_autocorr*norm_tp_autocorr)**0.5-1)
                            IE_norm_corr_devi = max((abs(norm_tp_model_corr/norm_model_autocorr-1)
                                                     *weight_corr_unnorm_model_tp[0]
                                                     + abs(norm_tp_model_corr/norm_tp_autocorr-1)
                                                       *weight_corr_unnorm_model_tp[1]
                                                     if abs(norm_tp_model_corr/norm_model_autocorr-1)
                                                        /abs(norm_tp_model_corr/norm_tp_autocorr-1) < 3
                                                        and not tp_padzeros
                                                     else
                                                     abs(norm_tp_model_corr/norm_model_autocorr-1)
                                                     *weight_corr_unnorm_model_tp[1]
                                                     + abs(norm_tp_model_corr/norm_tp_autocorr-1)
                                                       *weight_corr_unnorm_model_tp[0]), IE_norm_corr_devi_sqrt)
                            IE_unnorm_corr_devi_sqrt = abs(unnorm_tp_model_corr
                                                           /np.sqrt(unnorm_model_autocorr
                                                                    *unnorm_tp_autocorr)-1)
                            IE_unnorm_corr_devi = max(( abs(unnorm_tp_model_corr/unnorm_model_autocorr-1)
                                                        *weight_corr_unnorm_model_tp[0]
                                                        + abs(unnorm_tp_model_corr/unnorm_tp_autocorr-1)
                                                         *weight_corr_unnorm_model_tp[1]
                                                        if abs(unnorm_tp_model_corr/unnorm_model_autocorr-1)
                                                           /abs(unnorm_tp_model_corr/unnorm_tp_autocorr-1) < 3
                                                           and not tp_padzeros
                                                        else
                                                        abs(unnorm_tp_model_corr/unnorm_model_autocorr-1)
                                                        *weight_corr_unnorm_model_tp[1]
                                                        + abs(unnorm_tp_model_corr/unnorm_tp_autocorr-1)
                                                        *weight_corr_unnorm_model_tp[0]), IE_unnorm_corr_devi_sqrt)
                            # == weight varies with conditions
                            cri_applied_final = None
                            # if (    IE_norm_corr_devi >= th_norm_unnorm_IE_corr[1][0]
                            #         or
                            #         IE_unnorm_corr_devi >= th_norm_unnorm_IE_corr[1][1]):
                            if (    (   IE_norm_corr_devi >= th_norm_unnorm_IE_corr[1][0]
                                        and IE_unnorm_corr_devi >= th_norm_unnorm_IE_corr[0][1])
                                    or
                                    (   IE_norm_corr_devi >= th_norm_unnorm_IE_corr[0][0]
                                        and IE_unnorm_corr_devi >= th_norm_unnorm_IE_corr[1][1])):
                                score_IE_norm_corr_tmp = weight_norm_unnorm_IE_corr[2][0]*IE_norm_corr_devi
                                score_IE_unnorm_corr_tmp = weight_norm_unnorm_IE_corr[2][1]*IE_unnorm_corr_devi
                                cri_applied = 'H' if not tp_padzeros and not model_padzeros else 'H_'
                            elif (  IE_norm_corr_devi >= th_norm_unnorm_IE_corr[0][0]
                                    or IE_unnorm_corr_devi >= th_norm_unnorm_IE_corr[0][1]):
                                score_IE_norm_corr_tmp = weight_norm_unnorm_IE_corr[1][0]*IE_norm_corr_devi
                                score_IE_unnorm_corr_tmp = weight_norm_unnorm_IE_corr[1][1]*IE_unnorm_corr_devi
                                cri_applied = 'M' if not tp_padzeros and not model_padzeros else 'M_'
                            else:
                                score_IE_norm_corr_tmp = weight_norm_unnorm_IE_corr[0][0]*IE_norm_corr_devi
                                score_IE_unnorm_corr_tmp = weight_norm_unnorm_IE_corr[0][1]*IE_unnorm_corr_devi
                                cri_applied = 'L' if not tp_padzeros and not model_padzeros else 'L_'
                            if score_IE_norm_corr_tmp + score_IE_unnorm_corr_tmp < score_IE_norm_corr + score_IE_unnorm_corr:
                                # if k:
                                # print(f'final tp idx = {k}\t'
                                #         f'original score = {score_IE_norm_corr + score_IE_unnorm_corr:.2f}\t'
                                #         f'cuurent score = {score_IE_norm_corr_tmp + score_IE_unnorm_corr_tmp:.2f}')
                                score_IE_norm_corr = score_IE_norm_corr_tmp
                                score_IE_unnorm_corr = score_IE_unnorm_corr_tmp
                                test_pattern_re_final = test_pattern_re.copy()
                                norm_tp_model_corr_final = norm_tp_model_corr
                                unnorm_tp_model_corr_final = unnorm_tp_model_corr
                                norm_tp_autocorr_final = norm_tp_autocorr
                                unnorm_tp_autocorr_final = unnorm_tp_autocorr
                                IE_norm_corr_devi_final = IE_norm_corr_devi
                                IE_unnorm_corr_devi_final = IE_unnorm_corr_devi
                                cri_applied_final = cri_applied
                        cri_applied_final = cri_applied if not cri_applied_final else cri_applied_final
                        if cri_applied_final == 'H_':
                            cri_applied_final = 'H_m' if model_padzeros else 'H_t'
                        elif cri_applied_final == 'M_':
                            cri_applied_final = 'M_m' if model_padzeros else 'M_t'
                        # if model[3] == 'I0' and IE_unnorm_corr_devi >= th_norm_unnorm_IE_corr[1][1]:
                        #     score_IE_unnorm_corr = weight_norm_unnorm_IE_corr[2][1]*IE_unnorm_corr_devi
                        # == score of duration deviation
                        weight_IE_len_devi = self.sbwin.config['Envelope']['weight_IE_len_devi']
                        th_IE_len_devi = self.sbwin.config['Envelope']['th_IE_len_devi']
                        IE_len_devi = abs(1-max(tp_len_sec,model_len_sec)/min(tp_len_sec,model_len_sec))
                        score_IE_len_devi = (   weight_IE_len_devi[1]*IE_len_devi 
                                                if IE_len_devi >= th_IE_len_devi
                                                else weight_IE_len_devi[0]*IE_len_devi  )
                        # === tp_score:
                        # 0: score
                        # 1: stft ti idx of test pattern's start
                        # 2: IEpretag idx of test pattern's start
                        w_corr = self.sbwin.config['Envelope']['w_corr_padzero'] if '_' in cri_applied_final else 1
                        tp_score.append([score_IE_norm_corr*w_corr
                                         + score_IE_unnorm_corr*w_corr
                                         + score_IE_len_devi
                                         , tp_ti_idx
                                         , i])
                        msg_add = '--> in IEtag list!' if i in IEtagidx_list else ''
                        if (    tp_score[-1][0] < self.sbwin.config['Envelope']['th_model_score']
                                and model_len_sec not in RR_now_list    ):
                            IEpretag['grade'][model[0]:model[0]+model[1]] = 2
                            RR_now_list.append(model_len_sec)
                        # Dist, _, _, _ = dtw(model_pattern/np.max(model_pattern), 
                        #                     self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]/np.max(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]),
                        #                     dist=lambda x, y: np.abs(x - y))
                        msg_score = (f"tp(s)\tnorm auto/corr/devi(_model)(_tp)(_sqrt)(final)\t"
                            f"unnorm auto/corr/devi(_model)(_tp)(_sqrt)(final)\t"
                            f"width devi\tscore"
                            f"{IEpretag['tisec'][i]:4.1f}~{tp_tf_sec:4.1f}\t"
                            f'{norm_tp_autocorr_final:.0f} / {norm_tp_model_corr_final:.0f} / '
                            f'({norm_tp_model_corr_final/norm_model_autocorr-1:+.2f})'
                            f'({norm_tp_model_corr_final/norm_tp_autocorr_final-1:+.2f})'
                            f'({norm_tp_model_corr_final/(norm_model_autocorr*norm_tp_autocorr_final)**0.5-1:+.2f})'
                            f'({IE_norm_corr_devi_final:.2f})\t'
                            f'{unnorm_tp_autocorr_final:.0f} / {unnorm_tp_model_corr_final:.0f} / '
                            f'({unnorm_tp_model_corr_final/unnorm_model_autocorr-1:+.2f})'
                            f'({unnorm_tp_model_corr_final/unnorm_tp_autocorr_final-1:+.2f})'
                            f'({unnorm_tp_model_corr_final/(unnorm_model_autocorr*unnorm_tp_autocorr_final)**0.5-1:.2f})'
                            f'({IE_unnorm_corr_devi_final:.2}) {cri_applied_final}\t\t'
                            f'{tp_len_sec/model_len_sec-1:+.2f}\t\t'
                            f'{tp_score[-1][0]:.2f}{msg_add}')
                        if not print_score_headline:
                            print_score_headline = True
                            lastidx = msg_score.find('score')+5
                            print(msg_score[:lastidx])
                        print(msg_score[lastidx:])
                        if self.sbwin.config['Envelope']['plot_IEtag_corr']:
                            fig = plt.figure(figsize=(9.5,8))
                            ax = fig.subplots(3,1)
                            ax[0].pcolormesh(model_pattern_re) #, vmin=0, vmax=5)
                            ax[0].set_title(f'model:'
                                               f"{IEpretag['tisec'][model[0]]:.1f}~"
                                               f"{IEpretag['tfsec'][min(model[0]+model[1]-1,IEpretag.size-1)]:.1f}sec")
                            ax[1].pcolormesh(test_pattern)    #, vmin=0, vmax=5)
                            ax[1].set_title(f'test pattern:'
                                                f'{tp_ti_sec:4.1f}~{tp_tf_sec:4.1f}sec')
                            ax[2].pcolormesh(test_pattern_re_final) #, vmin=0, vmax=5)
                            ax[2].set_title(f'resampled test pattern:'
                                                f'{tp_ti_sec:4.1f}~{tp_tf_sec:4.1f}sec')
                            msg_score = (msg_score.replace(' ','').replace('\t\t\t','<>')
                                                                  .replace('\t\t','<>')
                                                                  .replace('\t','<>')
                                                                  .replace('score','score\n'))
                            fig.suptitle((f"{os.path.basename(self.sbwin.fnnow)}"
                                          f'  norm/unnorm autocorr = {norm_model_autocorr:.1f} / {unnorm_model_autocorr:.1f}\n'
                                          f"{msg_score}"),
                                        x=0.02, y=0.98, ha='left',
                                        fontproperties=chinese_font,
                                        fontsize=12)
                                    #   f'  dtw = {Dist:.3f}')
                            plt.tight_layout(rect=(0.0,0.0,1,0.93))
                            plt.show()
                            plt.close()
        # # === examine how match between patterns and models and calcuate RR by envelope correlation
        # elif self.sbwin.config['Envelope']['goIEmodel_corr']:
        #     if model[0] != -1 and self.sbwin.config['Envelope']['goIEmodel_corr']:
        #         # = model info
        #         model_ti_idx = int(IEpretag['ti_envelope'][model[0]])
        #         model_tf_idx = int(IEpretag['tf_envelope'][min(model[0]+model[1]-1,IEpretag.size-1)])
        #         model_len_sec = self.sbwin.t_envelope_sec[model_tf_idx] - self.sbwin.t_envelope_sec[model_ti_idx]
        #         model_profile = self.sbwin.envelope_RR[model_ti_idx:model_tf_idx]
        #         t_start = time.perf_counter()
        #         norm_model_autocorr = np.correlate( model_profile/np.max(model_profile),
        #                                             model_profile/np.max(model_profile)).max()
        #         unnorm_model_autocorr = np.correlate(model_profile, model_profile).max()
        #         t_end = time.perf_counter()
        #         print((f'evaluating model '
        #             f"{IEpretag['tisec'][model[0]]:.1f}~{IEpretag['tfsec'][min(model[0]+model[1]-1,IEpretag.size-1)]:.1f}sec,"
        #             f' len = {model[1]}, type:{model[3]},'
        #             f' Normalized/Unnormalized Auto-Correlation = {norm_model_autocorr:4.1f}'
        #             f' / {unnorm_model_autocorr:4.1f}\n'
        #             f' consumed time = {(t_end-t_start)*1000:.1f}ms'))
        #         # = scan IE sets to find matched IE
        #         for j in range(len(IEtagidx_list)):
        #             if j == 0:
        #                 if not (IEtagidx_list == model[0]).nonzero()[0]:
        #                     break
        #                 # envelope idx of test pattern's end
        #                 tp_tf_idx = model_ti_idx
        #                 # timestamp(sec) of envelope idx of test pattern's end
        #                 tp_tf_sec = self.sbwin.t_envelope_sec[tp_tf_idx] + self.sbwin.ti
        #                 # idx of IEpretag NEXT to test pattern's end
        #                 tp_f_IEpretag_idx = model[0]
        #                 # a reference in IEtagidx_list idx for searching test pattern
        #                 ref_i = IEtagidx_list[(IEtagidx_list == model[0]).nonzero()[0]-1]
        #             else:
        #                 best_tp_idx = np.argmin(np.array(tp_score)[:,0])
        #                 tp_tf_idx = tp_score[best_tp_idx][1]
        #                 tp_tf_sec = self.sbwin.t_envelope_sec[tp_tf_idx] + self.sbwin.ti
        #                 if tp_score[best_tp_idx][0] < self.sbwin.config['Envelope']['th_model_score']:
        #                     IEpretag['grade'][tp_score[best_tp_idx][2]:tp_f_IEpretag_idx] = 2
        #                     RR_now_list.append( IEpretag['tisec'][tp_f_IEpretag_idx]
        #                                         -IEpretag['tisec'][tp_score[best_tp_idx][2]])
        #                 # quit if already checked the 1st seg
        #                 if not tp_score[best_tp_idx][2] or j == len(IEtagidx_list)-1:
        #                     break
        #                 ref_i = IEtagidx_list[(IEtagidx_list == tp_score[best_tp_idx][2]).nonzero()[0]-1]
        #                 if not ref_i.size or tp_score[best_tp_idx][0] > self.sbwin.config['Envelope']['th_model_score']:
        #                     # == correct IEpretag of pattern prior to model
        #                     if tp_score[best_tp_idx][0] < self.sbwin.config['Envelope']['th_model_score']:
        #                         for i in range(tp_score[best_tp_idx][2],tp_f_IEpretag_idx):
        #                             if i == tp_score[best_tp_idx][2]:
        #                                 IEpretag[i,0] = 1
        #                             elif IEpretag[i,0] == 1:
        #                                 IEpretag[i,0] = -2
        #                     break
        #                 print('------- next IE --------')
        #                 tp_f_IEpretag_idx = tp_score[best_tp_idx][2]
        #             tp_score = []
        #             for i in range(int(ref_i-1), min(int(ref_i+2),tp_f_IEpretag_idx-1)):
        #                 # skip negative and silence segment
        #                 if i < 0 or IEpretag['tag'][i] == -1:
        #                     continue
        #                 # = envelope idx of test pattern's start
        #                 tp_ti_idx = int(IEpretag[i][3])
        #                 # = resample test pattern to equalize the length of tp and model
        #                 tp_re = signal.resample_poly(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx], model_profile.size, tp_tf_idx-tp_ti_idx)
        #                 # = correlation between normalized model and normalized test pattern
        #                 norm_tp_model_corr = np.correlate(model_profile/model_profile.max(),
        #                                                self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]
        #                                                     /self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx].max())
        #                 # norm_tp_model_corr = np.correlate(model_profile/np.max(model_profile),
        #                 #                                tp_re/np.max(tp_re))
        #                 # correlation between model and test pattern
        #                 unnorm_tp_model_corr = np.correlate(model_profile,
        #                                                  self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx])
        #                 # unnorm_tp_model_corr = np.correlate(model_profile, tp_re)[0]
        #                 # norm_tp_autocorr = np.correlate(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]
        #                 #                                     /np.max(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]),
        #                 #                                 self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]
        #                 #                                     /np.max(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx])).max()
        #                 # = auto-correlation of norm/unnorm tp
        #                 norm_tp_autocorr = np.correlate(tp_re/np.max(tp_re), tp_re/np.max(tp_re))[0]
        #                 # unnorm_tp_autocorr = np.correlate(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx],
        #                 #                                   self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]).max()
        #                 unnorm_tp_autocorr = np.correlate(tp_re, tp_re)[0]
        #                 tp_len_sec = self.sbwin.t_envelope_sec[tp_tf_idx] - self.sbwin.t_envelope_sec[tp_ti_idx]
        #                 # == score of correlation deviation
        #                 weight_norm_unnorm_IE_corr = self.sbwin.config['Envelope']['weight_norm_unnorm_IE_corr']
        #                 th_norm_unnorm_IE_corr = self.sbwin.config['Envelope']['th_norm_unnorm_IE_corr']
        #                 weight_corr_unnorm_model_tp = self.sbwin.config['Envelope']['weight_corr_unnorm_model_tp']
        #                 # IE_norm_corr_devi = abs(norm_tp_model_corr.max()/max(norm_model_autocorr, norm_tp_autocorr)-1)
        #                 IE_norm_corr_devi = abs(max(norm_tp_model_corr[0],norm_tp_model_corr[-1])/max(norm_model_autocorr, norm_tp_autocorr)-1)
        #                 # IE_unnorm_corr_devi = abs(  unnorm_tp_model_corr.max()
        #                 #                             /np.sqrt(unnorm_model_autocorr
        #                 #                                     *unnorm_tp_autocorr)-1  )
        #                 # IE_unnorm_corr_devi = ( abs(unnorm_tp_model_corr.max()/unnorm_model_autocorr-1)*weight_corr_unnorm_model_tp[0]
        #                 #                         + abs(unnorm_tp_model_corr.max()/unnorm_tp_autocorr-1)*weight_corr_unnorm_model_tp[1])
        #                 IE_unnorm_corr_devi = ( abs(max(unnorm_tp_model_corr[0], unnorm_tp_model_corr[-1])/unnorm_model_autocorr-1)*weight_corr_unnorm_model_tp[0]
        #                                         + abs(max(unnorm_tp_model_corr[0], unnorm_tp_model_corr[-1])/unnorm_tp_autocorr-1)*weight_corr_unnorm_model_tp[1])
        #                 # == 
        #                 if IE_norm_corr_devi >= th_norm_unnorm_IE_corr[1][0] or IE_unnorm_corr_devi >= th_norm_unnorm_IE_corr[1][1]:
        #                     score_IE_norm_corr = weight_norm_unnorm_IE_corr[2][0]*IE_norm_corr_devi
        #                     score_IE_unnorm_corr = weight_norm_unnorm_IE_corr[2][1]*IE_unnorm_corr_devi
        #                 elif IE_norm_corr_devi >= th_norm_unnorm_IE_corr[0][0] or IE_unnorm_corr_devi >= th_norm_unnorm_IE_corr[0][1]:
        #                     score_IE_norm_corr = weight_norm_unnorm_IE_corr[1][0]*IE_norm_corr_devi
        #                     score_IE_unnorm_corr = weight_norm_unnorm_IE_corr[1][1]*IE_unnorm_corr_devi
        #                 else:
        #                     score_IE_norm_corr = weight_norm_unnorm_IE_corr[0][0]*IE_norm_corr_devi
        #                     score_IE_unnorm_corr = weight_norm_unnorm_IE_corr[0][1]*IE_unnorm_corr_devi
        #                 if model[3] == 'I0' and IE_unnorm_corr_devi >= th_norm_unnorm_IE_corr[1][1]:
        #                     score_IE_unnorm_corr = weight_norm_unnorm_IE_corr[2][1]*IE_unnorm_corr_devi
        #                 # == score of duration deviation
        #                 weight_IE_len_devi = self.sbwin.config['Envelope']['weight_IE_len_devi']
        #                 th_IE_len_devi = self.sbwin.config['Envelope']['th_IE_len_devi']
        #                 IE_len_devi = abs(1-tp_len_sec/model_len_sec)
        #                 score_IE_len_devi = (   weight_IE_len_devi[1]*IE_len_devi 
        #                                         if IE_len_devi >= th_IE_len_devi
        #                                         else weight_IE_len_devi[0]*IE_len_devi  )
        #                 # === tp_score: score, envelope idx of test pattern's start, IEpretag idx of test pattern's start
        #                 tp_score.append([score_IE_norm_corr
        #                                     + score_IE_unnorm_corr
        #                                     + score_IE_len_devi
        #                                 , tp_ti_idx
        #                                 , i])
        #                 msg_add = '--> in model list!' if i in IEtagidx_list else ''
        #                 if (    tp_score[-1][0] < self.sbwin.config['Envelope']['th_model_score']
        #                         and model_len_sec not in RR_now_list    ):
        #                     IEpretag[model[0]:model[0]+model[1],-1] = 2
        #                     RR_now_list.append(model_len_sec)
        #                 # Dist, _, _, _ = dtw(model_profile/np.max(model_profile), 
        #                 #                     self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]/np.max(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]),
        #                 #                     dist=lambda x, y: np.abs(x - y))
        #                 print(f' tp({IEpretag[i][1]:4.1f}~{tp_tf_sec:4.1f}s)'
        #                     f'(autocorr={norm_tp_autocorr.max():4.1f}/{unnorm_tp_autocorr:4.1f})'
        #                     f' norm/unnorm correlation = {norm_tp_model_corr.max():4.1f}'
        #                     f'({norm_tp_model_corr.max()/norm_model_autocorr-1:5.2f})'
        #                     f' / {unnorm_tp_model_corr.max():4.1f}'
        #                     f'({unnorm_tp_model_corr.max()/unnorm_model_autocorr-1:+5.2f})'
        #                     f'({unnorm_tp_model_corr.max()/unnorm_tp_autocorr-1:+5.2f})'
        #                     f'({IE_unnorm_corr_devi:4.2})'
        #                     f' duration = {tp_len_sec/model_len_sec:.2f}({tp_len_sec/model_len_sec-1:+6.2f})'
        #                     f' score = {tp_score[-1][0]:.3f}{msg_add}')
        #                     #   f' (dtw = {Dist:.3f})')
        #                 if self.sbwin.config['Envelope']['plot_IEtag_corr']:
        #                     fig, ax = plt.subplots(2,1)
        #                     ax[0].plot(model_profile/np.max(model_profile), 
        #                                 label=(f'model:'
        #                                        f'{IEpretag[model[0]][1]:.1f}~'
        #                                        f'{IEpretag[min(model[0]+model[1]-1,IEpretag.size-1)][2]:.1f}sec'))
        #                     ax[0].plot(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]/np.max(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx]),
        #                                 label=(f'test pattern:'
        #                                         f'{IEpretag[i][1]:4.1f}~{tp_tf_sec:4.1f}sec'))
        #                     ax[0].plot(tp_re/np.max(tp_re),
        #                                 label=(f're test pattern:'
        #                                         f'{IEpretag[i][1]:4.1f}~{tp_tf_sec:4.1f}sec'))
        #                     ax[1].plot(model_profile, label=f'model')
        #                     ax[1].plot(self.sbwin.envelope_RR[tp_ti_idx:tp_tf_idx],
        #                                 label=f'test pattern')
        #                     ax[1].plot(tp_re,
        #                                 label=f're test pattern')
        #                     ax[0].legend()
        #                     ax[1].legend()
        #                     fig.suptitle(f' correlation = {norm_tp_model_corr.max():4.1f}'
        #                                 f'({norm_tp_model_corr.max()/norm_model_autocorr-1:5.2f})'
        #                                 f'\nduration ratio = {tp_len_sec/model_len_sec:.2f}'
        #                                 f'({tp_len_sec/model_len_sec-1:+6.2f})'
        #                                 f'\nscore = {tp_score[-1][0]:.3f}{msg_add}',
        #                                 fontproperties=chinese_font)
        #                             #   f'  dtw = {Dist:.3f}')
        #                     plt.show()
        #                     plt.close()
        # === calculate RR and silence
        print(f'RR_sec now: {np.round(np.array(RR_now_list),2)}')
        print(f'RR_now_score: {np.round(np.array(RR_now_score_list),4)}  mean={np.round(np.mean(RR_now_score_list),4)}')
        weight_RRnow = self.sbwin.config['Envelope']['weight_RRnow']
        if len(RR_now_list):
            self.sbwin.RR_sec = ( self.sbwin.RR_sec * (1 - weight_RRnow)
                            + np.average(RR_now_list) * weight_RRnow
                            if self.sbwin.RR_sec
                            else np.average(RR_now_list))
            self.sbwin.RR_lowQ_cnt = 0
        else:
            self.sbwin.RR_lowQ_cnt += 1
        if model[-1] != 'nonie' and model[-1] != 'IE':
            self.sbwin.silence_start = IEpretag[model[0]+model[1]-1][1]
            silence_end = IEpretag[model[0]+model[1]-1][2]
        elif model[-1] == 'IE':
            self.sbwin.silence_start = IEpretag[model[0]+model[1]-1][2]
            silence_end = IEpretag[model[0]+model[1]-1][2]
        else:   # no model was found
            silence_end = self.sbwin.tf
        if self.sbwin.RR_sec == None or self.sbwin.RR_lowQ_cnt > 3:
            self.sbwin.RR_sec = 0
        # = display RR info
        msg_tmp = ( (   f'RR = {self.sbwin.RR_sec:.2f} sec({60/self.sbwin.RR_sec:.1f} BPM)\n'
                        f'silence = {silence_end - self.sbwin.silence_start:.1f} sec\n'
                        f'RR_lowQ_cnt = {self.sbwin.RR_lowQ_cnt}')
                    if self.sbwin.RR_sec
                    else
                    (   f'RR = 0 sec(0 BPM)\n'
                        f'silence = {silence_end - self.sbwin.silence_start:.1f} sec\n'
                        f'RR_lowQ_cnt = {self.sbwin.RR_lowQ_cnt}'))
        p1txt_RR = pg.TextItem(msg_tmp, color=(0,255,255,255), anchor=(0,0))
        self.sbwin.p1.addItem(p1txt_RR)
        p1txt_RR.setPos(self.sbwin.duration*.75,self.sbwin.Spectfmax.value()*0.7)
        # === presume IE tag from head
        IEtag = [[] for i in range(len(IEpretag))]
        for i in range(len(IEpretag)):
            if not IEtag[i]:
                if IEpretag['tag'][i] == -1:
                    IEtag[i] = ['Unknown', IEpretag['tisec'][i], IEpretag['tfsec'][i]]
                elif IEpretag['tag'][i] == 1:
                    IEtag[i] = (['Inspiration_Golden', IEpretag['tisec'][i], IEpretag['tfsec'][i]]
                                if IEpretag[i][-1] == 2
                                else ['Inspiration_Grade1', IEpretag['tisec'][i], IEpretag['tfsec'][i]])
                elif IEpretag['tag'][i] == -2:
                    IEtag[i] = (['Expiration_Golden', IEpretag['tisec'][i], IEpretag['tfsec'][i]]
                                if IEpretag[i][-1] == 2
                                else ['Expiration_Grade1', IEpretag['tisec'][i], IEpretag['tfsec'][i]])
        IEtag = np.array(IEtag)
        self.sbwin.preTagPosData = [[] for i in range(len(self.sbwin.TagPosData))]
        for tag in IEtag:
            if not len(tag):
                continue
            for idx in self.sbwin.TagTypIdx:
                if tag[0] == self.sbwin.tagName[idx]:
                    self.sbwin.preTagPosData[idx].append((tag[1], tag[2]))
                    # print(f'loading previous tag data...\n{self.sbwin.tagName[idx]}  {self.sbwin.preTagPosData[idx]}')
        self.sbwin.show_my_pretag = True
        if model[0] != -1:
            QtCore.QTimer.singleShot(500, partial(self.sbwin.p1region.setRegion,
                                                    (IEpretag[model[0]][1]-self.sbwin.ti,
                                                    IEpretag[model[0]+model[1]-1][2]-self.sbwin.ti)))
        return IEtag


    def getEnvelopWAV(self, sndData, winL, noverlap, n_fft):
        print('')
        if max(sndData) == 0:
            print(f'skip getEnvelop due to zero input')
            return [],[],[],[]
        # === parameters of spectrogram
        winL_mySmooth = winL    # librosa fft size = maplotlib NFFT
        n_fft_mySmooth = n_fft if winL <= n_fft else winL  # librosa n_fft = matplotlib pad_to
        stride_mySmooth = round((winL-noverlap))  # librosa hop_legnth (stride) = matplotlib (NFFT-noverlap)
        # ====== image processing spectrogram
        # ti_librosa = time.perf_counter()
        sndData = (np.asfortranarray(sndData)
                    if "filtWAV" in self.sbwin.config.keys()
                        and len(self.sbwin.config['filtWAV']['fcut'])
                    else sndData)
        stft = librosa.stft(sndData, hop_length=stride_mySmooth, win_length=winL_mySmooth ,n_fft=n_fft_mySmooth)
        stft_abs = np.abs(stft)
        # === get wav intensity per sec
        N_filt = 3
        nyq = self.sbwin.sr/2
        wn = np.array((80,500))
        # self.sbwin.debugMsg(f'wn  {wn}')
        b_butter, a_butter = signal.butter(N_filt, wn/nyq, btype='bandpass')
        sndData_filtered = signal.filtfilt(b_butter, a_butter, sndData)
        Intensity_wav = np.sum(np.power(sndData_filtered,2))/self.sbwin.duration
        # plt.pcolormesh(np.log10(np.where(stft_abs!=0, stft_abs, 1e-20)), cmap='afmhot', \
        #     vmin=self.sbwin.config['Spectrogram']['vmin'], vmax=self.sbwin.config['Spectrogram']['vmax'])
        # plt.show()
        # stft_ang = np.angle(stft)
        # tf_libraosa = time.perf_counter()
        # self.sbwin.debugMsg(f'stft time: {tf_libraosa-ti_librosa}')
        # the freqency(Hz) to decide each frequency band
        freqband_node_Hz = self.sbwin.config['Envelope']['freqband_node_Hz'] if self.sbwin.isvip else [80,500]
        # the freqency(idx) to decide each frequency band
        freqband_node_idx = [round(freqband_node_Hz[i]*stft_abs.shape[0]/2000) for i in range(len(freqband_node_Hz))]
        freqband_weight = self.sbwin.config['Envelope']['freqband_weight'] if self.sbwin.isvip else [1]
        if self.sbwin.isvip:
            print(f'stft shape in EnvelopWAV:{stft_abs.shape}')
        # ====== smooth by median filter
        Vsize_mySmooth = [int(round(2000/385*self.sbwin.config['Envelope']['medfilt_kernel_Vsize'][0]/2000*stft_abs.shape[0])),1] \
            if self.sbwin.isvip else \
                [round(2000/385*31/2000*stft_abs.shape[0]),1]
        Hsize_mySmooth = [1,int(round(15/469*self.sbwin.config['Envelope']['medfilt_kernel_Hsize'][1]/self.sbwin.duration*stft_abs.shape[1]))] \
            if self.sbwin.isvip else \
                [1,round(15/469*7/self.sbwin.duration*stft_abs.shape[1])]
        if Vsize_mySmooth[0] % 2 == 0:
            Vsize_mySmooth[0] += 1
        if Hsize_mySmooth[1] % 2 == 0:
            Hsize_mySmooth[1] += 1
        if self.sbwin.isvip:
            go_medfilt = self.sbwin.config['Envelope']['go_medfilt']
            msg_tmp = f'run median filter? {go_medfilt}\n'
            msg_tmp += (f'median filter kernel size  V:{self.sbwin.sr/2/stft_abs.shape[0]*Vsize_mySmooth[0]:.1f}Hz '
                      f'H:{self.sbwin.duration/stft_abs.shape[1]*Hsize_mySmooth[1]:.3f}sec')
            msg_tmp += f"\nEnvelope data from "
            for i, w in enumerate(freqband_weight):
                if w:
                    msg_tmp += f"\n\t{freqband_node_Hz[i]:4}~{freqband_node_Hz[i+1]:4}Hz  weight={w:3}"
            print(msg_tmp)
            # p1txt_medfilt_size = pg.TextItem(msg_tmp, color=(255,255,50,200), anchor=(0,0))
            # self.sbwin.p1.addItem(p1txt_medfilt_size)
            # p1txt_medfilt_size.setPos(self.sbwin.duration*.6,self.sbwin.Spectfmax.value()*0.94)
        else:
            go_medfilt = True
        stft_abs_medfilt = signal.medfilt2d(stft_abs, Vsize_mySmooth) if go_medfilt else stft_abs
        stft_abs_medfilt = signal.medfilt2d(stft_abs_medfilt, Hsize_mySmooth) if go_medfilt else stft_abs_medfilt
        # ====== get intensity profile of each frequency band and process
        # how long(idx) to be sum
        time_len = int(max(round(self.sbwin.config['Envelope']['freqband_timebin_len_sec']*stft_abs.shape[1]/self.sbwin.duration),1)) \
            if self.sbwin.isvip else round(0.4*stft_abs.shape[1]/self.sbwin.duration)
        # time_len = (int(max(round(0.3*stft_abs.shape[1]/self.sbwin.duration), time_len))
        #             if (Intensity_wav < 0.01 or Intensity_wav > 90)
        #             else time_len) # try to smooth envelope if intensity is low
        # how far(idx) a step
        time_stride = ( int(max(round(   self.sbwin.config['Envelope']['freqband_timebin_stride_sec']
                                        *stft_abs.shape[1]/self.sbwin.duration),1))
                        if self.sbwin.isvip else 1)
        sum_freqband = [[] for i in range(len(freqband_node_idx)-1)]  # intensity profile of each frequency band
        # p1txt_corr = []     # correlation coefficient between frequency bands
        t_envelope_sec = []  # time series data(sec) for plot in p1
        # t_freqbin_idx = []  # time series data(idx)
        # ====== options of apply medfilt and log scale
        if self.sbwin.isvip:
            stft_abs_tmp = (stft_abs_medfilt
                            if go_medfilt
                            else stft_abs)
            spectro_data = (np.log10(np.where(stft_abs_tmp!=0, stft_abs_tmp, 1e-20))
                            if self.sbwin.config['Envelope']['isProfileLogScale']
                            else stft_abs_tmp)
        else:
            stft_abs_tmp = stft_abs_medfilt
            spectro_data = np.log10(np.where(stft_abs_tmp!=0, stft_abs_tmp, 1e-20))       
        # ====== calculate intensity profile along frequency bands   
        corr_freqband = []       
        for i in range(len(freqband_node_idx)-1):
            # == get intensity profile
            ti = 0
            tf = ti+time_len
            while tf < stft_abs.shape[1]:
                sum_freqband[i].append(np.sum(spectro_data[freqband_node_idx[0+i]
                                                            :freqband_node_idx[1+i]
                                                           ,ti:tf]))
                if i == 0:
                    t_envelope_sec.append((ti+tf)/2/(stft_abs.shape[1]/self.sbwin.duration))
                    # t_freqbin_idx.append([ti,tf])
                ti += time_stride
                tf += time_stride
            # == plot intensity profile from sum of freqbands
            if self.sbwin.isvip and self.sbwin.config['Envelope']['isPlotfreqband']:
                 # scale and offset envelope of each frequency band
                fact_tmp = (freqband_node_Hz[i+1]-freqband_node_Hz[i]) / (max(sum_freqband[i])-min(sum_freqband[i]))
                sum_freqband[i] = fact_tmp * (np.asfarray(sum_freqband[i]-min(sum_freqband[i]))) + freqband_node_Hz[i]
                self.sbwin.p1.plot(t_envelope_sec, sum_freqband[i])
            if self.sbwin.isvip and i:
                if Intensity_wav > self.sbwin.config['Envelope']['Isum_th']:
                    corr_freqband.append(np.corrcoef(sum_freqband[i-1], sum_freqband[i])[0,1])
                    print(f'correlation coeff between '
                        f'"{freqband_node_Hz[i-1]:4d}~{freqband_node_Hz[i]:4d}Hz" and '
                        f'"{freqband_node_Hz[i]:4d}~{freqband_node_Hz[i+1]:4d}Hz" = {corr_freqband[-1]:.3f}')
                else:
                    corr_freqband.append(0)
        # freqband_weight = freqband_weight * np.block([1,np.array(corr_freqband) > 0.74])
        if self.sbwin.isvip:
            print(f'final freqband weight={freqband_weight}')
        # === get envelope data
        envelope = np.zeros(np.shape(sum_freqband)[1])
        for i, weight in enumerate(freqband_weight):
            envelope += np.array(sum_freqband[i])*weight
        if self.sbwin.isvip:
            # == applying filter
            wn = np.array(self.sbwin.config['Envelope']['filter_Hz'])
            if any(wn) and \
                    (('band' in self.sbwin.config['Envelope']['filter_typ'] and len(wn)==2) or 
                    ('band' not in self.sbwin.config['Envelope']['filter_typ'] and len(wn)==1)):
                N_filt = self.sbwin.config['Envelope']['filter_N']
                nyq = len(envelope)/self.sbwin.duration/2
                if np.max(wn) > nyq:
                    wn[np.argmax(wn)] = nyq*0.99
                b_butter, a_butter = signal.butter(N_filt, wn/nyq, btype=self.sbwin.config['Envelope']['filter_typ'])
                envelope = signal.filtfilt(b_butter, a_butter, envelope)
            else:
                print(f'wrong filter arguments')
        # == segments to evaluate level and stability of audio's volume
        num_split = int(5 * self.sbwin.duration / 15)
        length_tmp = len(envelope) // num_split
        # == zeroing
        local_mini = [np.min(envelope[length_tmp*j:length_tmp*(j+1)]) for j in range(num_split)]
        # = clip envelope
        height_base = np.median(local_mini)
        envelope -= height_base
        envelope = np.where(envelope < 0, 0, envelope)
        # ===== auto-scaling
        # = peak height of all segments        
        local_maxi = [np.max(envelope[length_tmp*j:length_tmp*(j+1)])
                        for j in range(num_split)]
        # = height to normalize envelope
        norm_height = np.median(local_maxi)
        if self.sbwin.isvip:
            isquiet = norm_height < self.sbwin.config['Envelope']['quiet_norm_height']
            # === check if volume is stable enough
            # 2nd highest maxima > 2.1 * lowest maxima ==> unstable
            # unstable --> apply UL and LL to norm_height
            stable_ratio = self.sbwin.config['Envelope']['stable_ratio_local_maxi']
            # == absolute threshold
            abs_th_low_vol = self.sbwin.config['Envelope']['abs_th_low_vol']
            stable_local_maxi = (   False if (  np.median(local_maxi) >= abs_th_low_vol
                                                and len(local_maxi) > 1
                                                and sorted(local_maxi)[-2] / min(local_maxi) > stable_ratio)
                                            or 
                                            (   np.median(local_maxi) < abs_th_low_vol
                                                and len(local_maxi) > 1
                                                and max(local_maxi) / min(local_maxi)
                                                    > stable_ratio)
                                    else True   )
            # = if unstable, take the min volume for gain target
            # in the case of extream high signal and normal one,
            #   it helps to detect the normal one (not to be suppressed)
            # in the case of only voice (no IE),
            #   the final gain could be clampped by norm_height_range
            #   so that final envelope will be
            norm_height = ( min(local_maxi)
                            if not stable_local_maxi else norm_height)
            # ===== clamp norm_height
            LL_norm_height = self.sbwin.config['Envelope']['norm_height_range'][0]
            UL_norm_height = self.sbwin.config['Envelope']['norm_height_range'][1]
            # == absolute threshold
            low_vol_warning_abs = True if norm_height <= abs_th_low_vol else False
            # low_vol_warning_abs = True if norm_height <= quiet_norm_height else False
            # == relative threshold
            # = theshold of ratio of current norm_height to reference norm_height(max of local_maxi or movmean of norm_height)
            th_norm_height_ratio = self.sbwin.config['Envelope']['th_norm_height_ratio']      
            # if 2 or more segments at low volume were found in 15sec
            # reference1: max of current clip 
            low_vol_threshold_now = max(local_maxi) * th_norm_height_ratio
            low_vol_warning_now = (np.count_nonzero(np.array(local_maxi)
                                                    < low_vol_threshold_now)
                                    > 1)
            # reference2: moving average of norm_height
            if self.sbwin.movmen_norm_height_RR:
                low_vol_threshold_movmean = self.sbwin.movmen_norm_height_RR * th_norm_height_ratio
                low_vol_warning_movmean = np.count_nonzero(np.array(local_maxi) < low_vol_threshold_movmean) > 1
                self.sbwin.low_vol_warning = low_vol_warning_now or low_vol_warning_movmean or low_vol_warning_abs
            else:
                self.sbwin.low_vol_warning = low_vol_warning_now or low_vol_warning_abs        
            # == calculation of scale of envelope
            weight_old = (  (self.sbwin.config['Envelope']['movmean_len_norm_height'] - 1)
                            / self.sbwin.config['Envelope']['movmean_len_norm_height'])
            # case: normal volume with valid self.sbwin.movmen_norm_height_RR
            if (    self.sbwin.movmen_norm_height_RR
                    and not self.sbwin.low_vol_warning ):
                # moving average of envelope height target for RR calculation
                self.sbwin.movmen_norm_height_RR = weight_old*self.sbwin.movmen_norm_height_RR + (1-weight_old)*norm_height
                # envelope_scale = self.sbwin.config['Envelope']['height_target'] / self.sbwin.movmen_norm_height_RR
                msglowvol = f'normal volume with valid movmean_norm_height'
            # case: normal volume w/o valid self.sbwin.movmen_norm_height_RR
            elif (  norm_height
                    and not self.sbwin.low_vol_warning
                    and not self.sbwin.movmen_norm_height_RR):
                self.sbwin.movmen_norm_height_RR = norm_height
                # envelope_scale = self.sbwin.config['Envelope']['height_target'] / self.sbwin.movmen_norm_height_RR
                msglowvol = f'normal volume w/o valid movmean_norm_height!'
            # case: low volume with valid self.sbwin.movmen_norm_height_RR
            elif (  (   not norm_height
                        or self.sbwin.low_vol_warning  )
                    and self.sbwin.movmen_norm_height_RR):
                norm_height = self.sbwin.movmen_norm_height_RR
                # envelope_scale = self.sbwin.config['Envelope']['height_target'] / self.sbwin.movmen_norm_height_RR
                msglowvol = f'low volume with valid movmean_norm_height!'
            # case: low volume w/o valid self.sbwin.movmen_norm_height_RR
            else:
                norm_height = norm_height if norm_height else np.max(local_maxi)
                # envelope_scale = self.sbwin.config['Envelope']['height_target'] / norm_height
                msglowvol = f'low volume w/o valid movmean_norm_height!'
            # envelope for RR calculation
            norm_height = ( LL_norm_height
                            if  norm_height < LL_norm_height
                                and not stable_local_maxi
                                or isquiet
                            else norm_height)
            norm_height = (UL_norm_height
                            if norm_height > UL_norm_height
                                and not stable_local_maxi
                            else norm_height)
            self.sbwin.envelope_RR = np.array(envelope) / norm_height
        # scaling envelope for plotting
        envelope_scale = self.sbwin.config['Envelope']['height_target'] / norm_height
        envelope *= envelope_scale
        # self.sbwin.debugMsg(fenvelope_scale'gain of envelope = {envelope_scale}')
        if self.sbwin.isvip:
            print(f'time_len={time_len}pixel({time_len/stft_abs.shape[1]*self.sbwin.duration:.2f}sec)\t'
                  f'time_stride_pixel={time_stride}pixel\t'
                  f'envelope len:{len(envelope)}\t')
            # == show intensity info on p1
            msg_tmp = (f'Intensity/sec: {Intensity_wav:.4f}\n'
                       f'norm height target: {norm_height:.1f}\n{np.round(local_maxi,1)}\n'
                       f'Scale: {envelope_scale:.3f}\n'
                       f'MovMean. height: {self.sbwin.movmen_norm_height_RR:.1f}\n'
                       f'stable local maxi: {stable_local_maxi}\n'
                       f'{msglowvol}\n'
                       f'quiet: {isquiet}')
        else:
            msg_tmp = f'Intensity/sec: {Intensity_wav:.4f}'
        p1txt_intensity = pg.TextItem(msg_tmp, color=(0,255,255,255), anchor=(0,0))
        self.sbwin.p1.addItem(p1txt_intensity)
        p1txt_intensity.setPos(self.sbwin.duration*.75,self.sbwin.Spectfmax.value())
        print('')
        return np.array(t_envelope_sec), envelope, stft_abs, Intensity_wav
    
    def bwfilter(self, data_in, sr, f_cut, N_filt=3, filtype='bandpass', ispltfreqResp=False):
        """apply butterworth filter with zero phase shift"""
        nyq = sr/2
        wn = np.array(f_cut)
        if np.max(wn) > nyq:
            wn[np.argmax(wn)] = nyq*0.99
        b_butter, a_butter = signal.butter(N_filt, wn/nyq, btype=filtype)
        data_filtered = signal.filtfilt(b_butter, a_butter, data_in)
        # plot of freq response
        if ispltfreqResp:
            w_butter, h_butter = signal.freqz(b_butter, a_butter, fs=sr)
            fig_fr = plt.figure(figsize=(10,6))
            ax_fr = fig_fr.subplots(1,1)
            ax_fr_2 = ax_fr.twinx()
            ax_fr.plot(w_butter, 20*np.log10(abs(h_butter)),'b',label="gain")
            ax_fr_2.plot(w_butter, np.unwrap(np.angle(h_butter,deg=True)),'r',label="phase")
            ax_fr.grid(axis='both')
            ax_fr.legend()
            ax_fr_2.legend()
            # ax_fr.set_xlim((0,sr/2))
            plt.show()
        return data_filtered

        # def getRMSEnergy(self, wavdata):
    #     S = np.abs(librosa.stft(wavdata, win_length=512))
    #     rms = librosa.feature.rms(S=S[\
    #         round(self.sbwin.config['Envelope']['freqband_node_Hz'][0]/(self.sbwin.sr/2)*S.shape[0]):\
    #             round(self.sbwin.config['Envelope']['freqband_node_Hz'][1]/(self.sbwin.sr/2)*S.shape[0])])
    #     plt.figure(figsize=(12,5))
    #     plt.subplot(2, 1, 1)
    #     plt.semilogy(rms.T, label='RMS Energy')
    #     plt.xticks([])
    #     plt.xlim([0, rms.shape[-1]])
    #     plt.legend()
    #     plt.subplot(2, 1, 2)
    #     librosa.display.specshow(librosa.amplitude_to_db(S, ref=np.max),
    #                             y_axis='linear', x_axis='time')
    #     plt.title('log Power spectrogram')
    #     plt.tight_layout()
    #     plt.show()
    
    def getPSE(self, wavdata, sr):
        nfft = self.sbwin.config['Envelope']['PSE_nfft']
        winlen = self.sbwin.config['Envelope']['PSE_winlen']
        hop = self.sbwin.config['Envelope']['PSE_hop']
        S = np.abs(librosa.stft(wavdata, win_length=winlen, hop_length=hop, n_fft=nfft))
        logS = np.log(S)
        # bands = np.asarray([0,100,800])
        # bands = np.arange(0,2000,100)
        bands = self.sbwin.config['Envelope']['PSE_bands']
        freqs = librosa.fft_frequencies(sr=sr, n_fft=nfft)
        times = librosa.times_like(S, sr=sr, hop_length=hop, n_fft=nfft)
        idx_t = 0
        PSE = np.array([])
        for s in S.T:
            psd = s**2
            psd /= np.sum(psd)  # normalized PSD
            # freqs = np.fft.rfftfreq(y.size, 1/sr)            
            # bands = np.asarray(bands)
            # freq_limits_low = np.concatenate([[0.0],bands])
            freq_limits_low = bands
            freq_limits_up = np.concatenate([bands[1:], [2000]])
            PSE_per_band = []
            for low,up in zip(freq_limits_low, freq_limits_up):
                psd_roi = psd[np.bitwise_and(freqs >= low, freqs < up)]
                PSE_per_band.append(-1 * np.sum(psd_roi * np.log2(psd_roi)))
                # print(f'{low}~{up}Hz')                
            # print(times[idx_t])
            PSE = np.block([PSE, np.array(PSE_per_band).reshape(len(PSE_per_band),-1)]) \
                if idx_t > 0 else np.block([PSE, np.array(PSE_per_band)]).reshape(len(PSE_per_band),-1)
            idx_t += 1
        fig = plt.figure(figsize=(12,7))
        ax = fig.subplots(2,1)
        ax[0].pcolormesh(times, freqs, logS, cmap='afmhot', vmin=-5, vmax=6)
        ax[0].set_title('spectrogram')
        ax[1].pcolormesh(times, np.block([np.array(bands),2000]), PSE, cmap='afmhot', vmin=1, vmax=6)
        ax[1].set_title('Spectral Entropy linear scale')
        # ax[2].pcolormesh(times, np.block([bands, 2000]), np.log(PSE), cmap='afmhot', vmin=-5, vmax=6)
        # ax[2].set_title('Spectral Entropy log scale')
        plt.tight_layout(rect=(0.0,0.0,1,0.95))
        plt.suptitle(f'{os.path.basename(self.sbwin.fnnow)}')
        plt.show()
    
    def mydetrend(self, data, detrend_fitting_deg=1, detrend_jointhalf=True):
        x_tmp = np.arange(len(data))
        if detrend_jointhalf:
            select = np.where(x_tmp < len(data)/2)
            x_selection = x_tmp[select]
            y_selection = data[select]
            coeff = np.polyfit(x_selection, y_selection, detrend_fitting_deg)
            data_detrend = data[select]- np.polyval(coeff, x_selection)
            select = np.where(x_tmp >= len(data)/2)
            x_selection = x_tmp[select]
            y_selection = data[select]
            coeff = np.polyfit(x_selection, y_selection, detrend_fitting_deg)
            data_detrend = np.block([data_detrend, data[select]- np.polyval(coeff, x_selection)])
        else:
            x_tmp = np.arange(len(data))
            select = x_tmp
            x_selection = x_tmp[select]
            y_selection = data[select]
            coeff = np.polyfit(x_selection, y_selection, 2)
            data_detrend = data[select]- np.polyval(coeff, x_selection)
        return data_detrend


    # def getRR_
    # 
    def autocorr(self, stft_abs, envelope, Intensity_wav):
        chinese_font = matplotlib.font_manager.FontProperties(fname='C:\Windows\Fonts\mingliu.ttc', size=16)
        # spectro_dataset = [stft_abs, np.log10(np.where(stft_abs!=0, stft_abs, 1e-20))]
        spectro_dataset =   [stft_abs, envelope]  #, [envelope]
        # ====== get auto-correlation data for RR calculation
        AutoCorr_dataTyp = ['stft_linear','envelope'] # ['envelope']
        # # === high pass filter for auto-correlation sum
        # # sr_spectrogram = stft_abs.shape[1]/self.sbwin.duration    # sampling rate of spectrogram
        # wn = np.array([self.sbwin.config['Envelope']['Hpass_autocorr_Hz']])
        # N_filt = 3
        # nyq = self.sbwin.sr/2
        # b_butter, a_butter = signal.butter(N_filt, wn/nyq, btype='highpass')
        # === calculate auto-correlation of spectrogram data in the one frequency bin
        idx_AutoCorr = 0    # idx for input data  
        p1txt_RR = ''
        for data in spectro_dataset:
            if AutoCorr_dataTyp[idx_AutoCorr] not in self.sbwin.config['Envelope']['AutoCorr_typ']:
                idx_AutoCorr += 1
                continue
            print(f'\ngetRR from {AutoCorr_dataTyp[idx_AutoCorr]}')
            timebin_len = data.shape[1] if 'stft' in AutoCorr_dataTyp[idx_AutoCorr] else data.size
            # === initialize accumulative auto-correlation data
            if not len(self.sbwin.AutoCorr_accum):
                self.sbwin.AutoCorr_accum = [[] for i in range(len(spectro_dataset))]
            # print(f'\nGet AutoCorrelation of {AutoCorr_dataTyp[idx_AutoCorr]}  timebin_len={timebin_len}')            
            if 'stft' in AutoCorr_dataTyp[idx_AutoCorr]:    # data of each freq bin (2D array)
                AutoCorr = signal.correlate(data[0],data[0])  # 1st freq bin
                # print(f'\n\tshape of autocorr:{np.shape(AutoCorr)}')            
                for i in range(data.size-1):    # each freq bins
                    array_tmp = signal.correlate(data[i+1],data[i+1])
                    # print(f'\n\tshape of array_tmp:{array_tmp.shape}')
                    AutoCorr = np.vstack((AutoCorr,array_tmp))
                # === sum of data of all freq bin at the same time bin
                AutoCorr_sum = np.sum(AutoCorr
                    [round(self.sbwin.config['Envelope']['freqband_node_Hz'][0]/(self.sbwin.sr/2)*data.size):
                    round(self.sbwin.config['Envelope']['freqband_node_Hz'][1]/(self.sbwin.sr/2)*data.size)], axis=0)
            else:
                AutoCorr_sum = signal.correlate(data, data)
            # diff_orig = np.diff(AutoCorr_sum)
            # == high-pass filter on auto-correlation data
            # AutoCorr_sum_filt = signal.filtfilt(b_butter, a_butter, AutoCorr_sum)
            # == detrend
            # AutoCorr_sum_detrend = signal.detrend(AutoCorr_sum, bp=(0, timebin_len, len(AutoCorr_sum)))
            AutoCorr_sum_detrend = self.sbwin.mydetrend(AutoCorr_sum, 
                    detrend_fitting_deg=self.sbwin.config['Envelope']['detrend_fitting_deg'], 
                    detrend_jointhalf=self.sbwin.config['Envelope']['detrend_jointhalf'])
            diff_de = np.diff(AutoCorr_sum_detrend)
            # = linear
            AutoCorr_sum_detrend_linear = self.sbwin.mydetrend(AutoCorr_sum, detrend_fitting_deg=1, detrend_jointhalf=True)
            AutoCorr_sum_detrend_linear -= np.min(AutoCorr_sum_detrend_linear)
            # diff_de_linear_jointhalf = np.diff(AutoCorr_sum_detrend_linear)
            # = 2nd whole
            AutoCorr_sum_detrend_poly = self.sbwin.mydetrend(AutoCorr_sum, detrend_fitting_deg=2, detrend_jointhalf=False)
            AutoCorr_sum_detrend_poly -= np.min(AutoCorr_sum_detrend_poly)
            # diff_de_poly_whole = np.diff(AutoCorr_sum_detrend_poly)
            # == offset it to >=0
            AutoCorr_sum -= np.min(AutoCorr_sum)
            # AutoCorr_sum_filt -= np.min(AutoCorr_sum_filt)
            AutoCorr_sum_detrend -= np.min(AutoCorr_sum_detrend)
            # == normalize it
            AutoCorr_sum_norm = AutoCorr_sum/np.max(AutoCorr_sum)
            # AutoCorr_sum_filt_norm = AutoCorr_sum_filt/np.max(AutoCorr_sum_filt)
            AutoCorr_sum_detrend_norm = AutoCorr_sum_detrend/np.max(AutoCorr_sum_detrend)
            # # == keep accumulative data up to date
            # # self.sbwin.AutoCorr_accum[idx_AutoCorr] += AutoCorr_sum_detrend_norm
            # if len(self.sbwin.AutoCorr_accum[idx_AutoCorr]) and \
            #         len(self.sbwin.AutoCorr_accum[idx_AutoCorr][-1]) != len(AutoCorr_sum_detrend_norm):
            #     self.sbwin.AutoCorr_accum[idx_AutoCorr] = []
            # if len(self.sbwin.AutoCorr_accum[idx_AutoCorr]) > 4:
            #     self.sbwin.AutoCorr_accum[idx_AutoCorr].pop(0)
            # self.sbwin.AutoCorr_accum[idx_AutoCorr].append(AutoCorr_sum_detrend_norm)
            # AutoCorr_avg = np.sum(self.sbwin.AutoCorr_accum[idx_AutoCorr],axis=0) / len(self.sbwin.AutoCorr_accum[idx_AutoCorr])
            # find peaks of auto-correlation sum                
            if 'linear' in AutoCorr_dataTyp[idx_AutoCorr] or 'envelope' in AutoCorr_dataTyp[idx_AutoCorr]:
                AutoCorr_sum_dataset = [[],[]]   # orginal, high-passs filtered(skipped), detrend, accumulative(skipped)
                # ======  find peaks from center peak
                # === peaks of original data                
                data_tmp = AutoCorr_sum_norm[round(timebin_len):]
                pks_orig, pkinfo_orig = signal.find_peaks(data_tmp, \
                    prominence=self.sbwin.config['Envelope']['pk_AuCo_prominence_orig'], \
                    wlen=self.sbwin.config['Envelope']['pk_AuCo_wlen_sec']/self.sbwin.duration*timebin_len )
                print(f'peaks of auto-correlation sum(original):{np.round(pks_orig/timebin_len*self.sbwin.duration,decimals=2)}(sec)')
                AutoCorr_sum_dataset[0] = [AutoCorr_sum_norm, pks_orig, pkinfo_orig, 'pks_all freq_orig', (0,0), 0, 'k']
                # # === peaks of filtered
                # data_tmp = AutoCorr_sum_filt_norm[round(timebin_len/2):timebin_len]                    
                # pks_filt, pkinfo_filt = signal.find_peaks(data_tmp, \
                #     prominence=self.sbwin.config['Envelope']['pk_AuCo_prominence'], \
                #     height=self.sbwin.config['Envelope']['pk_AuCo_height'], \
                #     wlen=self.sbwin.config['Envelope']['pk_AuCo_wlen_sec']/self.sbwin.duration*timebin_len )
                # # print(f'\npeaks of auto-correlation sum:{np.round(pks_filt/timebin_len*self.sbwin.duration,decimals=2)}(sec)')
                # # bases_legnth = np.array([round((pkinfo['right_bases'][i]-pkinfo['left_bases'][i])/timebin_len*self.sbwin.duration,2) \
                # #     for i in range(len(pks))])
                # # print(f'pkinfo(bases length) of auto-correlation sum:{bases_legnth}(sec)')
                # AutoCorr_sum_dataset[1] = [AutoCorr_sum_filt_norm, pks_filt, pkinfo_filt, 'pks_all freq_filt', self.sbwin.config['Envelope']['pk_AuCo_txt_offset'][0], 0, 'g']
                # AutoCorr_sum_dataset[1] = [[], [], [], 'pks_all freq_filt', self.sbwin.config['Envelope']['pk_AuCo_txt_offset'][0], 0, 'g']
                # === peaks of detrend
                data_tmp = AutoCorr_sum_detrend_norm[round(timebin_len):]
                pks_detrend, pkinfo_detrend = signal.find_peaks(data_tmp, \
                    prominence=self.sbwin.config['Envelope']['pk_AuCo_prominence'], \
                    height=self.sbwin.config['Envelope']['pk_AuCo_height'], \
                    wlen=self.sbwin.config['Envelope']['pk_AuCo_wlen_sec']/self.sbwin.duration*timebin_len )
                print(f'peaks of auto-correlation sum(detrend):{np.round(pks_detrend/timebin_len*self.sbwin.duration,decimals=2)}(sec)')
                AutoCorr_sum_dataset[1] = [AutoCorr_sum_detrend_norm, pks_detrend, pkinfo_detrend, 'pks_all freq_detrend', self.sbwin.config['Envelope']['pk_AuCo_txt_offset'][1], 0, 'b']
                # # === peaks of accumlative
                # data_tmp = AutoCorr_avg[round(timebin_len):]
                # pks_accum, pkinfo_accum = signal.find_peaks(data_tmp, \
                #     prominence=self.sbwin.config['Envelope']['pk_AuCo_prominence'], \
                #     height=self.sbwin.config['Envelope']['pk_AuCo_height'], \
                #     wlen=self.sbwin.config['Envelope']['pk_AuCo_wlen_sec']/self.sbwin.duration*timebin_len )
                # print(f'peaks of auto-correlation sum(accumulative):{np.round(pks_accum/timebin_len*self.sbwin.duration,decimals=2)}(sec)')
                # AutoCorr_sum_dataset[2] = [AutoCorr_avg, pks_accum, pkinfo_accum, 'pks_all freq_accum', self.sbwin.config['Envelope']['pk_AuCo_txt_offset'][2], 3, 'b']
            # idx_AutoCorr += 1
            AutoCorr_Chkfreq = self.sbwin.config['Envelope']['AutoCorr_Chkfreq']
            fig_ = plt.figure(figsize=(28,14), clear=True)
            fontsize_fig = 13
            gs = fig_.add_gridspec(3,2)
            # ax_ = fig_.subplots(3,1)    # Upper:half period(left:original / right:normalized) /  lower:whole(original)
            ax_ = [[] for i in range(4)]
            ax2_ = [[] for i in range(4)]
            ax_[0] = fig_.add_subplot(gs[0,:])  # right half of auto-correlation with peaks info, left: normalized @ specified frequnecies; right: normalized sum
            ax_[1] = fig_.add_subplot(gs[1,:])  # whole auto-correlation, left: orginal sum, right: filted, detrend
            ax_[2] = fig_.add_subplot(gs[2,0])  # spectrogram
            # ax_[3] = fig_.add_subplot(gs[2,1:])  # right half of accumulative auto-correlation sum
            # === plot auto-correlation at given frequencies w/ normalization
            if 'stft' in AutoCorr_dataTyp[idx_AutoCorr]:
                [ax_[0].plot(AutoCorr[round(freq/(self.sbwin.sr/2)*spectro_dataset[idx_AutoCorr].shape[0])]/\
                        np.max(AutoCorr[round(freq/(self.sbwin.sr/2)*spectro_dataset[idx_AutoCorr].shape[0])]),\
                        label=f'{freq}Hz') for freq in AutoCorr_Chkfreq]
                ax_[0].legend(loc='upper left')
                # [ax_[0].plot(AutoCorr[round(freq/(self.sbwin.sr/2)*spectro_dataset[idx_AutoCorr].shape[0])]*200,label=f'{freq}Hz') for freq in AutoCorr_Chkfreq if freq>=800]
            # === normalized auto-correlation sum
            ax2_[0] = ax_[0].twinx()
            ax2_[0].plot(AutoCorr_sum_norm,'k-',label=f'all freq(normalzied)')
            # ax2_[0].plot(AutoCorr_sum_filt_norm,'g--',\
            #     label=f"all freq(Normalzied H-Pass after {self.sbwin.config['Envelope']['Hpass_autocorr_Hz']}Hz)")
            ax2_[0].plot(AutoCorr_sum_detrend_norm,'c--', label=f"all freq(Normalzied detrend)")
            # === original auto-correlation sum
            ax2_[1] = ax_[1].twinx()
            ax_[1].plot(AutoCorr_sum_norm,'k-',label=f'all freq')
            # ax2_[1].plot(AutoCorr_sum_filt,'g--',label=f"all freq(H-Pass after {self.sbwin.config['Envelope']['Hpass_autocorr_Hz']}Hz)")
            ax_[1].plot(AutoCorr_sum_detrend_norm,'c-',label=f"all freq(detrend_now)")
            ax_[1].plot(AutoCorr_sum_detrend_linear/np.max(AutoCorr_sum_detrend_linear),'r--',label=f"all freq(detrend_linear)")
            ax_[1].plot(AutoCorr_sum_detrend_poly/np.max(AutoCorr_sum_detrend_poly),'b--',label=f"all freq(detrend_2nd)")
            ax2_[1].plot(diff_de,'c--',label=f"diff_detrend")
            # === spectrogram
            ax_[2].pcolormesh(self.sbwin.logSx, cmap='afmhot', vmin=self.sbwin.config['Spectrogram']['vmin'], vmax=self.sbwin.config['Spectrogram']['vmax'])
            ax_[2].plot(envelope/np.max(envelope)*(0.5*self.sbwin.logSx.shape[0]), 'c')
            # # === accumulation of detrend auto-correlation sum
            # ax_[3].plot(AutoCorr_avg,'c-', label=f'Avg. Accumulation')
            # === add peaks info
            if AutoCorr_dataTyp[idx_AutoCorr] == 'stft_linear' or AutoCorr_dataTyp[idx_AutoCorr] == 'envelope':
                RR_tmp = []
                RRinx_tmp = []
                RRinx_slope_tmp = []
                RRinx_Q_tmp = []
                RR_accum_tmp = []
                RRinx_accum_tmp = []
                for AutoCorr_sum_data, pks, pkinfo, pklabel, XYoff, idx_ax, txtColor in AutoCorr_sum_dataset:
                    if not len(pks):
                        continue
                    if 'accum' in pklabel:
                        ax_[idx_ax].plot(pks+round(timebin_len),AutoCorr_sum_data[pks+round(timebin_len)],f'{txtColor}o',label=pklabel)
                    else:
                        ax2_[idx_ax].plot(pks+round(timebin_len),AutoCorr_sum_data[pks+round(timebin_len)],f'{txtColor}o',label=pklabel)
                    # if 'detrend' in pklabel:
                    #     # === calculate peak width
                    #     pks_width, pks_width_height, pks_width_left_ips, pks_width_right_ips = signal.peak_widths(AutoCorr_sum_data, pks+round(timebin_len/2), rel_height=0.5)
                    for j in range(len(pks)):                        
                        msg_tmp = f'{pks[j]/timebin_len*self.sbwin.duration:.2f}sec({60/(pks[j]/timebin_len*self.sbwin.duration):.1f}BPM)\n'
                        # === calculate peak left/right prominence
                        # msg_tmp += f'Promi_{pkinfo["prominences"][j]:.2f}'
                        # == left prominence
                        slope = np.diff(AutoCorr_sum_data[pkinfo['left_bases'][j]+round(timebin_len)-2:pkinfo['left_bases'][j]+round(timebin_len)+3])
                        # if np.min(AutoCorr_sum_data[pkinfo['left_bases'][j]+round(timebin_len/2)-2:pkinfo['left_bases'][j]+round(timebin_len/2)+3]) == \
                        #     AutoCorr_sum_data[pkinfo['left_bases'][j]+round(timebin_len/2)]:
                        if slope[0] < 0:
                            left_bases_idx = pkinfo['left_bases'][j]
                        else:
                            # print(f'\t{pklabel.split("_")[-1]}:go to search correct local minma of left prominence')
                            left_bases_idx = np.argmin(AutoCorr_sum_data[round(timebin_len):pks[j]+round(timebin_len)]) if j == 0 \
                                else np.argmin(AutoCorr_sum_data[pks[j-1]+round(timebin_len):pks[j]+round(timebin_len)])+pks[j-1]
                        promi_left = AutoCorr_sum_data[pks[j]+round(timebin_len)]-AutoCorr_sum_data[left_bases_idx+round(timebin_len)]
                        msg_tmp += f'left:{promi_left:.2f}@{(left_bases_idx)/timebin_len*self.sbwin.duration:.2f}sec'
                        if 'detrend' in pklabel or 'accum' in pklabel:
                            slopeData = np.diff(AutoCorr_sum_data[left_bases_idx+round(timebin_len):pks[j]+1+round(timebin_len)])
                            inx_maxslope = np.argmax(slopeData) + left_bases_idx + round(timebin_len)
                            peak_level = AutoCorr_sum_data[pks[j]+round(timebin_len)] - AutoCorr_sum_data[inx_maxslope]
                            peak_width = pks[j]+round(timebin_len) - inx_maxslope
                            Q_left = peak_level/(2*peak_width)*1000
                            maxslope_left = slopeData[inx_maxslope-left_bases_idx-round(timebin_len)]*100
                            msg_tmp += f'\n  @{(inx_maxslope-round(timebin_len))/timebin_len*self.sbwin.duration:.1f}s slope:{maxslope_left:.2f} '
                            msg_tmp += f'Q:{Q_left:.2f}' if 'detrend' in pklabel \
                                else f'\n   Q:{Q_left:.2f}'
                        # == right prominence
                        slope = np.diff(AutoCorr_sum_data[pkinfo['right_bases'][j]+round(timebin_len)-2:pkinfo['right_bases'][j]+round(timebin_len)+3])
                        if slope[0] > 0:
                            right_bases_idx = pkinfo['right_bases'][j]
                        else:
                            # print(f'\t{pklabel.split("_")[-1]}:go to search correct local minma of right prominence')
                            right_bases_idx = np.argmin(AutoCorr_sum_data[pks[j]+round(timebin_len):])+pks[j] if j == len(pks)-1 \
                                else np.argmin(AutoCorr_sum_data[pks[j]+round(timebin_len):pks[j+1]+round(timebin_len)])+pks[j]
                        promi_right = AutoCorr_sum_data[pks[j]+round(timebin_len)]-AutoCorr_sum_data[right_bases_idx+round(timebin_len)]
                        msg_tmp += f'\nright:{promi_right:.2f}@{(right_bases_idx)/timebin_len*self.sbwin.duration:.1f}sec'
                        if 'detrend' in pklabel or 'accum' in pklabel:
                            slopeData = np.diff(AutoCorr_sum_data[pks[j]-1+round(timebin_len):right_bases_idx+round(timebin_len)])
                            inx_maxslope = np.argmin(slopeData) + pks[j]-1 + round(timebin_len)
                            peak_level = AutoCorr_sum_data[pks[j]+round(timebin_len)] - AutoCorr_sum_data[inx_maxslope]
                            peak_width = inx_maxslope - (pks[j]+round(timebin_len))
                            Q_right = peak_level/(2*peak_width)*1000
                            maxslope_right = slopeData[inx_maxslope-pks[j]-round(timebin_len)]*100
                            msg_tmp += f'\n  @{(inx_maxslope-round(timebin_len))/timebin_len*self.sbwin.duration:.2f}s slope:{maxslope_right:.2f} '
                            msg_tmp += f'Q:{Q_right:.2f}' if 'detrend' in pklabel \
                                else f'\n   Q:{Q_right:.2f}'
                        msg_tmp += f'\nProm: Avg. {(promi_left+promi_right)/2:.3f}'
                        if 'detrend' in pklabel or 'accum' in pklabel:
                            msg_tmp += f'\n       *slope {(promi_left*maxslope_left-promi_right*maxslope_right)/2:.3f}'
                            msg_tmp += f'\n             *Q {(promi_left*Q_left+promi_right*Q_right)/2:.3f}'
                        if 'detrend' in pklabel:
                            RR_tmp.append(round(60/(pks[j]/timebin_len*self.sbwin.duration),2))
                            RRinx_tmp.append(np.round((promi_left+promi_right)/2,3))
                            RRinx_slope_tmp.append(round((promi_left*maxslope_left-promi_right*maxslope_right)/2,3))
                            RRinx_Q_tmp.append(round((promi_left*Q_left+promi_right*Q_right)/2,3))
                            # msg_tmp += f'\nwidth:{pks_width[j]/timebin_len*self.sbwin.duration:.0f}(Q:{(pkinfo["peak_heights"][j]-pks_width_height[j])/pks_width[j]*100:.2f})'
                        if 'accum' in pklabel:
                            RR_accum_tmp.append(round(60/(pks[j]/timebin_len*self.sbwin.duration),1))
                            RRinx_accum_tmp.append(np.round((promi_left+promi_right)/2,3))
                            ax_[idx_ax].annotate(msg_tmp, (pks[j]+round(timebin_len), 0), \
                                fontsize=fontsize_fig, color=txtColor, xytext=XYoff, textcoords='offset points')
                            ax_[idx_ax].plot(left_bases_idx+round(timebin_len), AutoCorr_sum_data[left_bases_idx+round(timebin_len)],\
                                f'{txtColor}>',markersize=14)
                            ax_[idx_ax].plot(right_bases_idx+round(timebin_len), AutoCorr_sum_data[right_bases_idx+round(timebin_len)],\
                                f'{txtColor}<',markersize=14)
                        else:
                            ax2_[idx_ax].annotate(msg_tmp, (pks[j]+round(timebin_len), 0), \
                                fontsize=fontsize_fig, color=txtColor, xytext=XYoff, textcoords='offset points')
                            ax2_[idx_ax].plot(left_bases_idx+round(timebin_len), AutoCorr_sum_data[left_bases_idx+round(timebin_len)],\
                                f'{txtColor}>',markersize=14)
                            ax2_[idx_ax].plot(right_bases_idx+round(timebin_len), AutoCorr_sum_data[right_bases_idx+round(timebin_len)],\
                                f'{txtColor}<',markersize=14)
                # ====== find RR
                # === sort RRdata by RRinx
                inx_sorted = sorted(range(len(RRinx_tmp)), key=lambda k: RRinx_tmp[k], reverse=True)
                # == if RR_sec is beyond the range of 2~3 sec, gap of orignal highest 2 RRinx <= 0.1, sort RRdata by RRinx_slope
                # if len(RRinx_tmp)>1 and np.array(RRinx_tmp)[inx_sorted][0]-np.array(RRinx_tmp)[inx_sorted][1] <= 0.1:
                if len(RRinx_tmp)>1 and (np.array(RR_tmp)[inx_sorted][0]<20 or np.array(RR_tmp)[inx_sorted][0]>30) and \
                        np.array(RRinx_tmp)[inx_sorted][0]-np.array(RRinx_tmp)[inx_sorted][1] <= 0.1:
                    inx_sorted = sorted(range(len(RRinx_slope_tmp)), key=lambda k: RRinx_slope_tmp[k], reverse=True)
                    print('RR rule1: sort RRinx_slope')
                msg_tmp = f'detrend:\nsorted RR:{np.array(RR_tmp)[inx_sorted]}BRM\n'
                msg_tmp += f'RRidx:{np.array(RRinx_tmp)[inx_sorted]}\n'
                msg_tmp += f'RRidx_slope:{np.array(RRinx_slope_tmp)[inx_sorted]}\n'
                msg_tmp += f'interval:{np.round(pks_detrend[inx_sorted]/timebin_len*self.sbwin.duration,3)}sec'
                print(msg_tmp)
                ax_[0].annotate(msg_tmp,(timebin_len,1), fontsize=fontsize_fig, color='b')
                inx_sorted_accum = sorted(range(len(RRinx_accum_tmp)), key=lambda k: RRinx_accum_tmp[k], reverse=True)
                # msg_tmp = f'accumulative:\nsorted RR:{np.array(RR_accum_tmp)[inx_sorted_accum]}BRM\n'
                # msg_tmp += f'RRidx:{np.array(RRinx_accum_tmp)[inx_sorted_accum]}\n'
                # msg_tmp += f'interval:{np.round(pks_accum[inx_sorted_accum]/timebin_len*self.sbwin.duration,3)}sec'
                # ax_[3].annotate(msg_tmp,(timebin_len,1), fontsize=fontsize_fig, color='b')
                # print(msg_tmp)
                # === initial value
                RR = 0
                RRinx = 0
                RR_sec = 0
                # === if 1st interval < 2.14sec and its RRinx >= 0.28
                # === and 5 peaks with stable periods, choose the 2nd peak whose RRinx > 0.2
                inx_periodic = 0
                cnt_periodic = 1                
                if (len(RR_tmp) >= 5 and RR_tmp[0] >= 28 and RRinx_tmp[0] >= 0.28):
                    print(f'\nevaluating Rule2')
                    for i in range(1, len(RRinx_tmp)):
                        print(f'{RR_tmp[0]}/{RR_tmp[i]}/{cnt_periodic+1}={RR_tmp[0]/RR_tmp[i]/(cnt_periodic+1):.3f}')
                        cnt_periodic += 1 if RR_tmp[0] < (cnt_periodic+1)*1.1*RR_tmp[i] and RR_tmp[0] > (cnt_periodic+1)*0.9*RR_tmp[i] \
                                        else 0
                        if cnt_periodic == 2:
                            inx_periodic = i
                        if cnt_periodic == 5 and RRinx_tmp[inx_periodic] > 0.2:
                            RRinx = RRinx_tmp[inx_periodic]
                            RR = RR_tmp[inx_periodic]
                            RR_sec = np.round(pks_detrend[inx_periodic]/timebin_len*self.sbwin.duration,3)
                            print('RR rule2: 5 peaks with stable periods')
                            break
                # === if duration = 30sec and RR_sec < duration/3sec, 
                # find at least 30//RR_sec stable interval that is almost equal to RR_sec
                if self.sbwin.duration == 30 and RR == 0:
                    stable_interval_RR_cnt = 0
                    for i in range(len(RRinx_tmp)):
                        if 60/RR_tmp[i] > self.sbwin.duration/2 or stable_interval_RR_cnt == 2:
                            break
                        print(f'\nevaluating Rule5')
                        cnt_periodic = 1
                        for j in range(i, len(RR_tmp)):
                            print(f'{RR_tmp[i]}/{RR_tmp[j]}/{cnt_periodic+1}={RR_tmp[i]/RR_tmp[j]/(cnt_periodic+1):.3f}')
                            cnt_periodic += 1 if RR_tmp[i] < (cnt_periodic+1)*1.075*RR_tmp[j] and \
                                                RR_tmp[i] > (cnt_periodic+1)*0.93*RR_tmp[j] \
                                            else 0
                            if cnt_periodic == min(10, self.sbwin.duration//(60/RR_tmp[i])):
                                RRinx = RRinx_tmp[i]
                                RR = RR_tmp[i]
                                RR_sec = np.round(pks_detrend[i]/timebin_len*self.sbwin.duration,3)
                                print(f'RR rule5: RR_{RR_sec}sec {cnt_periodic} peaks with stable periods')
                                stable_interval_RR_cnt += 1
                                break
                # # try to find the interval relation among peaks if 
                # # 1. gap of highest two RRinx < 0.175
                # # 2. peak count: 3~5
                # # 3. 1st peak < 2sec
                # if RR == 0 and len(RR_tmp) >= 3 and len(RR_tmp) <= 5 and \
                #         60/RR_tmp[0] > 3.5 \
                #         and np.array(RRinx_tmp)[inx_sorted][0]-np.array(RRinx_tmp)[inx_sorted][1] <= 0.175:
                #     idx = 2
                #     # check if RR_sec[i]-RR_sec[i-1] = RR_sec[0]
                #     while idx < len(RR_tmp) and not RR:
                #         if RR_tmp[idx] > 4.5 and \
                #                 RRinx_tmp[idx] > np.max(RRinx_tmp)/3 and \
                #                 RRinx_tmp[idx-1] > np.max(RRinx_tmp)/4.5 and \
                #                 60/np.array(RR_tmp[idx])-60/np.array(RR_tmp[idx-1]) <= 60/np.array(RR_tmp[0])*1.17 and \
                #                 60/np.array(RR_tmp[idx])-60/np.array(RR_tmp[idx-1]) >= 60/np.array(RR_tmp[0])*0.86:
                #             RRinx = RRinx_tmp[idx]
                #             RR = RR_tmp[idx]
                #             RR_sec = np.round(pks_detrend[idx]/timebin_len*self.sbwin.duration,3)
                #             print('RR rule4: RR_sec[i]-RR_sec[i-1] = RR_sec[0]')
                #             break
                #         idx += 1
                    # # check if RR_sec[i] = RR_sec[i+1] - RR_sec[i-1]
                    # idx = 1
                    # while idx < len(RR_tmp)-1 and not RR:
                    #     if RR_tmp[idx] > 4.5 and RRinx_tmp[idx] > 0.2 and RRinx_tmp[idx-1] > 0.2 and \
                    #             60/np.array(RR_tmp[idx-1])+60/np.array(RR_tmp[idx]) <= 60/np.array(RR_tmp[idx+1])*1.1 and \
                    #             60/np.array(RR_tmp[idx-1])+60/np.array(RR_tmp[idx]) >= 60/np.array(RR_tmp[idx+1])*0.9:
                    #         RRinx = RRinx_tmp[idx]
                    #         RR = RR_tmp[idx]
                    #         RR_sec = np.round(pks_detrend[idx]/timebin_len*self.sbwin.duration,3)
                    #         print('RR rule5: RR_sec[i] = RR_sec[i+1] - RR_sec[i-1]')
                    #         break
                    #     idx += 1
                # valid RRcnt = count (RRinx >= 0.09, RR_sec gap >= 1.5, peak_level gap > 0.1 if RR_sec gap < 1.5) 
                #                   if RR cnt < 9 else orignal RR cnt
                RR_cnt = 0
                RR_valid_sec = [0]
                pk_valid_level = [0]
                if len(RR_tmp) <= 8:
                    for i in range(len(RR_tmp)):
                        if RRinx_tmp[i] >= 0.09 and \
                            (i == 0 or \
                                (i > 0 and ((60/RR_tmp[i]-RR_valid_sec[-1]) >= 1.5 or 
                                    abs(AutoCorr_sum_data[pks[i]+round(timebin_len)]- pk_valid_level[-1]) > 0.1))):
                                    # abs(RRinx_tmp[i]-RRinx_valid[-1]) >= 0.1))):
                            RR_cnt += 1
                            RR_valid_sec.append(60/RR_tmp[i])
                            pk_valid_level.append(AutoCorr_sum_data[pks[i]+round(timebin_len)])
                else:
                    RR_cnt = len(RR_tmp)
                RR_cnt = RR_cnt if RR_cnt else len(RR_tmp)
                is_byRule3 = False
                if RR == 0:
                    is_byRule3 = True
                    # ====== rule 3
                    # === skip RR beyound upper/lower limit
                    # UL:3 / 13(case of 1 peak in 15sec)
                    # LL:4
                    # LL: RR_sec < duration/RRcnt*3
                    idx = 0
                    while len(RR_tmp) and idx < len(RR_tmp) and \
                            (np.array(RR_tmp)[inx_sorted][idx] > 32 or
                            np.array(RR_tmp)[inx_sorted][idx] < 4 or
                            (RR_cnt == 1 and np.array(RR_tmp)[inx_sorted][idx] > 60/(self.sbwin.duration*0.31)) or
                            (np.array(RR_tmp)[inx_sorted][idx] < 60/(self.sbwin.duration/RR_cnt*3))):
                            # (np.array(RR_tmp)[inx_sorted][idx] < 60/(self.sbwin.duration/len(RR_tmp)*3))):
                            # (RR_cnt >= 2 and np.array(RR_tmp)[inx_sorted][idx] > 60/(self.sbwin.duration*0.42)) or
                        print(f'RR rule3  { np.array(RR_tmp)[inx_sorted][idx]} is out  (RR_cnt={RR_cnt})')
                        idx += 1
                    if idx < len(RR_tmp):
                        RRinx = np.array(RRinx_tmp)[inx_sorted][idx] if len(RR_tmp) else 0
                        RR = np.array(RR_tmp)[inx_sorted][idx] if len(RR_tmp) else 0
                        RR_sec = np.round(pks_detrend[inx_sorted]/timebin_len*self.sbwin.duration,3)[idx] if len(RR_tmp) else 0
                p1txt_RR += f'{AutoCorr_dataTyp[idx_AutoCorr]}\nRR={RR}BPM({RR_sec:.2f}sec)\n'
                p1txt_RR += f'RRindex={RRinx:.3f}\n'
            ax2_[0].legend(loc='upper right')
            ax_[0].grid(b=True,axis='x')
            ax2_[0].grid(b=True,axis='y')
            ax_[0].set_xticks(np.linspace(0,len(AutoCorr_sum),num=80))
            ax_[0].set_xticklabels(np.round(np.linspace(-self.sbwin.duration,self.sbwin.duration,num=80),decimals=1), fontdict={'fontsize':fontsize_fig})
            ax_[0].set_xlim(timebin_len,len(AutoCorr_sum))
            ax_[0].set_xlabel('shift(sec)')
            fndir_ref = '/'.join(self.sbwin.fnnow.split('/')[-4:-1]) if len(self.sbwin.fnnow.split('/')) >= 4 \
                        else os.path.dirname(self.sbwin.fnnow)
            ax_[0].set_title((f'{fndir_ref}/'
                              f'{os.path.basename(self.sbwin.fnnow)}\n' 
                              f'{self.sbwin.ti:.1f}~{self.sbwin.tf:.1f}s    '
                              f'Intensity/sec= {Intensity_wav:.4f}  RR_cnt= {RR_cnt}\n'
                              f'AutoCorrelation @ different frequency\n'
                              f'from {AutoCorr_dataTyp[idx_AutoCorr]} data'), \
                              fontproperties=chinese_font)
            ax_[0].set_ylim((0,1))

            ax_[1].legend(loc='upper left')
            ax2_[1].legend(loc='upper right')
            ax_[1].grid(b=True,axis='x')
            ax2_[1].grid(axis='y')
            ax_[1].set_xticks(np.linspace(0,len(AutoCorr_sum),num=40))
            ax_[1].set_xticklabels(np.round(np.linspace(-self.sbwin.duration,self.sbwin.duration,num=40),decimals=1), fontdict={'fontsize':fontsize_fig})
            ax_[1].set_xlim(0,len(AutoCorr_sum))
            
            ax_[2].set_xticks(np.linspace(0,self.sbwin.logSx.shape[1],num=20))
            ax_[2].set_xticklabels(np.round(np.linspace(0,self.sbwin.duration,num=20),decimals=1), fontdict={'fontsize':fontsize_fig})
            ax_[2].set_yticks(np.linspace(0,self.sbwin.logSx.shape[0],num=9))   
            ax_[2].set_yticklabels(np.arange(self.sbwin.sr/2,step=250,dtype='int'), fontdict={'fontsize':fontsize_fig})
            ax_[2].set_xlabel('time(sec)')
            
            # ax_[3].legend(loc='upper right')
            # ax_[3].set_xticks(np.linspace(0,len(AutoCorr_sum),num=80))
            # ax_[3].set_xticklabels(np.round(np.linspace(-self.sbwin.duration,self.sbwin.duration,num=80),decimals=1), fontdict={'fontsize':fontsize_fig})
            # ax_[3].set_xlim(timebin_len,len(AutoCorr_sum))
            # ax_[3].set_xlabel('shift(sec)')
            # ax_[3].set_ylim((0,1))
            # img = self.sbwin.win.grab(QtCore.QRect(0,0,int(self.sbwin.p1.geometry().width()),int(self.sbwin.p1.geometry().height()+10)))
            # img.save('tmp.png')
            # img = plt.imread('tmp.png')
            # ax_[2].imshow(img)
            plt.tight_layout()
            fn_currentpng = f'AutoCorr-{AutoCorr_dataTyp[idx_AutoCorr]}_current.png'
            plt.savefig(fn_currentpng)
            if self.sbwin.config['Envelope']['isSavRRpng']:                
                fn_RRpng = f"{self.sbwin.config['Envelope']['path_SavRRpng']}/{os.path.basename(self.sbwin.fnnow)[:-4]}/"
                if not os.path.exists(fn_RRpng):
                    os.makedirs(fn_RRpng)
                fn_RRpng += f'AutoCorr-{AutoCorr_dataTyp[idx_AutoCorr]}-{self.sbwin.ti}_{self.sbwin.tf}s.png'
                # plt.savefig(fn_RRpng)
                if os.path.exists(fn_RRpng):
                    os.remove(fn_RRpng)
                shutil.copy2(fn_currentpng, fn_RRpng)
            plt.close()
            # plt.show()
            # # === auto-correlation at all frequencies
            # plt.figure(figsize=(12,7))
            # plt.pcolormesh(AutoCorr, cmap='afmhot', vmin=self.sbwin.config['Spectrogram']['vmin'])
            # plt.title('auto correlation frequency by frequency')
            # plt.xticks(np.linspace(0,timebin_len,num=20), np.round(np.linspace(-self.sbwin.Set_duration.value()/2,self.sbwin.Set_duration.value()/2,num=20),decimals=1))
            # plt.yticks(np.linspace(0,data.size,num=20), np.linspace(0,self.sbwin.sr/2,num=20,dtype='int16'))
            # plt.xlabel('shift(sec)')
            # plt.ylabel('Hz')
            # plt.title('AutoCorrelation Mesh -frequency by frequency')
            # plt.savefig('AutoCorrelationMesh.png')
            # plt.close()
            # # plt.show()
            idx_AutoCorr += 1
        print(p1txt_RR)
        p1txt_RRinfo = pg.TextItem(p1txt_RR, color=(255,255,50,200), anchor=(0,0))
        self.sbwin.p1.addItem(p1txt_RRinfo)
        p1txt_RRinfo.setPos(self.sbwin.duration*.85,self.sbwin.Spectfmax.value()*0.99)

        RR_list = [RR]
        if RR and is_byRule3:  # provide 2nd candidate of RR whose RRinx is in the top two
            idx = 0
            for idx, RR_pack in enumerate(zip(np.array(RR_tmp)[inx_sorted], np.array(RRinx_tmp)[inx_sorted])):
                RR_t = RR_pack[0]
                RRinx_t = RR_pack[1]
                if len(RR_list)==2 or RR_t < 4 or RR_t == RR_list[0] or RRinx_t < 0.4 or\
                        (RR_cnt == 1 and np.array(RR_tmp)[inx_sorted][idx] > 60/(self.sbwin.duration*0.31)) or\
                        (np.array(RR_tmp)[inx_sorted][idx] < 60/(self.sbwin.duration/RR_cnt*2)):
                    continue
                RR_list.append(RR_t)
        self.sbwin.RR_sec = 60/np.array(RR_list)

        return RR, RRinx