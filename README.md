This is a audio labeling software developed by Heroic Faith Medical Science. Its original function is to label respiratory sounds. 

To use it, please pip install -r requirements.txt first, and then execute the main.py or click the runLabeler.bat. Please type the id "superUser" into the prompt and open the software. Drag and drop some audiofiles in WAVE format and you can start the labeling.

The user id is written in the ulist.py. If you want to add your own user id to use the software, please add the name you want into these two variables, self.userlist and self.VIP.

For example, 
        ...
        self.userlist = ["superUser", "myID"]
        self.VIP = ["superUser", "myID"]
        ...

The types of labels can be modified in the config.txt. You must replace the items in TagButtons with the ones you want, and also add them into UserDefinedTags. 

For example,
    ...
    "TagButtons": ["myTag", "myTag2", "Rhonchi", "Continuous","Inspiration", "Expiration", "Noise", "nonContinuous"],
    "UserDefinedTags": ["myTag", "myTag2"],
    ...

Note that a label file is not generated until you switch to labeling the next WAVE file. Therefore, after completing the labeling, click another WAVE file on the list or drag and drop a new WAVE file to generate the label file.

In the GUI, on the upper left side, you can find Spectrogram/Waveform Setting panel; you can adjust how to draw the spectrogram of the WAVE files by setting the parameters and clicking update. In the Tagging Action panel on the left side, you can switch to delete the label (after works for one time, it will be switched back to add/modify). Move the yellow lines on the spectrogram to define an ROI and click on the time tracks below to add labels corresponding to the tag strings on the left side. On the upper right side of GUI, you can see the WAVE file list. In the Set Tstart panel on the right side, you can adjust the time scale of the spectrogram display and move the time forward or backward. In the Play Refresh Sound panel, after checking the Refresh Mode and clicking the button below, the software plays a corresponding example sound. On the right bottom of the GUI, you can play the sound to your speaker or headphones. 

The software can be built and run stably on Python 3.7.8. The stability can't be guaranteed on other versions. The required modules in the requirement.txt may not be complete.

This project is licensed under GPLv3. 



