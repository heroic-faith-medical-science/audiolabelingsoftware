# from PyQt5.QtGui import *
# from PyQt5.QtWidgets import *
from PyQt5.QtCore import Qt
# from PyQt5.QtMultimedia import *
# from PyQt5.QtMultimediaWidgets import *
from PyQt5 import QtCore, QtGui, QtWidgets, QtMultimedia

from MainWindow import Ui_MainWindow

# from collections import deque
import numpy as np
import pyqtgraph as pg

# from scipy.io import wavfile
from scipy import signal
# from scipy.interpolate import interp1d
import soundfile as sf
from matplotlib import cm
import os
import csv
import time
import json
import socket
# os.environ['LIBROSA_CACHE_DIR'] = 'c:/tmp/librosa_cache'
import librosa
import matplotlib.pyplot as plt
import pgLinearRegionItem
import pgInfiniteLine
from functools import partial
# import ntplib
# from datetime import datetime, timezone
# import hashlib
import matplotlib.mlab as mlab
import shutil
import threading
import matplotlib
# matplotlib.use('agg')
# from dtw import dtw
from scipy import ndimage
chinese_font = matplotlib.font_manager.FontProperties(fname='C:\Windows\Fonts\mingliu.ttc', size=16)

# import sys
# sys.path.append("..")
# from RR_IEtag.getRR_IEtag import getIEtag as IEtag
# import ActiveNoiseCancel.runANC as ANC

import ulist
import sbutils

def hhmmss(sec, outType=0):
    h, r = divmod(sec, 3600)
    m, r = divmod(r, 60)
    # s, _ = divmod(r, 1000)
    s = r
    if outType == 0:
        if h:
            # ans = f'{h}:{m:02d}:{s:.2f}'
            ans = f'{h:02.0f}:{m:02.0f}:{s:02.0f}'
        elif m:
            # ans = f'{m:02d}:{s:.2f}'
            ans = f'{h:02.0f}:{m:02.0f}:{s:02.0f}'
        else:        
            ans = f'{h:02.0f}:{m:02.0f}:{s:02.0f}'
    elif outType == 1:   # for tag time slot in the exported tag file
        ans = f'{h:02.0f}:{m:02.0f}:{s:09.6f}'
    elif outType == 2:  # for update self.ti
        return h,m,s
    # elif outType == 2:  # for self.Set_ti.setValidator
    #     if h:
    #         ans = f"^[0-{h}]{{1,1}}:[0-5]\\d{{1,1}}:[0-5]\\d{{1,1}}.\\d{{3,3}}$"
    #     elif m:
    #         m_10, r = divmod(m, 10)
    #         if m_10:
    #             ans = f"^0:[0-{m_10}]\\d{{1,1}}:[0-5]\\d{{1,1}}.\\d{{3,3}}$"
    #         else:
    #             ans = f"^0:0\\[0-{m}]:[0-5]\\d{{1,1}}.\\d{{3,3}}$"
    #     else:
    #         s_10, r = divmod(s, 10)
    #         if s_10:
    #             ans = f"^0:00:(({round(s_10)}[0-{round(r)}])|(0\\d{{1,1}})).\\d{{3,3}}$"
    #         else:
    #             ans = f"^0:00:0\\[0-{round(s)}].\\d{{3,3}}$"
    return ans

# class ViewerWindow(QMainWindow):
#     state = pyqtSignal(bool)

#     def closeEvent(self, e):
#         # Emit the window state, to update the viewer toggle button.
#         self.state.emit(False)


class PlaylistModel(QtCore.QAbstractListModel):
    def __init__(self, playlist, *args, **kwargs):
        super(PlaylistModel, self).__init__(*args, **kwargs)
        self.playlist = playlist

    def data(self, index, role):
        if role == Qt.DisplayRole:
            media = self.playlist.media(index.row())
            return media.canonicalUrl().fileName()

    def rowCount(self, index):
        return self.playlist.mediaCount()


class MainWindow(QtWidgets.QMainWindow, Ui_MainWindow):
    def __init__(self, *args, **kwargs):
        super(MainWindow, self).__init__(*args, **kwargs)
        self.setupUi(self)

        self.player = QtMultimedia.QMediaPlayer()
        self.refreshplayer = QtMultimedia.QMediaPlayer()

        self.player.error.connect(self.erroralert)
        self.player.play()
        self.player.setNotifyInterval(10)
        self.refreshplayer.setNotifyInterval(10)

        # Setup the playlist.
        self.playlist = QtMultimedia.QMediaPlaylist()
        self.player.setPlaylist(self.playlist)

        # # Add viewer for video playback, separate floating window.
        # self.viewer = ViewerWindow(self)
        # self.viewer.setWindowFlags(self.viewer.windowFlags() | Qt.WindowStaysOnTopHint)
        # self.viewer.setMinimumSize(QSize(480,360))

        # videoWidget = QVideoWidget()
        # self.viewer.setCentralWidget(videoWidget)
        # self.player.setVideoOutput(videoWidget)

        # Connect control buttons/slides for media player.
        self.playButton.pressed.connect(self.playNormal)
        self.playZoomButton.pressed.connect(self.playZoom)
        self.playZoomROIButton.pressed.connect(self.playZoomROI)
        self.pauseButton.pressed.connect(self.player.pause)
        self.pauseButton.pressed.connect(self.refreshplayer.pause)
        # self.stopButton.pressed.connect(self.player.stop)
        # self.stopButton.pressed.connect(self.refreshplayer.stop)
        # self.volumeSlider.valueChanged.connect(self.player.setVolume)

        # self.viewButton.toggled.connect(self.toggle_viewer)
        # self.viewer.state.connect(self.viewButton.setChecked)

        self.previousButton.pressed.connect(self.playlist.previous)
        self.nextButton.pressed.connect(self.playlist.next)

        self.model = PlaylistModel(self.playlist)
        self.playlistView.setModel(self.model)
        self.playlist.currentIndexChanged.connect(self.playlist_position_changed)
        selection_model = self.playlistView.selectionModel()
        selection_model.selectionChanged.connect(self.playlist_selection_changed)

        # self.player.durationChanged.connect(self.update_duration)
        self.player.positionChanged.connect(self.update_position)
        # self.refreshplayer.durationChanged.connect(self.update_duration)
        self.refreshplayer.positionChanged.connect(self.update_position)
        # self.timeSlider.valueChanged.connect(self.player.setPosition)
        # self.timeSlider.valueChanged.connect(self.refreshplayer.setPosition)

        self.open_file_action.triggered.connect(self.open_file)

        self.setAcceptDrops(True)

        # hand made
        # --- input tagger and passwd
        self.tagger = 'heroicfaithmedsci'
        self.ids = ulist.userid(self)
        self.userid = self.ids.userlist
        self.ids.updateTagger()
        self.ids.checkpasswd()
        if self.tagger != 'cykuo':
            self.Sel_Job()

        self.WheezeButton.clicked.connect(self.taggingWheeze)
        self.StridorButton.clicked.connect(self.taggingStridor)
        self.RhonchiButton.clicked.connect(self.taggingRhonchi)        
        self.ContinuousButton.clicked.connect(self.taggingCountinuous)
        self.OthersButton.clicked.connect(self.taggingOthers)
        self.RalesButton.clicked.connect(self.taggingRales)
        self.InspirButton.clicked.connect(self.taggingInspiration)
        self.ExpirButton.clicked.connect(self.taggingExpiration)
        self.NoiseButton.clicked.connect(self.taggingNoise)
        self.nonContinuousButton.clicked.connect(self.taggingnonContinous)
        self.NormalButton.clicked.connect(self.taggingNormal)
        self.NBCButton.clicked.connect(partial(self.taggingNBC, self.NBCButton.text()))
        self.unknownButton.clicked.connect(partial(self.taggingUnknown, self.unknownButton.text()))
        [btn.clicked.connect(partial(self.taggingExtra, btn.text())) for btn in self.extraBtns]
        self.RefreshButton1.clicked.connect(self.playrefreshsnd_NormalVes)
        self.RefreshButton2.clicked.connect(self.playrefreshsnd_NormalTra)
        self.RefreshButton3.clicked.connect(self.playrefreshsnd_Crackle)
        self.RefreshButton4.clicked.connect(self.playrefreshsnd_Rhonchi)
        self.RefreshButton5.clicked.connect(self.playrefreshsnd_Stridor)
        self.RefreshButton6.clicked.connect(self.playrefreshsnd_Wheeze)
        # self.ReviewOnlyChkBox.stateChanged.connect(self.ShowPreviousData)
        self.ScreenShotButton.clicked.connect(partial(self.ScreenShot, None, 'whole'))
        self.taggerNameInput.editingFinished.connect(self.ids.updateTagger)
        self.refreshMode.stateChanged.connect(partial(self.replottingPlaylist, True))
        self.SpectUpdate.clicked.connect(partial(self.replottingPlaylist, False))
        self.Spectzoom.stateChanged.connect(partial(self.replottingPlaylist, False))
        # self.Spectnfft.currentIndexChanged.connect(self.SpectManulMode)
        # self.Spectnfft.highlighted.connect(self.SpectManulMode)
        # self.Spectnoverlap.valueChanged.connect(self.SpectManulMode)
        # self.Spectnoverlap.editingFinished.connect(self.SpectManulMode)
        # self.Spectpad_to.valueChanged.connect(self.SpectManulMode)
        self.PreviousSeg.clicked.connect(self.plotPreviousSeg)
        self.NextSeg.clicked.connect(self.plotNextSeg)
        self.stepback.clicked.connect(self.plotPreviousStep)
        self.stepfor.clicked.connect(self.plotNextStep)
        self.judgement1_btn.clicked.connect(partial(self.saveJudge,0))
        # self.judgement1_btn.clicked.connect(self.testshow)
        self.judgement2_btn.clicked.connect(partial(self.saveJudge,1))
        self.judgement3_btn.clicked.connect(partial(self.saveJudge,2))
        self.clrPlayListBtn.clicked.connect(partial(self.clrPlayList,0))
        self.clrCurrentSongBtn.clicked.connect(partial(self.clrPlayList,1))
        self.savSpectroBtn.clicked.connect(self.SavSpectro)
        self.SoundClipBtn.clicked.connect(self.SavClip)        
        self.Set_ti_hour.valueChanged.connect(self.TimerEdited)
        self.Set_ti_min.valueChanged.connect(self.TimerEdited)
        self.Set_ti_sec.valueChanged.connect(self.TimerEdited)
        self.ShowAITagChkBox.stateChanged.connect(self.showAItag)
        self.myCommentBtn.clicked.connect(partial(self.comment,1))
        self.myTagBtn.clicked.connect(partial(self.comment,0))

        self.show()
       
        self.playZoomState = False
        self.playZoomROIState = False
        pg.setConfigOptions(imageAxisOrder='row-major')
        self.ReSampleFact = 1
        self.manulSpectro = False
        self.ti = 0
        self.tf = self.Set_duration.value()
        self.Tzoomi_local = 0
        self.Tzoomf_local = 1
        # self.segidx = 0

        self.GoldentagColor = 255
        self.Grade1tagColor = 150
        self.UncertaintagColor = 100
        self.xspan_tagColor = np.array([self.untagColor/255,self.UncertaintagColor/255,self.Grade1tagColor/255,self.GoldentagColor/255])
        self.colors_tagColor = np.array([[20,20,20,255],[100,100,100,255],[13,162,212,255],[255,255,0,255]],dtype=np.float)/255
        cmscale = 255 if int(pg.__version__.split('.')[1])>=11 else 1
        self.cmap_tagColor = pg.ColorMap(pos=self.xspan_tagColor, color=self.colors_tagColor*cmscale)
        self.lut_tagColor = self.cmap_tagColor.getLookupTable(0, 1, 256)
        # self.segidx = 0
        self.savedTzoom = False
        self.pageturned = False
        self.sr_acc = 100
        self.savSpectro = False
        # self.polarity_acc_sum = None
        self.wav_len_sec = 0
        self.isZoomin = False
        self.isUnZoom = True
        self.show_my_pretag = False

        self.fnnow = ' '
        self.tmpMsg = ['']
        self.tfNGinfo = []         # information of NG TF result
        self.previoustagfn = ''
        self.myComment = ''
        self.mytag = []
        # self.detectionT = None

        # self.ProjNameLabel.setText('Project: '+ self.config['Project'])
        self.ProjNameLabel.setText(f'{self.tagButtonSelect_name}-{time.strftime("%Y%m%d", time.localtime())}')
        self.ProjName = f'{time.strftime("%Y%m%d", time.localtime())}-{self.tagButtonSelect_name}'

        if self.config['Examining']['goTFscore']:
            with open(f"whereNG-{self.config['Examining']['keywd_trainProj']}.csv", 'r', newline='') as scoreCSV:
                rows = csv.reader(scoreCSV)
                for row in rows:
                    if not os.path.exists(row[0]) and os.path.exists('G:\\我的雲端硬碟\\臨床部\\[臨床部]-[FA1胸音計畫]'):
                        row[0] = row[0].replace('G:\\My Drive','G:\\我的雲端硬碟\\臨床部\\[臨床部]-[FA1胸音計畫]')
                    self.playlist.addMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(row[0])))
                    self.tfNGinfo.append(row[1:])    # Tag path(0), extracted wavfn(1), T_start(2), T_end(3), classification label(4), label list, score list
            self.model.layoutChanged.emit()
        else:
            srcDir = self.config['preloadWAV']
            if os.path.exists(srcDir):
                wavList = [fn for fn in os.listdir(srcDir)
                            if fn.endswith('.wav')
                                or fn.endswith('.WAV')
                                or fn.endswith('.flac')
                                or fn.endswith('.mp3')]
                wavList.sort()
                [self.playlist.addMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(srcDir+'\\'+wavfn))) for wavfn in wavList]
                self.model.layoutChanged.emit()
        
        
        self.sbutils = sbutils.utils(self)  # utils, such as 

    def dragEnterEvent(self, e):
        if e.mimeData().hasUrls():
            e.acceptProposedAction()

    def dropEvent(self, e):
        for url in e.mimeData().urls():
            if (url.toString().endswith('.wav')
                    or url.toString().endswith('.WAV')
                    or url.toString().endswith('.flac')
                    or url.toString().endswith('.mp3')):
                self.playlist.addMedia(
                    QtMultimedia.QMediaContent(url)
                )            

        self.model.layoutChanged.emit()

        # If not playing, seeking to first of newly added + play.
        if self.player.state() != QtMultimedia.QMediaPlayer.PlayingState:
            i = self.playlist.mediaCount() - len(e.mimeData().urls())
            self.playlist.setCurrentIndex(i)
            # self.player.play()

    def open_file(self):
        paths, _ = QtWidgets.QFileDialog.getOpenFileNames(self, "Open file", "", "mp3 Audio (*.mp3);mp4 Video (*.mp4);Movie files (*.mov);All files (*.*)")

        if paths:            
            for path in paths:
                if path.endswith('.wav') or path.endswith('.WAV') or path.endswith('.flac') or path.endswith('.mp3'):
                    self.playlist.addMedia(
                        QtMultimedia.QMediaContent(
                            QtCore.QUrl.fromLocalFile(path)
                        )
                    )

        self.model.layoutChanged.emit()

    # def update_duration(self, mc):
    #     if self.refreshMode.isChecked():
    #         theplayer = self.refreshplayer
    #     else:
    #         theplayer = self.player
                    
    #     # self.timeSlider.setMaximum(theplayer.duration())
    #     duration = theplayer.duration()/1000
    #     # if duration >= 0:
    #         # self.totalTimeLabel.setText(hhmmss(duration))

    def update_position(self, *args):
        if self.refreshMode.isChecked():
            theplayer = self.refreshplayer
        else:
            theplayer = self.player

        position = theplayer.position()/1000
        duration = theplayer.duration()/1000
        # print(f'snd pos:{position}')
        if position >= 0 and theplayer.duration() > 0:
            self.playSndGrpBox.setTitle(hhmmss(position))
            if self.playZoomState:
                playEnd = min(self.Tzoomf+self.pExtraROI-.001, duration-.001)
            elif self.playZoomROIState:
                playEnd = min(self.Tzoomf-.001, duration-.001)
            else:
                playEnd = duration-.001
                playEnd = self.tf
            # print(f'playend{playEnd}')
            if position >= playEnd:
                # print(f'playend:{playEnd/2:.3f} pos:{position/2:.3f}  duration:{duration/2:.3f} noti:{theplayer.notifyInterval()}')
                if self.playZoomState:
                    theplayer.setPosition(int(self.Tzoomi*1000-self.pExtraROI*1000))
                    theplayer.pause()
                    # theplayer.play()
                elif self.playZoomROIState:
                    theplayer.setPosition(int(self.Tzoomi*1000))
                    # theplayer.play()
                    theplayer.pause()
                else:
                    theplayer.setPosition(int(self.ti*1000))
                    theplayer.pause()
                self.playZoomState = False
                self.playZoomROIState = False

        # Disable the events to prevent updating triggering a setPosition event (can cause stuttering).
        # self.timeSlider.blockSignals(True)
        # self.timeSlider.setValue(position)
        # self.timeSlider.blockSignals(False)
        self.p1infLine.blockSignals = True
        self.p1infLine.setValue(position-self.ti)
        self.p1infLine.blockSignals = False

    def playlist_selection_changed(self, ix):
        # We receive a QItemSelection from selectionChanged.
        i = ix.indexes()[0].row()
        self.playlist.setCurrentIndex(i)

    def playlist_position_changed(self, i):
        if i > -1:
            self.player.stop()
            ix = self.model.index(i)
            self.playlistView.setCurrentIndex(ix)

            # initialize preTagPosData
            self.preTagPosData = [[] for i in range(len(self.TagPosData))]
                         
            # ====== export/replay tag data
            if os.path.exists(self.fnnow) and not self.ReviewOnlyChkBox.isChecked():  # if sound file exists in playlist and not reviewonly mode
                self.saveTag(cleartag=True)
            self.fnnow = self.playlist.media(self.playlist.currentIndex()).canonicalUrl().toString()[8:]
            if not os.path.exists(self.fnnow):
                self.clrPlayList(1)
            if 'steth' in self.fnnow:
                self.DeviceName = '3M'
            else:
                self.DeviceName = 'FA1'
            print(f'\nplaylist.index:{self.playlist.currentIndex()}/{self.playlist.mediaCount()}\nfnnow: {self.fnnow}\n{time.strftime("%Y/%m/%d %H:%M:%S", time.localtime())}')
            if self.config['Examining']['goCheckSeverity'] and os.path.exists('./CheckSeverity/judgement.csv'):
                with open('./CheckSeverity/judgement.csv', 'r', newline='', encoding = 'utf-8-sig') as csvfile:
                    rows = csv.reader(csvfile)
                    for row in rows:
                        if row and os.path.basename(row[0]) == os.path.basename(self.fnnow):
                            self.playlist.next()
                            break            
            # === reset tagdata if in reviewonly mode
            if self.ReviewOnlyChkBox.isChecked():
                for tag in self.TagPosData:
                    tag.clear()            
            # === read another tag data, such as serverity.txt
            tag2_fn = f'{self.fnnow[:-4]}_{self.config["tag2_suffix"]}.txt'
            self.p1_extra_info = ['']
            if os.path.exists(tag2_fn):
                with open(tag2_fn,'r',newline='') as csvfile:
                    rows = csv.reader(csvfile)
                    i = 1
                    for row in rows:
                        self.p1_extra_info = [row[0], row[-1]] if i == 2 else ''
                        i += 1
            # === read previous tag data
            self.previoustagfn = ''     # initialize every time
            self.findtagfile()
            # === initialization                    
            if (self.fnnow.endswith('.wav') or self.fnnow.endswith('.WAV')
                    or self.fnnow.endswith('.flac') or self.fnnow.endswith('.mp3')):  # if input file is WAV-format
                self.Grade1TypButton.setChecked(True)   # restore grade1 as default
                self.addmodiTagButton.setChecked(True)  # restore add/modify as default
                self.polarity_acc_sum = 0
                self.movmen_norm_height_RR = 0
                # self.movmean_envelope_scale= 0
                # self.segidx = 0
                self.savedTzoom = False
                self.pageturned = False
                self.wav_len_sec = 0
                self.plotting(self.fnnow)
                if self.isvip:
                    self.RR_sec = None
                    self.RR_lowQ_cnt = 0
                    self.silence_start = 0
                    self.goGetRR()
                self.showAItag()
                # self.plotTagsPos(self.TagPosData)
                if not self.ti:
                    self.Set_ti_sec.setValue(0)
                    self.Set_ti_min.setValue(0)
                    self.Set_ti_hour.setValue(0)
                    self.playSndGrpBox.setTitle('00:00:00')
                else:
                    QtCore.QTimer.singleShot(500,
                                             partial(self.player.setPosition,
                                                     int(self.ti*1000)))
                self.isZoomin = False
                self.isUnZoom = True
                # self.timerclipGrp.setTitle(f'Set Tstart')
                self.timerclipGrp.setTitle(f'{self.ti}~{self.tf}sec(Max. {self.wav_len_sec:.3f}sec)')
                # self.isTimerEdited = False
            else:
                if self.playlist.currentIndex() == self.playlist.mediaCount() and self.playlist.mediaCount()>1:
                    self.playlist.previous()
                elif self.playlist.mediaCount()>1:
                    self.playlist.next()
            
            # with open('log.txt', 'a', newline='') as log:
            #     [log.write(f'{self.tmpMsg[i]}\n') for i in range(len(self.tmpMsg))]
            #     self.tmpMsg = ''
            self.refreshMode.setChecked(False)
            self.Spectzoom.setChecked(False)
            # self.SpectAuto.setChecked(True)
            # self.ShowPreviousData()

    # def toggle_viewer(self, state):
    #     if state:
    #         self.viewer.show()
    #     else:
    #         self.viewer.hide()

    def erroralert(self, *args):
        print(args)    

    def taggingInspiration(self):
        if self.GoldenTypButton.isChecked():
            tagName = 'Inspiration_Golden'
            tagPos = self.InspirGoldenPos
            self.tagColor = self.GoldentagColor
            goTagging = True
        elif self.Grade1TypButton.isChecked():
            tagName = 'Inspiration_Grade1'
            tagPos = self.InspirGrade1Pos
            self.tagColor = self.Grade1tagColor
            goTagging = True
        else:
            self.tmpMsg = f'"Uncertain" is Not an Available Option! Please choose again!\n'
            print(self.tmpMsg)
            self.MsgView.setText(self.tmpMsg)
            goTagging = False
            self.Grade1TypButton.setChecked(True)
            self.addmodiTagButton.setChecked(True)
        if goTagging:
            idx = self.tagButtonSelect_name.index('Inspiration')
            self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])
            goTagging = False

    def taggingExpiration(self):
        if self.GoldenTypButton.isChecked():
            tagName = 'Expiration_Golden'
            tagPos = self.ExpirGoldenPos
            self.tagColor = self.GoldentagColor
            goTagging = True
        elif self.Grade1TypButton.isChecked():
            tagName = 'Expiration_Grade1'
            tagPos = self.ExpirGrade1Pos
            self.tagColor = self.Grade1tagColor
            goTagging = True
        else:
            self.tmpMsg = f'"Uncertain" is Not an Available Option! Please choose again!\n'
            print(self.tmpMsg)
            self.MsgView.setText(self.tmpMsg)
            goTagging = False
            self.Grade1TypButton.setChecked(True)
            self.addmodiTagButton.setChecked(True)
        if goTagging:
            idx = self.tagButtonSelect_name.index('Expiration')
            self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])
            goTagging = False
    
    def taggingWheeze(self):        
        if self.UncertainTypButton.isChecked():
            tagName = 'Wheeze_Uncertain'
            tagPos = self.WheezeUncertainPos
            self.tagColor = self.UncertaintagColor
        elif self.GoldenTypButton.isChecked():
            tagName = 'Wheeze_Golden'
            tagPos = self.WheezeGoldenPos
            self.tagColor = self.GoldentagColor
        else:
            tagName = 'Wheeze_Grade1'
            tagPos = self.WheezeGrade1Pos
            self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('Wheeze')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingStridor(self):
        if self.UncertainTypButton.isChecked():
            tagName = 'Stridor_Uncertain'
            tagPos = self.StridorUncertainPos
            self.tagColor = self.UncertaintagColor
        elif self.GoldenTypButton.isChecked():
            tagName = 'Stridor_Golden'
            tagPos = self.StridorGoldenPos
            self.tagColor = self.GoldentagColor
        else:
            tagName = 'Stridor_Grade1'
            tagPos = self.StridorGrade1Pos
            self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('Stridor')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])
        
    def taggingRhonchi(self):
        if self.UncertainTypButton.isChecked():
            tagName = 'Rhonchi_Uncertain'
            tagPos = self.RhonchiUncertainPos
            self.tagColor = self.UncertaintagColor
        elif self.GoldenTypButton.isChecked():
            tagName = 'Rhonchi_Golden'
            tagPos = self.RhonchiGoldenPos
            self.tagColor = self.GoldentagColor
        else:
            tagName = 'Rhonchi_Grade1'
            tagPos = self.RhonchiGrade1Pos
            self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('Rhonchi')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingCountinuous(self):
        tagName = 'Continuous'
        tagPos = self.ContinuousPos
        self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('Continuous')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingOthers(self):
        tagName = 'Others'
        tagPos = self.OthersPos
        self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('Others')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingRales(self):
        if self.UncertainTypButton.isChecked():
            tagName = 'Rales_Uncertain'
            tagPos = self.RalesUncertainPos
            self.tagColor = self.UncertaintagColor
        elif self.GoldenTypButton.isChecked():
            tagName = 'Rales_Golden'
            tagPos = self.RalesGoldenPos
            self.tagColor = self.GoldentagColor
        else:
            tagName = 'Rales_Grade1'
            tagPos = self.RalesGrade1Pos
            self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('Rales')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])
    
    def taggingNoise(self):
        tagName = 'Noise'
        tagPos = self.NoisePos
        self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('Noise')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingnonContinous(self):
        tagName = 'nonContinuous'
        tagPos = self.nonContinuousPos
        self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('nonContinuous')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingNormal(self):
        tagName = 'Normal'
        tagPos = self.NormalPos
        self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index('Normal')
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingNBC(self, tagName):
        # tagName = 'Normal'
        tagPos = self.NBCPos
        self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index(tagName)
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingUnknown(self, tagName):
        idx = self.tagName.index(tagName)
        tagPos = self.TagPosData[idx]
        self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index(tagName)
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def taggingExtra(self, tagName):
        # tagName = 'Normal'
        idx = self.tagName.index(tagName)
        tagPos = self.TagPosData[idx]
        self.tagColor = self.Grade1tagColor
        idx = self.tagButtonSelect_name.index(tagName)
        self.modifyTagPos(tagName, tagPos, self.pTagPos[idx], self.imaTagPos[idx], self.imgTagPos[idx])

    def playZoom(self):
        # self.Tzoomi, self.Tzoomf = self.region.getRegion()
        if self.refreshMode.isChecked():
            theplayer = self.refreshplayer
        else:
            theplayer = self.player
        theplayer.setPosition(int(self.Tzoomi*1000-self.pExtraROI*1000))
        theplayer.play()
        self.playZoomState = True
        self.playZoomROIState = False
    
    def playZoomROI(self):
        # self.Tzoomi, self.Tzoomf = self.region.getRegion()
        if self.refreshMode.isChecked():
            theplayer = self.refreshplayer
        else:
            theplayer = self.player
        theplayer.setPosition(int(self.Tzoomi*1000))
        theplayer.play()
        self.playZoomROIState = True
        self.playZoomState = False
    
    def playNormal(self):
        self.playZoomState = False
        self.playZoomROIState = False
        if self.refreshMode.isChecked():
            self.refreshplayer.play()
        else:
            self.player.play()      
    
    def dragp1infLine(self):
        if self.refreshMode.isChecked():
            theplayer = self.refreshplayer
        else:
            theplayer = self.player
        theplayer.setPosition(int((self.p1infLine.value()+self.ti)*1000))

    def initialplayerPos(self):
        if self.refreshMode.isChecked():
            theplayer = self.refreshplayer
        else:
            theplayer = self.player
        theplayer.setPosition(int((self.p1infLine.value()+self.ti)*1000))

    def playrefreshsnd_NormalVes(self):
        if self.refreshMode.isChecked():
            sndfn = "refreshSND/Normal vesicular sound v1.wav"
            self.refreshplayer.setMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(sndfn)))
            self.plotting(sndfn)
            self.refreshplayer.play()
    
    def playrefreshsnd_NormalTra(self):
        if self.refreshMode.isChecked():
            sndfn = "refreshSND/Normal Tracheal sound.wav"
            self.refreshplayer.setMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(sndfn)))
            self.plotting(sndfn)
            self.refreshplayer.play()
    
    def playrefreshsnd_Crackle(self):
        if self.refreshMode.isChecked():
            sndfn = "refreshSND/crackle.wav"
            self.refreshplayer.setMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(sndfn)))
            self.plotting(sndfn)
            self.refreshplayer.play()

    def playrefreshsnd_Rhonchi(self):
        if self.refreshMode.isChecked():
            sndfn = "refreshSND/RhonchI.wav"
            self.refreshplayer.setMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(sndfn)))
            self.plotting(sndfn)
            self.refreshplayer.play()
    
    def playrefreshsnd_Stridor(self):
        if self.refreshMode.isChecked():
            sndfn = "refreshSND/Stridor.wav"
            self.refreshplayer.setMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(sndfn)))
            self.plotting(sndfn)
            self.refreshplayer.play()
    
    def playrefreshsnd_Wheeze(self):
        if self.refreshMode.isChecked():
            sndfn = "refreshSND/Wheezing.wav"
            self.refreshplayer.setMedia(QtMultimedia.QMediaContent(QtCore.QUrl.fromLocalFile(sndfn)))
            self.plotting(sndfn)
            self.refreshplayer.play()
    
    def UpdateSpectro(self):
        self.replottingPlaylist(isReadagain=False)

    def UpdateTimer(self):
        self.Set_ti_hour.blockSignals(True)
        self.Set_ti_min.blockSignals(True)
        self.Set_ti_sec.blockSignals(True)
        h, m, s = hhmmss(self.ti, outType=2)
        self.Set_ti_hour.setValue(h)
        self.Set_ti_min.setValue(m)
        self.Set_ti_sec.setValue(s)
        self.Set_ti_hour.blockSignals(False)
        self.Set_ti_min.blockSignals(False)
        self.Set_ti_sec.blockSignals(False)
    
    def TimerEdited(self):
        with open('config.json' , 'r', encoding = 'utf-8-sig') as reader:
            self.config = json.loads(reader.read())
        ti_set = self.Set_ti_hour.value()*60*60 + self.Set_ti_min.value()*60 + self.Set_ti_sec.value()
        if ti_set <= self.wav_len_sec-0.1:
            self.ti = ti_set
            self.tf = self.ti + self.Set_duration.value() if self.wav_len_sec > self.ti+self.Set_duration.value() \
                else self.wav_len_sec
            # print(f'The global ti/tf is set to {self.ti}/{self.tf}sec')
            self.timerclipGrp.setTitle(f'{self.ti:.3f}~{self.tf:.3f}sec(Max. {self.wav_len_sec:.3f}sec)')
        else:
            msg_tmp = 'Over duration of this wav file!'
            self.debugMsg(msg_tmp)
            self.MsgView.setText(msg_tmp)
        self.pageturned = True

    def replottingPlaylist(self, isReadagain=True):
        if not self.ReviewOnlyChkBox.isChecked():
            self.saveTag()
        with open('config.json' , 'r', encoding = 'utf-8-sig') as reader:
            self.config = json.loads(reader.read())
        self.TagTypGrpbox.setFixedHeight(self.config['tagbtngrp_height'])
        self.p1.setFixedHeight(self.config['spectrogram_height'])
        self.win.setMinimumWidth(self.config['plotwidth'])
        if self.fnnow == ' ':
            return
        self.TimerEdited()
        if not self.isZoomin and self.isUnZoom:
            self.ti_unzoom = self.ti
            self.tf_unzoom = self.tf
        print(f'ti_unzoom:{self.ti_unzoom}  tf_unzoom:{self.tf_unzoom}')
        self.isZoomin = True if self.Spectzoom.isChecked() else False
        if self.Spectzoom.isChecked():
            self.ti = max((self.Tzoomi - self.pExtraROI),0)
            self.tf = min((self.Tzoomf + self.pExtraROI), len(self.sndData)/self.sr)            
            print(f'going to zoom:  global Tzoomi:{self.Tzoomi:.3f}(s)  Tzoomf:{self.Tzoomf:.3f}(s)  ti:{self.ti}')
            # self.UpdateTimer()
        elif not self.isZoomin and not self.isUnZoom:   # restore from zoomin
            self.ti = self.ti_unzoom
            self.tf = self.tf_unzoom
            self.Tzoomi_local = self.Tzoomi - self.ti
            self.Tzoomf_local = self.Tzoomf - self.ti
            # self.UpdateTimer()
        self.isUnZoom = False if self.Spectzoom.isChecked() else True
        if not self.refreshMode.isChecked() and not self.playlist.isEmpty():
            self.plotting(self.fnnow, isReadWav=isReadagain)
            if self.isvip and not self.isZoomin and self.isUnZoom:
                self.goGetRR()
            if self.isUnZoom:
                # if not self.show_my_pretag and self.ShowAITagChkBox.isChecked():
                #     self.plotTagsPos(self.TagPosData)
                # else:
                #     self.plotTagsPos(self.preTagPosData)
                self.showAItag()
        self.UpdateTimer()
    
    def plotPreviousSeg(self):
        with open('config.json', 'r', encoding='utf-8-sig') as reader:
            self.config = json.loads(reader.read())
        if self.ti != 0 and not self.Spectzoom.isChecked():
            self.ti -= self.Set_duration.value()\
                        if self.ti - self.Set_duration.value() >= 0\
                        else self.ti
            self.tf = self.ti + self.Set_duration.value()
            self.UpdateTimer()
            self.pageturned = True 
            self.replottingPlaylist(isReadagain=False)
            # self.plotTagsPos(self.TagPosData)
            self.tmpMsg = '' if self.ti - self.Set_duration.value() >= 0\
                        else 'less than display range!'
        else:
            if self.ti == 0:
                self.tmpMsg = f'Already at the 1st Page!'
            else:
                self.tmpMsg = f'Not able to turn page in zoom mode!'
        self.MsgView.setText(self.tmpMsg)
        print(self.tmpMsg)
    
    def plotPreviousStep(self):
        with open('config.json', 'r', encoding='utf-8-sig') as reader:
            self.config = json.loads(reader.read())
        if self.ti != 0 and not self.Spectzoom.isChecked():
            self.ti -= self.config['StepLength']\
                        if self.ti - self.config['StepLength'] >= 0\
                        else self.ti
            self.tf = self.ti + self.Set_duration.value()
            self.UpdateTimer()
            self.pageturned = True 
            self.replottingPlaylist(isReadagain=False)
            # self.plotTagsPos(self.TagPosData)
            self.tmpMsg = '' if self.ti - self.config['StepLength'] >= 0\
                        else 'less than display range!'
        else:
            if self.ti == 0:
                self.tmpMsg = f'Already at the 1st Page!'
            else:
                self.tmpMsg = f'Not able to step in zoom mode!'
        self.MsgView.setText(self.tmpMsg)
        print(self.tmpMsg)
    
    def plotNextSeg(self):
        with open('config.json', 'r', encoding='utf-8-sig') as reader:
            self.config = json.loads(reader.read())
        if self.tf != self.wav_len_sec and not self.Spectzoom.isChecked():
            self.ti += self.Set_duration.value()\
                        if self.tf + self.Set_duration.value() <= self.wav_len_sec\
                        else self.wav_len_sec-self.tf
            self.tf = self.ti + self.Set_duration.value()
            self.UpdateTimer()
            self.pageturned = True        
            self.replottingPlaylist(isReadagain=False)
            self.player.setPosition(int(self.ti*1000))
            # self.plotTagsPos(self.TagPosData)
            self.tmpMsg = '' if self.tf + self.Set_duration.value() <= self.wav_len_sec\
                        else 'over audio length!'
        else:
            if self.tf == self.wav_len_sec:
                self.tmpMsg = f'Already at the last Page!'
            else:
                self.tmpMsg = f'Not able to turn page in zoom mode!'
        self.MsgView.setText(self.tmpMsg)
        print(self.tmpMsg)

    def plotNextStep(self):
        with open('config.json', 'r', encoding='utf-8-sig') as reader:
            self.config = json.loads(reader.read())
        if self.tf != self.wav_len_sec and not self.Spectzoom.isChecked():
            self.ti += self.config['StepLength']\
                        if self.tf + self.config['StepLength'] <= self.wav_len_sec\
                        else self.wav_len_sec-self.tf
            self.tf = self.ti + self.Set_duration.value()
            self.UpdateTimer()
            self.pageturned = True        
            self.replottingPlaylist(isReadagain=False) 
            self.player.setPosition(int(self.ti*1000))
            # self.plotTagsPos(self.TagPosData)
            self.tmpMsg = '' if self.tf + self.config['StepLength'] <= self.wav_len_sec\
                        else 'over audio length!'
        else:
            if self.tf == self.wav_len_sec:
                self.tmpMsg = f'Already at the last Page!'
            else:
                self.tmpMsg = f'Not able to step in zoom mode!'
        self.MsgView.setText(self.tmpMsg)
        print(self.tmpMsg)

    def plotting(self, sndfn, isReadWav=True):
        # detectionT = None
        with open('config.json' , 'r', encoding='utf-8-sig') as reader:
            self.config = json.loads(reader.read())
        print(f'\nmainwindow width x height: {self.MainLayout.geometry().width()} x {self.MainLayout.geometry().height()}')
        print(f'display size: {self.p1.geometry().width():.0f} x {self.p1.geometry().height():.0f}')
        # self.p1.setFixedHeight(500*self.MainLayout.geometry().height()//800)
        # print(f'p1 height:{self.p1.geometry().height()}')
        # --- get (snd data and sample rate of WAV) and (frequency, timing, and data of spectrogram)
        if isReadWav:   # initial data
            if sndfn.endswith('mp3'):
                self.sndData, self.sr = librosa.load(sndfn, sr=None)
            else:
                self.sndData, self.sr = sf.read(sndfn)
            if self.sndData.ndim == 2:  # ignore one of dual channel
                print(f'\n\tThe original wav is NOT mono! ch = {self.sndData.ndim}')
                self.sndData = self.sndData[:,0]                
            if self.sr != 4000:
                print(f'\n\toriginal sampling rate = {self.sr}! resampling to 4000Hz!')
                self.sndData, self.sr = librosa.load(sndfn, sr=4000)
            if "filtWAV" in self.config.keys() and len(self.config['filtWAV']['fcut']):
                self.sndData = self.sbutils.bwfilter(self.sndData, self.sr,
                                            self.config['filtWAV']['fcut'],
                                            N_filt=self.config['filtWAV']['N'],
                                            filtype=self.config['filtWAV']['typ'])
            self.wav_len_sec = len(self.sndData)/self.sr
            self.ti = (0 if self.ti >= self.wav_len_sec and self.tf > self.wav_len_sec
                         else self.ti)
            self.tf = (min(self.Set_duration.value(), len(self.sndData)/self.sr)
                        if self.ti >= self.wav_len_sec or self.tf > self.wav_len_sec
                        else self.tf)
            self.ti_unzoom = self.ti
            self.tf_unzoom = self.tf
            self.AutoCorr_accum = np.array([])
            self.isgetRR = False
        else:
            print(f'\tNo Read again!')
        self.tmpMsg = f'snd len:{len(self.sndData)}({len(self.sndData)/self.sr:.3f}s)  snd sr:{self.sr}Hz  snd dim:{self.sndData.ndim} snd shape:{self.sndData.shape}'
        print(self.tmpMsg)        
        # ========= plot spectrogram
        # === define display range
        if self.refreshMode.isChecked():
            self.ti = 0
            self.tf = len(self.sndData)/self.sr
        self.duration = self.tf-self.ti
        # === update duration when PageLength was just altered
        if self.Set_duration.value() != self.duration and \
                self.Set_duration.value() <= self.wav_len_sec and \
                not self.Spectzoom.isChecked() and not self.pageturned:
            self.tf = self.ti + self.Set_duration.value()
            self.duration = self.tf-self.ti
            self.AutoCorr_accum = np.array([])
        self.ti_local = 0
        self.tf_local = self.duration
        self.idxi = int(self.ti*self.sr)
        self.idxf = int(self.tf*self.sr)
        print(f'display range(global/local) of time: ti:{self.ti:.3f}/{self.ti_local:.3f}(s)  tf:{self.tf:.3f}/{self.tf_local:.3f}(s)  duration:{self.duration:.3f}s')
        print(f'display range     of wav data index:  i:{self.idxi}({self.idxi/self.sr:.3f}s)  f:{self.idxf}({self.idxf/self.sr:.3f}s)') 
        # ====== get spectrogram
        fftlen, noverlap, pad_to, self.im_spectro, f, Sxx, logSx = self.sbutils.getSpectrData(self.sndData)
        # === smoothing spectrogram if requested
        if self.config['Envelope']['GoSmooSpectro']:
            Sxx_filtered = signal.medfilt2d(Sxx,self.config['Envelope']['medfilt_kernel_Vsize']) if self.isvip \
                else signal.medfilt2d(Sxx,[31,1])
            Sxx_filtered = signal.medfilt2d(Sxx_filtered,self.config['Envelope']['medfilt_kernel_Hsize']) if self.isvip \
                else signal.medfilt2d(Sxx,[1,7])
            logSx_filtered = np.log10(np.where(Sxx_filtered!=0, Sxx_filtered, 1e-20))
            logSx_filtered = logSx_filtered[0:int(len(logSx_filtered)/f[-1]*self.Spectfmax.value()),:]
            if self.isvip:
                print(f'logSx>   max:{np.max(logSx):.3f}  min:{np.min(logSx):.3f}')
                print(f'logSx_filtered>   max:{np.max(logSx_filtered):.3f}  min:{np.min(logSx_filtered):.3f}')
        else:
            logSx_filtered = logSx
        # === plot spectrogram on p1
        self.p1.clear()
        self.img = pg.ImageItem()
        self.p1.addItem(self.img)
        self.img.setImage(logSx_filtered, autoDownsample=False)
        self.img.scale((self.duration)/np.size(Sxx, axis=1), f[-1]/np.size(Sxx, axis=0))    # correct scale
        # print(f'p1 sale t[-1]{t[-1]}  npsize{np.size(self.Sxx, axis=1)}  {t[-1]/np.size(self.Sxx, axis=1)}')
        # -- set colormap of spectrogram
        colorObj = cm.get_cmap(self.config['Envelope']["cmap"], self.config['Envelope']['max_cmap_seg'])
        xspan = np.linspace(0,1,12)
        xspan_colorObj = np.linspace(0,1,self.config['Envelope']['max_cmap_seg'])
        cmscale = 255 if int(pg.__version__.split('.')[1])>=11 else 1
        cmap = pg.ColorMap(pos=xspan,
                            color=np.array(colorObj(xspan_colorObj),
                                                dtype=np.float)
                                            *cmscale)
        lut = cmap.getLookupTable(0.0, 1.0, 256)
        self.img.setLookupTable(lut)
        if not self.config['SmallerScreen']:
            vmin = float(self.Spectvmin.text())/10
            vmax = float(self.Spectvmax.text())/10
        else:
            vmin = self.config['Spectrogram']['vmin']
            vmax = self.config['Spectrogram']['vmax']
        self.img.setLevels([vmin, vmax])      # dynamic range of display
        # ========= p1 ROI
        if not self.Spectzoom.isChecked():  # not zoom mode            
            if self.savedTzoom == False:    # assign ROI Range at the 1st time
                # self.Tzoomi_local = self.tf_local/10
                # self.Tzoomf_local = self.tf_local/5
                if self.config['Examining']['goTFscore']:
                    if float(self.tfNGinfo[self.playlist.currentIndex()][2]) > self.tf:
                        self.plotNextSeg()
                    self.Tzoomi_local = float(self.tfNGinfo[self.playlist.currentIndex()][2])-self.ti
                    self.Tzoomf_local = float(self.tfNGinfo[self.playlist.currentIndex()][3].split('-')[0])-self.ti
                self.Tzoomi = self.Tzoomi_local + self.ti
                self.Tzoomf = self.Tzoomf_local + self.ti
                self.savedTzoom = True
            # else:   # define ROI range
            #     self.Tzoomi_local = self.Tzoomi - self.ti
            #     self.Tzoomf_local = self.Tzoomf - self.ti
            # print(f'           global/local Tzoomi:{self.Tzoomi:.3f}/{self.Tzoomi_local:.3f}(s)  global/local Tzoomf:{self.Tzoomf:.3f}/{self.Tzoomf_local:.3f}(s)')
        else: # zoom mode            
            self.Tzoomi_local = self.Tzoomi - self.ti
            self.Tzoomf_local = self.Tzoomf - self.ti
            print(f'zoom mode {self.Tzoomi_local} {self.Tzoomi} {self.ti}')
            print(f'zoom mode {self.Tzoomf_local} {self.Tzoomf} {self.tf}')
        if self.Tzoomf_local > self.duration or self.Tzoomi_local > self.duration:
            self.Tzoomi_local = self.tf_local/10
            self.Tzoomf_local = self.tf_local/5
        self.p1region = pgLinearRegionItem.LinearRegionItem(values=[self.Tzoomi_local,
                                                                    self.Tzoomf_local],
                                                            pen=pg.mkPen('y',width=2),
                                                            bounds=[0,self.duration],
                                                            brush=pg.mkBrush(color=(255,255,0,25)),
                                                            swapMode='push')
        if self.pageturned: # just turned page
            # self.p1region.setRegion((self.tf_local/10,self.tf_local/5))
            self.pageturned = False
            self.Tzoomi_local, self.Tzoomf_local = self.p1region.getRegion()  # local ti,tf
            self.Tzoomi = self.Tzoomi_local + self.ti  # global ti, tf
            self.Tzoomf = self.Tzoomf_local + self.ti        
        print(f'current region:({self.p1region.getRegion()[0]:.3f},{self.p1region.getRegion()[1]:.3f})  global/local Tzoomi:{self.Tzoomi:.3f}/{self.Tzoomi-self.ti_unzoom:.3f}(s)  Tzoomf:{self.Tzoomf:.3f}/{self.Tzoomf-self.ti_unzoom:.3f}(s)')
        self.p1region.setZValue(10)
        self.p1region.sigRegionChangeFinished.connect(self.p1RegionUpdate)
        self.p1.addItem(self.p1region, ignoreBounds=True)
        # --- axisItem
        # == x
        if self.Spectzoom.isChecked():
            TickStep = self.duration/11
            digit = 4
        else:
            TickStep = self.duration/30 if self.tf < 1000 else self.duration/20
            digit = 2
        # print(f'TickStep:{TickStep}')
        xtick_ori = (np.arange(self.ti_local,self.tf_local+TickStep,TickStep))
        xtick_new = [ round(i,digit) for i in list(np.arange(self.ti,self.tf+TickStep,TickStep))]
        # print(f'ori:{xtick_ori}')
        # print(f'new:{xtick_new}')
        xdict = zip(xtick_ori, xtick_new)
        xxdict = list(xdict)
        # print(xxdict)
        p1axisX = self.p1.getAxis('bottom')
        p1axisX.setTicks([xxdict])
        # == y
        p1axisY = self.p1.getAxis('left')
        if not self.config['Envelope']['GoEnvelop']:
            yTickStep = self.Spectfmax.value()/20
            ytick_ori = (np.arange(0, self.Spectfmax.value()+yTickStep, yTickStep))
            mag_snd = (self.config['Envelope']['mag_snd']
                        if self.config['Envelope']['mag_snd']
                        else 1/self.sndData[self.idxi:self.idxf].max())
            yTickStep = 0.1/mag_snd
            while not int(yTickStep * 10**digit):
                digit += 1
            ytick_new = [ round(i,digit) for i in list(np.arange(-1/mag_snd, 1/mag_snd+yTickStep,yTickStep))]
            p1axisY.setTicks([list(zip(ytick_ori, ytick_new))])
        else:
            yTickStep = 100
            ytick_ori = (np.arange(0, self.Spectfmax.value()+yTickStep, yTickStep))
            # ytick_new = [ round(i,digit) for i in list(np.arange(-1/self.config['Envelope']['mag_snd'],1/self.config['Envelope']['mag_snd']+yTickStep,yTickStep))]
            p1axisY.setTicks([list(zip(ytick_ori, ytick_ori))])
        p1axisY.setTickSpacing(100,50)
        # --- SND waveform
        self.p1Rangei = self.idxi
        self.p1Rangef = self.idxf
        if self.Spectzoom.isChecked():  
            self.p1Rangei = max(int((self.Tzoomi-self.pExtraROI)*self.sr),0)
            self.p1Rangef = int(min((self.Tzoomf+self.pExtraROI)*self.sr,len(self.sndData)))
            print(f'p1Rangei:{self.p1Rangei}  p1Rangef:{self.p1Rangef}')
        if not self.config['Envelope']['GoEnvelop']:
            if self.p1Rangef-self.p1Rangei >= 1500:
                fact_resize = min(12, (self.p1Rangef-self.p1Rangei)//750)
                # downsampling for plotting waveform
                sndData_resize = signal.decimate(self.sndData[self.p1Rangei:self.p1Rangef],
                                                 fact_resize, ftype='fir', zero_phase=True)
            else:
                sndData_resize = self.sndData[self.p1Rangei:self.p1Rangef]
                fact_resize = 1
            alpha = min(255, self.WAValpha.value() * 10 / self.duration)     # transparent of waveform
            if self.isvip:
                print(f'waveform alpha: {alpha:.1f}')
            print(f'plotted SND waveform length:{len(sndData_resize)}'
                  f'(original length:{len(self.sndData[self.p1Rangei:self.p1Rangef])})'
                  f'ratio:{fact_resize}')
            magFact_org = self.Spectfmax.value()*mag_snd/2
            print(f'Max/min original wav data: {max(sndData_resize):.3e} / {min(sndData_resize):.3e}')
            print(f'Max/min scaled wav data: {max(sndData_resize*magFact_org+self.Spectfmax.value()/2):.0f}'
                  f'/ {min(sndData_resize*magFact_org+self.Spectfmax.value()/2):.0f}')
            self.p1.plot(np.arange(0, len(sndData_resize))/self.sr*fact_resize, \
                                    sndData_resize*magFact_org+self.Spectfmax.value()/2, \
                                    pen=pg.mkPen(width=1, color=(0,255,255,alpha)), \
                                    symbolBrush=(255,255,0,alpha//22), symbolPen=pg.mkPen(width=1,
                                    color=(255,255,255,(alpha*.8))), symbolSize=3)
            # print(f'\t original shape x:{np.shape(np.arange(0, self.p1Rangef-self.p1Rangei))}   y:{np.shape(self.sndData[self.p1Rangei: self.p1Rangef])}')  
        self.p1.autoRange()
        self.p1.setYRange(0,self.Spectfmax.value())
        fft_abs = []
        fft_freq = []
        fft_pkidx = []
        fft_pkfreq = []
        fft_pk = []
        fft_dataname = []
        fft_abs_std = []
        fft_abs_avg = []
        # === plotting envelop from smoothed spectrogram
        # == get envelop data from the snd data generated by smoothed spectrogram
        if self.config['Envelope']['GoEnvelop'] and self.duration > 3 and self.duration < 121:
            self.t_envelope_sec, self.envelope_plot, self.stft_abs_RR, self.Intensity_wav \
                = self.sbutils.getEnvelopWAV(self.sndData[self.p1Rangei: self.p1Rangef], fftlen, noverlap, pad_to)
            alpha = min(255, self.WAValpha.value() * 40 / self.duration)     # transparent of waveform
            if self.isvip:
                print(f'waveform alpha: {alpha:.1f}')
            self.p1.plot(self.t_envelope_sec, self.envelope_plot, brush=(250,250,250,20),
                         pen=pg.mkPen(width=2, color=(0,255,255,alpha)))
            # self.p1.plot(self.t_envelope_sec, sum_freqband, fillLevel=-0.9, brush=(250,250,250,20), pen=pg.mkPen(width=2, color=(0,255,255,alpha)))
            self.isgetRR = True if len(self.t_envelope_sec) else False
        # === plot ACC data
        if 'Envelope' in self.config.keys() and 'CH-' in os.path.basename(sndfn):
            idx_CH = os.path.basename(sndfn)[:-4].find('CH-')+3  
            sndCH = os.path.basename(sndfn)[:-4][idx_CH:].split('_')[0].split('-')[0]
            accCH = f'{int(sndCH)//2}'
            accfn = f'{os.path.split(sndfn)[0]}/{os.path.basename(sndfn).replace("CH-"+sndCH,"CH-"+accCH).replace("wav","csv")}'
            if not os.path.exists(accfn):
                print(f"acc's fn:{accfn} is NOT found")
            else:
                print(f"acc's fn:{accfn} is found")
                if isReadWav:
                    # === import accData
                    self.accData = []
                    with open(accfn, 'r', newline='') as csvfile:
                        rows = csv.reader(csvfile)
                        for row in rows:
                            if row[0] != 'x_raw':
                                self.accData.append(row[6])
                    self.accData = np.array(self.accData, dtype=np.float32)
                # === judge polarity of acc data
                if not self.Spectzoom.isChecked() and abs(self.polarity_acc_sum) < 20 and len(self.envelope_plot):
                    idxi = int(self.sr_acc*self.ti)
                    idxf = int(self.sr_acc*self.tf)
                    len_acc = len(self.accData[idxi:idxf])
                    envelope_resize_acc = signal.resample(self.envelope_plot, len_acc)                    
                    # idxf = int(self.sr_acc*self.ti)+len(envelope_resize_acc)  # make sure the same length                    
                    # PV_plus = [np.max(envelope_resize_acc[len_acc//3*i+len_acc//30:len_acc//3*(i+1)-len_acc//30]\
                    #     +self.accData[idxi:idxf][len_acc//3*i+len_acc//30:len_acc//3*(i+1)-len_acc//30])\
                    #     -np.min(envelope_resize_acc[len_acc//3*i+len_acc//30:len_acc//3*(i+1)-len_acc//30]\
                    #     +self.accData[idxi:idxf][len_acc//3*i+len_acc//30:len_acc//3*(i+1)-len_acc//30]) for i in range(3)]
                    # PV_minus = [np.max(envelope_resize_acc[len_acc//3*i+len_acc//30:len_acc//3*(i+1)-len_acc//30]\
                    #     -self.accData[idxi:idxf][len_acc//3*i+len_acc//30:len_acc//3*(i+1)-len_acc//30])\
                    #     -np.min(envelope_resize_acc[len_acc//3*i+len_acc//30:len_acc//3*(i+1)-len_acc//30]\
                    #     -self.accData[idxi:idxf][len_acc//3*i+len_acc//30:len_acc//3*(i+1)-len_acc//30]) for i in range(3)]
                    # isPlus = 0
                    # for i in range(3):
                    #     isPlus += (1 if PV_plus[i]>PV_minus[i] else -1)
                    isPlus = 0
                    corr_plus_ACC_Envelope = np.corrcoef(envelope_resize_acc, self.accData[idxi:idxf])[0,1]
                    corr_minus_ACC_Envelope = np.corrcoef(envelope_resize_acc, -self.accData[idxi:idxf])[0,1]                    
                    isPlus += (1 if corr_plus_ACC_Envelope > corr_minus_ACC_Envelope else -1)
                    self.polarity_acc_sum += isPlus
                    if self.isvip:
                        print(f'ACC> correlation coeff.: plus {corr_plus_ACC_Envelope:.2f}  minus:{corr_minus_ACC_Envelope:.2f} polarity index:{self.polarity_acc_sum}')
                polarity_acc = (1 if self.polarity_acc_sum>0 else -1)
                idxi = int(self.sr_acc*self.ti)
                idxf = int(self.sr_acc*self.tf)              
                DC_acc = abs(np.min(polarity_acc*self.accData[idxi:idxf]))
                magfact_acc = self.Spectfmax.value()/2/np.max(polarity_acc*self.accData[idxi:idxf]+DC_acc)
                self.p1.plot(np.arange(len(self.accData[idxi:idxf]))/self.sr_acc, (polarity_acc*self.accData[idxi:idxf]+DC_acc)*magfact_acc, \
                    pen=pg.mkPen(width=3, color=(60,255,60,alpha)))
                # === fft of ACC data
                if self.config['Envelope']['Gofft']:
                    fftabs, fftfreq, fftpkidxs, fftpkfreq, fftpks, fftdataname, fftabsstd, fftabsavg = self.peaks_of_fft(self.accData[idxi:idxf], self.sr_acc, 'acc Data')
                    fft_abs.append(fftabs)
                    fft_freq.append(fftfreq)
                    fft_pkidx.append(fftpkidxs)
                    fft_pkfreq.append(fftpkfreq)
                    fft_pk.append(fftpks)
                    fft_dataname.append(fftdataname)
                    fft_abs_std.append(fftabsstd)
                    fft_abs_avg.append(fftabsavg)
        # === plot fft of envelopWAV, ACC
        if len(fft_abs):
            xf = self.config['Envelope']['fft_pk_roi_fmax']
            step = self.config['Envelope']['xtick_step_fft']
            xticklabel_new = [ round(i,0) for i in list(np.arange(0,xf,xf/step)*60)]
            fig_fft = plt.figure(figsize=(12,7))
            fig_fft.suptitle(f'{os.path.basename(sndfn)}   {self.ti:.3f}~{self.tf:.3f}s')
            ax_fft = fig_fft.subplots(len(fft_abs),1)
            if len(fft_abs) == 1:
                ax_fft = [ax_fft]
            for i in range(len(fft_abs)):
                ax_fft[i].plot(fft_freq[i], fft_abs[i],'-o')
                ax_fft[i].set_xlim((0,xf))
                ax_fft[i].set_xticks(np.arange(0,xf,xf/step))
                ax_fft[i].set_xticklabels(xticklabel_new) 
                [ax_fft[i].annotate(f'({fft_pkfreq[i][j]*60:.1f},{fft_pk[i][j]:.1f})', (fft_pkfreq[i][j], fft_pk[i][j]), fontsize=14, color='k'
                    ,xytext=(10, 0), textcoords='offset points') for j in range(len(fft_pk[i]))]
                ax_fft[i].set_title(f'fft of {fft_dataname[i]}  std:{fft_abs_std[i]:.3f}  avg:{fft_abs_avg[i]:.4f}')                
            plt.show()
        # --- timing indicator
        self.p1infLine = pgInfiniteLine.InfiniteLine(pos=1, angle=90, movable=True, label='{value}', labelOpts={'position':0.95, 'xi':self.ti}, name='Vline', bounds=[0,self.tf_local], pen=pg.mkPen('g',width=2))
        self.p1infLine.setZValue(20)
        self.p1infLine.sigDragged.connect(self.dragp1infLine)
        self.p1.addItem(self.p1infLine, ignoreBounds=True)
        self.initialplayerPos()
        # --- text
        # wavfile path
        splitfn = sndfn.split('/')
        self.p1txt = pg.TextItem(self.p1text(splitfn), color=(0,255,255,255), anchor=(0.5,0))
        self.p1.addItem(self.p1txt)
        self.p1txt.setPos(self.duration*.4,self.Spectfmax.value()*0.95)
        # extra info
        self.p1txt_extra = pg.TextItem(f'{self.config["tag2_suffix"]}:{self.p1_extra_info[0]}', color=(0,255,255,255), anchor=(0,0))
        self.p1.addItem(self.p1txt_extra)
        self.p1txt_extra.setPos(0, self.Spectfmax.value()*0.95)        
        self.pExtraROI = (self.Tzoomf-self.Tzoomi)/5
        # sitePhoto
        if not self.config['SmallerScreen']:
            pixmap = QtGui.QPixmap('images/site.png')
            if ('CH-15' in self.fnnow) or ('CH-14' in self.fnnow):
                pixmap = QtGui.QPixmap('images/site1.png')
            elif ('CH-13' in self.fnnow) or ('CH-12' in self.fnnow):
                pixmap = QtGui.QPixmap('images/site2.png')
            elif ('CH-11' in self.fnnow) or ('CH-10' in self.fnnow):
                pixmap = QtGui.QPixmap('images/site4.png')
            elif ('CH-9' in self.fnnow) or ('CH-8' in self.fnnow):
                pixmap = QtGui.QPixmap('images/site5.png')
            elif ('CH-7' in self.fnnow) or ('CH-6' in self.fnnow):
                pixmap = QtGui.QPixmap('images/site6.png')
            elif ('CH-5' in self.fnnow) or ('CH-4' in self.fnnow):
                pixmap = QtGui.QPixmap('images/site8.png')
            self.sitePhotoLabel.setPixmap(pixmap)
        # # ========= Spectral Entropy
        # self.getPSE(self.sndData[self.idxi:self.idxf], self.sr)
        # # ========= RMS energy
        # self.getRMSEnergy(self.sndData[self.idxi:self.idxf])
        # # detectionT = DetectionThread(self)
        # # detectionT.start()


    def goGetRR(self):
        if self.isvip and self.isgetRR and self.duration > 4 and self.duration < 61:
            # t = threading.Thread(target=self.getRR_autocorr, args=(self.stft_abs_RR, self.envelope_plot, self.Intensity_wav))
            # t.setDaemon(True)
            # t.start()
            # t.join()
            # t2 = threading.Thread(target=self.find_local_min_max, args=(self.t_envelope_sec, self.envelope_RR, True))
            # t2.setDaemon(True)
            # t2.start()
            # self.getRR_autocorr(self.stft_abs_RR, self.envelope_plot, self.Intensity_wav)
            is_plot_locmxmi_detail = self.config['Envelope']['plot_locmxmi_detail']
            loc_min, loc_max = self.sbutils.find_local_min_max(self.t_envelope_sec, self.envelope_RR, is_plot_locmxmi_detail)
            self.p1.plot(self.t_envelope_sec[loc_min], self.envelope_plot[loc_min], pen=None, symbol='+',
                symbolBrush=(0,255,0), symbolPen=pg.mkPen(width=1, color=(0,0,0)), symbolSize=20)
            if loc_max.size:
                self.p1.plot(self.t_envelope_sec[loc_max], self.envelope_plot[loc_max], pen=None, symbol='x',
                    symbolBrush=(255,255,0), symbolPen=pg.mkPen(width=1, color=(0,0,0)), symbolSize=20)
            if self.config['Envelope']['getIEtag']:
                self.sbutils.getIEtag(loc_min, loc_max)
            # t2.join()
    
    def peaks_of_fft(self, data, sr, data_name):
        # === fft of envelopwav
        fft_abs = abs(np.fft.fft(data))
        fft_freq = np.fft.fftfreq(len(data), 1/sr)
        if len(fft_abs)//2*2 != len(fft_abs):
            fft_idxf = len(fft_abs)//2
        else:
            fft_idxf = len(fft_abs)//2-1
        # == peaks of fft
        fft_roi_idx = np.nonzero(fft_freq[1:fft_idxf] < self.config["Smoothing"]["fft_pk_roi_fmax"])
        pkIdxs,_ = signal.find_peaks(fft_abs[1:fft_idxf], prominence=self.config['Envelope']['fft_pk_prominence_ratio_std']*np.std(fft_abs[fft_roi_idx]), \
            height=self.config['Envelope']['fft_pk_height_ratio_avg']*np.mean(fft_abs[fft_roi_idx]))
        pk = np.array([fft_abs[pki+1] for pki in pkIdxs])
        pkfreq = np.array([fft_freq[pki+1] for pki in pkIdxs])
        # = check if period is unstable
        for idx in pkIdxs:
            if fft_abs[idx+1-2] > self.config['Envelope']['unstable_pk_ratio'] * fft_abs[idx+1] or \
                fft_abs[idx+1+2] > self.config['Envelope']['unstable_pk_ratio'] * fft_abs[idx+1]:
                print(f'\t({fft_freq[idx+1]*60:.1f}RR)  The RR is unstable!')
        fft_abs_std = np.std(np.delete(fft_abs[fft_roi_idx], pkIdxs+1))
        fft_abs_avg = np.mean(np.delete(fft_abs[fft_roi_idx], pkIdxs+1))
        print(f'\t{data_name}\n\tfft std:{fft_abs_std:.3f}  avg:{fft_abs_avg:.4f}')
        print(f'\tpkidx:{pkIdxs}\n\tpk_freq:{np.round(pkfreq*60,3)}RR\n\tpk:{np.round(pk,3)}')
        print(f'{data_name} fft>\tlen:{len(fft_abs)//2}\tshape:{fft_abs.shape}\t\
            freq:1~{fft_freq[-1]:.3f}(Hz)\tfreq step:{fft_freq[1]:.3f}Hz({fft_freq[1]*60:.3f}RR)')
        return fft_abs[1:fft_idxf], fft_freq[1:fft_idxf], pkIdxs, pkfreq, pk, data_name, fft_abs_std, fft_abs_avg

    
    def plotTagsPos(self, tag_pos):
        # downsampling audio data to plot tag position efficiently
        self.ReSampleFact = len(self.sndData[self.idxi:self.idxf])/self.sr*10/800
        # reset/initialize plot of tag position
        self.imaTagPos = []
        self.imaPreTagPos = []
        self.roiTagPos = []
        self.imaY = int(len(self.sndData[self.idxi:self.idxf])/self.sr*10/4/self.ReSampleFact)

        # --- last row as a time scale
        self.pTagPos[-1].clear()
        imaY = min(int(self.duration*100/4), 512)
        imaTagPos_ruler = np.ones((imaY, imaY*4))*self.untagColor
        imgTagPos_ruler = pg.ImageItem()
        self.pTagPos[-1].addItem(imgTagPos_ruler)
        imgTagPos_ruler.setImage(imaTagPos_ruler)
        imgTagPos_ruler.scale((self.duration)/(imaY*4), 1/imaY)    # correct scale
        # -- axisItem of time scale
        TickStep = self.duration/21
        digit = 2
        xtick_ori = (np.arange(self.ti_local,self.tf_local+TickStep,TickStep))
        xtick_new = [ round(i,digit) for i in list(np.arange(self.ti,self.tf+TickStep,TickStep))]
        xdict = zip(xtick_ori, xtick_new)
        xxdict = list(xdict)
        pTagPosAxis = self.pTagPos[-1].getAxis('bottom')
        pTagPosAxis.setTicks([xxdict]) 

        # --- tag position
        for idxButton in range(len(self.tagButtonSelect_name)):  # tagPos rows to be shown
            self.pTagPos[idxButton].clear() # reset
            self.imaTagPos.append(np.ones((self.imaY, self.imaY*4))*self.untagColor) # intitialize plot of tag position(2D array)

            for idxTagTyp in self.TagTypIdx:    # which tag to be shown
                if (self.tagButtonSelect_name[idxButton] == self.tagName[idxTagTyp].split('_')[0]
                    or
                    self.tagButtonSelect_name[idxButton] == self.tagName[idxTagTyp]):    # match tag row with tagName
                    tagColor = self.untagColor
                    if 'Golden' in self.tagName[idxTagTyp]:
                        tagColor = self.GoldentagColor                            
                    elif 'Uncertain' in self.tagName[idxTagTyp]:
                        tagColor = self.UncertaintagColor
                    else:   # grade1
                        tagColor = self.Grade1tagColor
                    for j in range(len(tag_pos[idxTagTyp])): # read tag data to define image
                        tagti = (float(tag_pos[idxTagTyp][j][0])-self.ti)   # local ti
                        tagtf = (float(tag_pos[idxTagTyp][j][1])-self.ti)   # local tf
                        # print(f'global ti:{self.ti}  local> tagti:{tagti}  tagtf:{tagtf}')
                        if tagtf > 0:   # tag area is within visible range
                            if tagti <= 0:  # only part of tag range is within visible range
                                tagti = 0
                            self.imaTagPos[idxButton][:,int(tagti*10/self.ReSampleFact):int(tagtf*10/self.ReSampleFact)] = tagColor

            self.imgTagPos[idxButton] = pg.ImageItem()
            self.pTagPos[idxButton].addItem(self.imgTagPos[idxButton])
            self.imgTagPos[idxButton].setImage(self.imaTagPos[idxButton])
            self.imgTagPos[idxButton].setLookupTable(self.lut_tagColor)
            self.imgTagPos[idxButton].setLevels([20, 255])
            # self.roiTagPos.append(pg.RectROI([(self.Tzoomi_local)*10/self.ReSampleFact, 0], [(self.Tzoomf_local-self.Tzoomi_local)*10/self.ReSampleFact, int(len(self.sndData[self.idxi:self.idxf])/self.sr*10/3/self.ReSampleFact)], pen=(0,9), movable=False, removable=False))
            self.roiTagPos.append(pg.RectROI([(self.Tzoomi-self.ti_unzoom)*10/self.ReSampleFact, 0],
                                             [(self.Tzoomf_local-self.Tzoomi_local)*10/self.ReSampleFact, self.imaY],
                                             pen=(0,9), movable=False, removable=False))
            self.pTagPos[idxButton].addItem(self.roiTagPos[idxButton])
            self.roiTagPos[idxButton].setAcceptedMouseButtons(QtCore.Qt.LeftButton)
            self.roiTagPos[idxButton].removeHandle(self.roiTagPos[idxButton].getHandles()[0])
            self.roiTagPos[idxButton].sigClicked.connect(partial(self.clicktagroi,self.tagButtonSelect[idxButton]))
            # self.roiTagPos[idxButton].addTranslateHandle((0,0))

    def clicktagroi(self, btn):
        btn.click()
    
    def modifyTagPos(self, TagName, TagPos, pltItem, ima, imgItem):
        if not self.refreshMode.isChecked():
            # edit tag position
            idx = [] 
            goAdd = True      
            # self.tmpMsg ='' 
            for i in range(len(TagPos)):    # check if overlap with existing tag
                # 1. tag's head <= head of roi < tag's end
                # 2. tag's head < end of roi <= tag's end
                # 3. tag is within roi, but not equal to roi
                if (self.Tzoomi >= TagPos[i][0] and self.Tzoomi < TagPos[i][-1])\
                    or (self.Tzoomf > TagPos[i][0] and self.Tzoomf <= TagPos[i][-1]) \
                    or (self.Tzoomi < TagPos[i][0] and self.Tzoomf > TagPos[i][-1]):
                    idx.append(i)
            if len(idx)>1:
                self.tmpMsg = f'overlap more than 1 existing {TagName}{idx}.'
                print(self.tmpMsg)
                goAdd = False
            if self.addmodiTagButton.isChecked() and len(idx) <= 1:   # add & modify
                if len(idx) == 1:   # modify
                    print(f'This timeslot is overlaped with the existing {TagName}{idx}\nGo to "modify" mode')
                    ima[:,int((TagPos[idx[0]][0]-self.ti_unzoom)*10/self.ReSampleFact):int((TagPos[idx[0]][1]-self.ti_unzoom)*10/self.ReSampleFact)] = self.untagColor
                    TagPos[idx[0]] = ( self.Tzoomi, self.Tzoomf )
                    ima[:,int((TagPos[idx[0]][0]-self.ti_unzoom)*10/self.ReSampleFact):int((TagPos[idx[0]][1]-self.ti_unzoom)*10/self.ReSampleFact)] = self.tagColor
                    goAdd = False
                    self.tmpMsg = f'{TagName}[{idx}] is modified.'
                    self.MsgView.setText(self.tmpMsg)
                else:   # add
                    TagPos.append(( self.Tzoomi, self.Tzoomf ))
                    ima[:,int((TagPos[-1][0]-self.ti_unzoom)*10/self.ReSampleFact):int((TagPos[-1][1]-self.ti_unzoom)*10/self.ReSampleFact)] = self.tagColor
                    self.tmpMsg = f'{TagName}[{len(TagPos)}] is added.'
                    print(self.tmpMsg)        
                    self.MsgView.setText(self.tmpMsg)
            elif self.DelTagButton.isChecked() and len(idx) > 0:   # delete
                for i in range(len(idx)):
                    idxTmp = idx.pop()
                    ima[:,int((TagPos[idxTmp][0]-self.ti_unzoom)*10/self.ReSampleFact):int((TagPos[idxTmp][1]-self.ti_unzoom)*10/self.ReSampleFact)] = self.untagColor
                    TagPos.pop(idxTmp)
                    self.tmpMsg = f'{TagName}[{idxTmp}] is deleted.'
                    print(self.tmpMsg)
                    self.MsgView.setText(self.tmpMsg)
            else:
                self.tmpMsg = f'Nothing is done!\nPlease select a proper time slot.'            
                self.MsgView.setText(self.tmpMsg)
                print(self.tmpMsg)
            # --- update plot
            imgItem = pg.ImageItem(ima)
            pltItem.addItem(imgItem)            
            imgItem.setLookupTable(self.lut_tagColor)
            imgItem.setLevels([20,255])
            self.Grade1TypButton.setChecked(True)   # restore grade1 as default
            self.addmodiTagButton.setChecked(True)  # restore add/modify as default
            self.tmpMsg = f'{TagName} num:{len(TagPos)} pos:{TagPos}\n'
            # self.MsgView.setText(self.tmpMsg)
            print(f'{TagName} num:{len(TagPos)} pos:{TagPos}\n')
    
    def p1RegionUpdate(self):
        self.p1region.setZValue(10)
        self.Tzoomi_local, self.Tzoomf_local = self.p1region.getRegion()  # local ti,tf
        self.Tzoomi = self.Tzoomi_local + self.ti  # global ti, tf
        self.Tzoomf = self.Tzoomf_local + self.ti
        # print(f'p1Region:{Tzoomi} ~ {Tzoomf}s')
        self.pExtraROI = (self.Tzoomf-self.Tzoomi)/5 # extra range to show p2
        # --- plot ROI in each button row
        for i in range(len(self.tagButtonSelect)):
            self.roiTagPos[i].setSize(((self.Tzoomf-self.Tzoomi)*10/self.ReSampleFact, self.imaY))
            self.roiTagPos[i].setPos((self.Tzoomi-self.ti_unzoom)*10/self.ReSampleFact, 0)
        splitfn = self.fnnow.split('/')
        self.p1txt.setText(self.p1text(splitfn))
        # print(f'current ROI region:({self.p1region.getRegion()[0]:.3f},{self.p1region.getRegion()[1]:.3f})  global/local Tzoomi:{self.Tzoomi:.3f}/{self.Tzoomi-self.ti_unzoom:.3f}(s)  Tzoomf:{self.Tzoomf:.3f}/{self.Tzoomf-self.ti_unzoom:.3f}(s)')
    
    # def p1addAItag(self, ti, tf, tagTyp):   # add tag by machine learning
    #     p1Region = []
    #     p1txt = []
    #     for k in range(4):
    #         p1Region.append(pgLinearRegionItem.LinearRegionItem(values=[ti, tf], pen=pg.mkPen('w',width=1), movable=False))
    #         self.p1.addItem(p1Region[k], ignoreBounds=True )
    #         p1txt.append(pg.TextItem(f'{tagTyp}', color=(255,255,50), anchor=(0.5,0.5)))
    #         self.p1.addItem(p1txt[k])
    #         p1txt[k].setPos((ti+tf)/2,1800)    
    
    def showvalue(self, value):
        print(value)    


    def readtagfile(self, tagfn, tagfns, tag_pos):
        labelapp = ''
        with open(tagfn,'r',newline='') as csvfile:
            rows = csv.reader(csvfile, delimiter='\t')
            newtagformat = 0
            if self.previoustagfn == '': self.previoustagfn = tagfn
            for row in rows:
                # if 'start,end,label' in row[0]: newtagformat = 1
                if ',' in row[0]: newtagformat = 1
                if newtagformat == 1: row = row[0].split(',')                                
                if not "Marker" in row[0] and not "Time" in row[0] and 'start' not in row[0] and '*' not in row[0]:
                    print('load... tagdata: ',row)
                    if len(row) == 3:
                        tag, ti, tf, labelapp = self.readTagtxt(row, tagfn, self.fnnow, labelapp)
                    else:
                        self.previoustagfn = ''
                        break
                    # for idx in self.TagTypIdx:
                    #     if tag == self.tagName[idx]:
                    #         tag_pos[idx].append((ti,tf))
                    #         print(f'loading previous tag data...\n{self.tagName[idx]}  {tag_pos[idx]}')
                    for idx, tagname in enumerate(self.tagName):
                        if tag == tagname:
                            tag_pos[idx].append((ti,tf))
                            print(f'loading previous tag data...\n{self.tagName[idx]}  {tag_pos[idx]}')
    
    def readTagtxt(self, row, tagfn, wavfn, labelapp):
        tag_final = ''
        # == recognize label APP
        if labelapp == '':
            if ':' in row[1]:
                labelapp = 'RX7'
                if len(tagfn) != len(wavfn):
                    labelapp = 'HF'
            else:
                labelapp = 'Audacity'
            print(f'labelling APP:{labelapp}')
        # == == find ti and tf as start and end of WAV truncation
        if labelapp == 'Audacity':
            ti = float(row[0])
            tf = float(row[1])
            tag = row[2]
            tag = tag.replace(' ','')
            if tag == '?':
                tag = 'Uncertain'
            # abbrTag = tag
        elif labelapp == 'RX7' or labelapp == 'HF':                        
            ti = float(row[1].split(':')[-1]) + 60*float(row[1].split(':')[-2]) + 60*60*float(row[1].split(':')[-3])
            if len(row) >= 3:
                tf = float(row[2].split(':')[-1]) + 60*float(row[2].split(':')[-2]) + 60*60*float(row[2].split(':')[-3])
            tag = row[0]
            # correct typo or improper tag name
            tag = tag.replace(' ','')
            if tag == '?':
                tag = 'Uncertain'
        # Translate tag named by other APP to HF's defined in TagNames
        if labelapp != 'HF':
            try:
                tag_final = self.config['TransTag'][tag]                                
                # print(f'original tag:{tag}  translated tag:{tag_final}', file=open("this.log", "a"))
                print(f'original tag:{tag}  translated tag:{tag_final}')
            except:
                try:
                    idx = self.tagName.index(tag)
                    tag_final = tag
                except:
                    print(f'<{tag}> no corresponding tag!  labelapp: {labelapp}')
                    # print(f'{tag}: no corresponding tag!  labelapp: {labelapp}', file=open("this.log", "a"))
        else:
            tag_final = tag
        return tag_final, ti, tf, labelapp

    def showAItag(self):
        with open('config.json', 'r', encoding='utf-8-sig') as reader:
            self.config = json.loads(reader.read())
        srcdir = self.config['Examining']['AItag_dir']
        suffix = self.config['Examining']['AItag_suffix']
        if (self.ShowAITagChkBox.isChecked()
                and not self.show_my_pretag
                and os.path.exists(srcdir)
                and self.playlist.media(self.playlist.currentIndex()).canonicalUrl().toString()[8:]):
            AI_tag_fns = [srcdir+'/'+fn for fn in os.listdir(srcdir) if fn.endswith(suffix) and
                        fn.startswith(os.path.basename(self.fnnow)[:-4]+'_')]
            for tagfn in AI_tag_fns:
                self.readtagfile(tagfn, AI_tag_fns, self.preTagPosData)
            self.plotTagsPos(self.preTagPosData)
        if self.ShowAITagChkBox.isChecked() and self.show_my_pretag:
            self.plotTagsPos(self.preTagPosData)
        elif not self.ShowAITagChkBox.isChecked():
            self.plotTagsPos(self.TagPosData)
    
    def p1text(self, splitfn):
        tfinfo = ''
        scoreTable = ''
        if self.config['Examining']['goTFscore']:
            labelcnt = (len(self.tfNGinfo[self.playlist.currentIndex()])-5)//2
            # print(f'                            labelcnt:{labelcnt}')
            self.scores = [ [self.tfNGinfo[self.playlist.currentIndex()][5+i], round(float(self.tfNGinfo[self.playlist.currentIndex()][5+labelcnt+i]),3)] for i in range(labelcnt)]
            self.scores.sort(key=lambda x:x[1], reverse=True)
            for i in range(3):
                scoreTable += f'{self.scores[i]}\n'
            tfinfo = f"TimeSlot: {self.tfNGinfo[self.playlist.currentIndex()][2]}~{self.tfNGinfo[self.playlist.currentIndex()][3]}(sec)\
                for training of {self.config['Examining']['keywd_trainProj']}\
                \nTrue Classification: {self.tfNGinfo[self.playlist.currentIndex()][4]}\n{scoreTable}"
        # if len(splitfn) < 3:
        #     fnlocation = f'{splitfn[-1]}'
        # else:
        #     fnlocation = f'{splitfn[-3]} / {splitfn[-2]}\n{splitfn[-1]}'
        fnlocation =  '/'.join(splitfn[min(3, len(splitfn)):-1])
        msg_tmp = (f'{fnlocation}\n{splitfn[-1]}\n'
                   f'Region of Interest: {self.Tzoomi:.3f}~{self.Tzoomf:.3f}(sec)'
                   f'Duration: {self.Tzoomf-self.Tzoomi:.3f}(sec)'
                   f'\nVisible Range: {self.p1Rangei/self.sr:.3f}~{self.p1Rangef/self.sr:.3f}'
                   f'   {self.duration:.3f}(sec)\n{tfinfo}')
        return msg_tmp
    
    def saveJudge(self, ans=None):
        # print(f'ans:{ans}')
        self.judgement = 'NA'
        # self.tfNGinfo    # Tag path(0), extracted wavfn(1), T_start(2), T_end(3), classification label(4), label list, score list
        if self.config['Examining']['goTFscore'] or self.config['Examining']['goCheckSeverity']:
            ansFN = f'./TFreview/TFreviewTable-{self.config["Examining"]["keywd_trainProj"]}.csv' \
                    if self.config['Examining']['goTFscore'] \
                    else f'./CheckSeverity/Judgement.csv' 
            if not os.path.exists(os.path.dirname(ansFN)):
                os.mkdir(os.path.dirname(ansFN))
            if ans == 0:
                self.judgement = 'yes' if self.config['Examining']['goCheckSeverity'] else 'model' 
            elif ans == 1:
                self.judgement = 'tag'
            elif ans == 2:
                self.judgement = 'No' if self.config['Examining']['goCheckSeverity'] else 'None'
            # === write result in csv
            if not os.path.exists(ansFN):
                with open(ansFN, 'w', newline='') as csvfile:            
                    writer = csv.writer(csvfile)
                    if self.config['Examining']['goTFscore']:
                        writer.writerow(['soundFile', 'tagfile','timeslot', 'Model', 'TrueClassification', 'label_1st', 'label_2nd', 'label_3rd', 'ExaminerID', 'NGcause'])
                    elif self.config['Examining']['goCheckSeverity']:
                        writer.writerow(['AudioFile', 'Acceptable', 'Examiner', 'TimeStamp', 'tag2_flag', 'Severity'])
            with open(ansFN, 'a', newline='', encoding = 'utf-8-sig') as csvfile:
                writer = csv.writer(csvfile)
                if self.config['Examining']['goTFscore']:
                    writer.writerow([self.fnnow, self.tfNGinfo[self.playlist.currentIndex()][0], \
                        f"{self.tfNGinfo[self.playlist.currentIndex()][2]}~{self.tfNGinfo[self.playlist.currentIndex()][3]}", \
                            self.config['Examining']['keywd_trainProj'], self.tfNGinfo[self.playlist.currentIndex()][4], \
                            self.scores[0], self.scores[1], self.scores[2], self.tagger, self.judgement])
                elif self.config['Examining']['goCheckSeverity']:
                    timestamp = time.strftime("%Y%m%d_%H%M%S", time.localtime())
                    writer.writerow([self.fnnow, self.judgement, self.tagger, timestamp, self.p1_extra_info[-1], self.p1_extra_info[0]])
            # === save zoom-in screen
            # if ans != 2:
            imgfn = (f'./TFreview/{self.judgement}NG_{self.tfNGinfo[self.playlist.currentIndex()][4]}_{self.scores[0][0][:-6]}-{os.path.basename(self.fnnow[:-4])}.jpg'
                    if self.config['Examining']['goTFscore']
                    else (f'./CheckSeverity/{os.path.basename(self.fnnow)[:-4]}-{self.tagger}-{timestamp}.png')) 
            if self.config['Examining']['goTFscore']:
                self.Spectzoom.setChecked(True)
                QtCore.QTimer.singleShot(500, partial(self.ScreenShot, imgfn, 'partial'))
            else:
                self.ScreenShot(imgfn, 'partial')
            self.playlist.next()

    def findtagfile(self, loadnew=True):
        self.config["tag2_suffix"] = (  ' ' if self.config["tag2_suffix"] == ''
                                        else self.config["tag2_suffix"])
        Humantag_dir = self.config['Examining']['Humantag_dir']
        if self.config['Examining']['goTFscore']:
            tagfns = [self.tfNGinfo[self.playlist.currentIndex()][0]]
            if not os.path.exists(tagfns[0]) and os.path.exists('G:\\我的雲端硬碟\\臨床部\\[臨床部]-[FA1胸音計畫]'):
                tagfns[0] = tagfns[0].replace('G:\\My Drive','G:\\我的雲端硬碟\\臨床部\\[臨床部]-[FA1胸音計畫]')
            print(f"\noriginal tagfn: {os.path.basename(os.path.split(tagfns[0])[-2])}/{os.path.split(tagfns[0])[-1]}")
        elif os.path.exists(Humantag_dir):  # read human tag package to benchmark AI and Human
            tagfns = [Humantag_dir+'/'+fn for fn in os.listdir(Humantag_dir) \
                if (fn.endswith('.txt') and 
                    (fn.startswith(os.path.basename(self.fnnow)[:-4]+'_') or
                    fn[:-4] == os.path.basename(self.fnnow)[:-4]))]
        else:
            tagfns = [os.path.dirname(self.fnnow)+'/'+fn for fn in os.listdir(os.path.dirname(self.fnnow)) \
                if (self.config["tag2_suffix"] not in fn and fn.endswith('.txt') and 
                    (fn.startswith(os.path.basename(self.fnnow)[:-4]+'_') or
                    fn[:-4] == os.path.basename(self.fnnow)[:-4]))]
            if len(tagfns):
                self.previoustagfn = tagfns[0]
        if loadnew:
            # [print(f'loading tag fn: {tagfn}') for tagfn in tagfns]
            for tagfn in tagfns:
                print(f'loading tag fn: {tagfn}')
                if os.path.exists(tagfn):
                    self.readtagfile(tagfn, tagfns, self.TagPosData)

    def saveTag(self, cleartag=False):
        if self.previoustagfn == '':
            self.findtagfile(loadnew=False)
        fnTimeStamp = time.strftime("%Y%m%d_%H%M%S", time.localtime())
        dataTimeStamp = time.strftime("%Y-%m-%d %H:%M:%S", time.strptime(fnTimeStamp,"%Y%m%d_%H%M%S"))
        TimeStamp_relabel = time.strftime("%Y%m%d", time.localtime())
        fncsv_table = f'Database_LabelledSound_{socket.getfqdn(socket.gethostname())}.csv'
        # === define filename of tag file
        if len(self.previoustagfn):
            if self.config['Examining']['goRelabel']:
                fncsv_each = f'{os.path.dirname(self.previoustagfn)}/relabel_{TimeStamp_relabel}_{self.tagger}/'
                if not os.path.exists(fncsv_each):
                    os.makedirs(fncsv_each)
                fncsv_each += f'{os.path.basename(self.previoustagfn)[:-4]}_{TimeStamp_relabel}_{self.tagger}.txt'
            else:
                fncsv_each = self.previoustagfn
        else:
            fncsv_each = f'{self.fnnow[:-4]}_{fnTimeStamp}_{self.tagger}.txt'
        # === export a table of labelled files
        if not os.path.exists(fncsv_table):     #
            with open(fncsv_table, 'a', newline='', encoding='utf-8-sig') as csvfile:
                writer = csv.writer(csvfile)
                row = ['SoundFile', 'Device', 'Project Name', 'Tagging Time', 'Tagger', 'TagFile']
                [row.append(self.tagName[j]+'_count') for j in range(len(self.tagName))]
                writer.writerow(row)
        else: # 1)rewrite header and previous counter data
            with open(fncsv_table, 'r', newline='', encoding='utf-8-sig') as csvfile:
                # = read current table data
                rows = csv.reader(csvfile)
                row_bak = []
                for row in rows:
                    if row[0] != 'SoundFile' and row[5] != os.path.basename(self.previoustagfn):
                        row_bak.append(row)
                with open(fncsv_table, 'w', newline='', encoding='utf-8-sig') as csvfile:
                    # === write header
                    row = ['SoundFile', 'Device', 'Project Name', 'Tagging Time', 'Tagger', 'TagFile']
                    [row.append(self.tagName[j]+'_count') for j in range(len(self.tagName))]
                    writer = csv.writer(csvfile)
                    writer.writerow(row)
                    # === restore counter table except data in prevous tag file
                    [writer.writerow(row_bak[j]) for j in range(len(row_bak)) ]
        # === update counter table
        with open(fncsv_table, 'a', newline='', encoding = 'utf-8-sig') as csvfile:
            writer = csv.writer(csvfile)
            row = [os.path.basename(self.fnnow), self.DeviceName, self.ProjName, dataTimeStamp, self.tagger, os.path.basename(fncsv_each)]
            [row.append(len(self.TagPosData[j])) for j in range(len(self.TagPosData))]
            for tagPos in self.TagPosData:  # write data only if tag data exists
                if len(tagPos):
                    writer.writerow(row)
                    break
        # === export tag file
        k = 0                
        row = []
        for tag in self.TagPosData:
            if len(tag):
                [row.append([self.tagName[k], hhmmss(tag[j][0], outType=1), hhmmss(tag[j][1], outType=1)]) for j in range(len(tag))]
            if cleartag:
                tag.clear()
            k += 1
        if os.path.exists(fncsv_each) or len(row) or len(self.myComment):
            row.sort(key=lambda x:x[1])
            with open(fncsv_each, 'w', newline='') as csvfile:
                writer = csv.writer(csvfile, delimiter='\t')
                [writer.writerow(row[j]) for j in range(len(row))]                        
    
    def ScreenShot(self, fn=None, ROI='whole'):
        if fn == None:
            ScrShotTimestamp = time.strftime("%Y%m%d_%H%M%S", time.localtime())
            ScrShotfn = f'{self.fnnow[:-4]}_{ScrShotTimestamp}_{self.tagger}.png'
        else:
            ScrShotfn = fn
        screen = QtWidgets.QApplication.primaryScreen()
        if ROI == 'whole':
            screenshot = screen.grabWindow(self.centralWidget.winId())
        elif ROI == 'selfp1':
            screenshot = screen.grabWindow(self.win.winId(),
                                           height=self.p1.geometry().height()+10)
        else:
            screenshot = screen.grabWindow(self.centralWidget.winId(),
                                           width=(self.MainLayout.geometry().width()
                                                  -self.playerLayout.geometry().width()-20), 
                                           height=(self.MainLayout.geometry().height()
                                                  -self.MsgView.height()))
        if not os.path.exists(os.path.dirname(ScrShotfn)):
            os.makedirs(os.path.dirname(ScrShotfn))
        screenshot.save(ScrShotfn, 'png')
        print(f'{ScrShotfn} is taken!')
    
    def SavSpectro(self):
        ScrShotTimestamp = time.strftime("%Y%m%d_%H%M%S", time.localtime())            
        imgfn = f'./specgram-{os.path.basename(self.fnnow)[:-4]}_{ScrShotTimestamp}_{self.tagger}.png'
        if self.isvip:
            # --- save spectrogram image
            # r = plt.gcf().canvas.get_renderer()
            imgg, _, _, _ = self.im_spectro.make_image('AGG',magnification=1.0, unsampled=True)
            # imgfn = f'{os.path.split(self.fnnow)[0]}/{os.path.basename(self.fnnow)[:-3]}png'            
            plt.imsave(f'{imgfn}',imgg)
            self.savSpectro = False
            print(f'\t{imgfn} is saved!')
        else:
            self.ScreenShot(fn=imgfn, ROI='selfp1')

    def clrPlayList(self, opt):
        # ansList = []
        # with open(f'./TFreview/TFreviewTable-{self.config["Examining"]["keywd_trainProj"]}.csv', 'r', newline='', encoding = 'utf-8-sig') as ansListcsv:
        #     rows = csv.reader(ansListcsv)                
        #     for row in rows:
        #         if row[0] == 'soundFile':
        #             continue
        #         ansList.append(row[0])        
        if opt: # remove current media
            idx = self.playlist.currentIndex()
            self.playlist.removeMedia(idx)
            print(f'currentIdx/Count:{self.playlist.currentIndex()}/{self.playlist.mediaCount()}')
            if (    self.playlist.currentIndex() == self.playlist.mediaCount()-1
                    and self.playlist.mediaCount()):
                self.playlist_position_changed(idx-1)
            elif self.playlist.mediaCount():
                self.playlist_position_changed(idx)
        else:   # clear whole list
            self.playlist.clear()
        self.model.layoutChanged.emit()

    def SavClip(self):
        if not os.path.exists(f'./clips/'):
            os.makedirs(f'./clips/')
        if self.SpectzoomROI.isChecked():
            if self.isvip:
                outfn = f"./clips/{os.path.basename(self.fnnow)[:-4]}.wav"
                # outfn = self.fnnow
                sf.write(outfn, self.sndData[round(self.Tzoomi*self.sr):round(self.Tzoomf*self.sr)], self.sr, 'PCM_24')
            else:
                outfn = f"./clips/{os.path.basename(self.fnnow)[:-4]}-{self.Tzoomi:.1f}~{self.Tzoomf:.1f}s.wav"
                sf.write(outfn, self.sndData[round(self.Tzoomi*self.sr):round(self.Tzoomf*self.sr)], self.sr, 'PCM_24')
        else:
            outfn = f"./clips/{os.path.basename(self.fnnow)[:-4]}-{self.ti:.1f}~{self.tf:.1f}s.wav"
            sf.write(outfn, self.sndData[self.idxi:self.idxf], self.sr, 'PCM_24')
        print(f'\n\tsave clip:{outfn}\n')

    def debugMsg(self,msg):
        print(f'\n\t debug: {msg}\n')

    def Sel_Job(self):
        msg = QtWidgets.QMessageBox()
        msg.setIcon(QtWidgets.QMessageBox.Question)
        msg.addButton('Review',QtWidgets.QMessageBox.YesRole)
        msg.addButton('Labeling',QtWidgets.QMessageBox.NoRole)
        # msg.resize(300,2000)                  
        msg.setText(f'Review Tag Only or Label Sound?\t\t')   
        res = msg.exec_()
        # print(f'\n\t res of sel job:{res}\n')
        if res == 0:
            print(f'\nReview Tag Mode!!!!')
            self.ReviewOnlyChkBox.setChecked(True)
        else:
            print(f'\nLabeling Mode!!!!')
            self.ReviewOnlyChkBox.setChecked(False)

    def comment(self, arg):
        msg = f'*{self.MsgView.toPlainText()}'
        if arg:
            print(f'my comment:{msg}')
            self.myComment = msg
        else:
            self.mytag.append((msg, self.Tzoomi, self.Tzoomf))
            print(f'my tag:{self.mytag}')

    def keyPressEvent(self, event):
        if event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Up:
            self.playlist.previous()
        if event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Down:
            self.playlist.next()
        if event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Equal:
            self.player.play()
        if event.modifiers() == Qt.ControlModifier and event.key() == Qt.Key_Minus:
            self.player.pause()
    

    # def testshow(self):
    #     self.datesel = DateSelector()
    #     self.datesel.show()
    
    # def p5roiClicked(self):
    #     tagPos = self.RhonchiPos
    #     imgItem = self.img5
    #     ima = self.ima5
    #     roi = self.p5roi
    #     tagName = 'Rhonchi'
    #     print(f'here is {self.p5roi.pos()*10}')
    #     self.RhonchiPos.append(self.p1region.getRegion())
    #     print(f'Rhonchi[{len(self.RhonchiPos)}] is added.')
    #     self.p5.removeItem(self.img5)
    #     for i in range(len(self.RhonchiPos)):
    #         self.ima5[:,int(self.RhonchiPos[i][0]*10):int(self.RhonchiPos[i][1]*10)] = 200
    #     self.img5 = pg.ImageItem(self.ima5)
    #     self.p5.addItem(self.img5)
    #     print(f'Rhonchi num:{len(self.RhonchiPos)} pos:{self.RhonchiPos}')

# class testThread(threading.Thread):
#     def __init__(self, mywin):
#         super().__init__(daemon=True, name='XXX')
#         self.mywin = mywin
#         self.daemon = True

#     def run(self):
#         self.wavData = self.mywin.sndData[self.mywin.idxi:self.mywin.idxf]
#         self.getRMSEnergy(self.wavData)
#             # self.mywin.run_calculation = False

#     def getRMSEnergy(self, wavdata):        
#         S = np.abs(librosa.stft(wavdata, win_length=1024))
#         rms = librosa.feature.rms(S=S)
#         plt.figure(figsize=(12,5))
#         plt.subplot(2, 1, 1)
#         plt.semilogy(rms.T, label='RMS Energy')
#         plt.xticks([])
#         plt.xlim([0, rms.shape[-1]])
#         plt.legend()
#         plt.subplot(2, 1, 2)
#         librosa.display.specshow(librosa.amplitude_to_db(S, ref=np.max),
#                                 y_axis='linear', x_axis='time')
#         plt.title('log Power spectrogram')
#         plt.tight_layout()
#         # plt.show()
#         # plt.close()        
#         plt.savefig('test.png')


if __name__ == '__main__':
    QtWidgets.QApplication.setAttribute(Qt.AA_DisableHighDpiScaling) 
    app = QtWidgets.QApplication([])
    app.setApplicationName("Heroic-Faith SoundLabeler Ver.202000819_01")

    app.setStyle("Fusion")

    # Fusion dark palette from https://gist.github.com/QuantumCD/6245215.
    palette = QtGui.QPalette()
    palette.setColor(QtGui.QPalette.Window, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.WindowText, Qt.white)
    palette.setColor(QtGui.QPalette.Base, QtGui.QColor(25, 25, 25))
    palette.setColor(QtGui.QPalette.AlternateBase, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.ToolTipBase, Qt.white)
    palette.setColor(QtGui.QPalette.ToolTipText, Qt.white)
    palette.setColor(QtGui.QPalette.Text, Qt.white)
    palette.setColor(QtGui.QPalette.Button, QtGui.QColor(53, 53, 53))
    palette.setColor(QtGui.QPalette.ButtonText, Qt.white)
    palette.setColor(QtGui.QPalette.BrightText, Qt.red)
    palette.setColor(QtGui.QPalette.Link, QtGui.QColor(42, 130, 218))
    palette.setColor(QtGui.QPalette.Highlight, QtGui.QColor(42, 130, 218))
    palette.setColor(QtGui.QPalette.HighlightedText, Qt.black)
    app.setPalette(palette)
    app.setStyleSheet("QToolTip { color: #ffffff; background-color: #2a82da; border: 1px solid white; }")

    # print(f'here  {app.font()}')
    # app.font().setPointSize(10)

    window = MainWindow()
    app.exec_()
