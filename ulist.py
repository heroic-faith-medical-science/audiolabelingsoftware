import socket
from PyQt5 import QtWidgets
import os
import ntplib
from datetime import datetime, timezone
import hashlib

class userid():
    def __init__(self, sbwin):
        self.sbwin = sbwin
        self.userlist = [ 
                "superUser"
                ]

        self.VIP = ["superUser"]

    def updateTagger(self):
    # key in a validated user id
        vip_pc = (self.sbwin.config['Envelope']['VIP_PC']
                    if 'VIP_PC' in self.sbwin.config['Envelope'].keys()
                    else False)
        if (socket.getfqdn(socket.gethostname()) == 'LAPTOP-14BS7HLE' or 
                socket.getfqdn(socket.gethostname()) == 'LAPTOP-66P75AFA' or 
                socket.getfqdn(socket.gethostname()) == 'DESKTOP-FPP7D9Q') and \
                vip_pc :
            self.sbwin.taggerNameInput.setText('cykuo')
        self.sbwin.taggerNameInput.blockSignals(True)
        try:
            userids = self.sbwin.config["userid"]
        except:
            userids = self.sbwin.userid
        if self.sbwin.taggerNameInput.text() in userids:
            self.sbwin.tagger = self.sbwin.taggerNameInput.text()
        else:
            errchance = 3
            while not self.sbwin.taggerNameInput.text() in userids:                
                msg = QtWidgets.QMessageBox()
                msg.setIcon(QtWidgets.QMessageBox.Warning)
                msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
                msg.resize(100,1000)
                msg.setText(f'\n\n\n\n\n')
                if errchance > 0:                    
                    msg.setText(f'Plz Key in a Valid ID! ({errchance} chances left)')
                    namein = QtWidgets.QLineEdit(msg)
                    namein.move(0,60)
                    namein.setFixedWidth(200)     
                    msg.exec_()
                    self.sbwin.taggerNameInput.setText(namein.text())
                    if namein.text() in userids:
                        self.sbwin.tagger = self.sbwin.taggerNameInput.text()
                else:
                    msg.setText("Plz contact info@heroic-faith.com !")
                    msg.exec_()
                    os._exit(0)
                errchance -= 1
        self.sbwin.taggerNameInput.blockSignals(False)
        self.sbwin.isvip = True if self.sbwin.tagger == 'cykuo' else False

    def checkpasswd(self):
        # wifissid = ''
        # try:
        #     iface = pywifi.PyWiFi().interfaces()[0]
        #     iface.scan()
        #     time.sleep(1.5)        
        #     if len(iface.scan_results()):
        #         wifissid = iface.scan_results()[0].ssid
        # except:
        #     wifissid = ''
        # # print(wifissid)
        # if not wifissid == 'HeroicFaith_5G' and not wifissid == 'HeroicFaith':        
        if not self.sbwin.tagger in self.sbwin.ids.VIP:
            client = ntplib.NTPClient()
            try:
                response = client.request('uk.pool.ntp.org', version=3)
            except:
                try:
                    response = client.request('tick.stdtime.gov.tw')
                except:
                    try:
                        response = client.request('pool.ntp.org')
                    except:
                        msg = QtWidgets.QMessageBox()
                        msg.setFixedWidth(1000)
                        msg.setIcon(QtWidgets.QMessageBox.Warning)                
                        msg.setStandardButtons(QtWidgets.QMessageBox.Ok)
                        msg.setText('Network Connection Issue!')
                        msg.exec_()
                        os._exit(0)
            ans = []
            for i in range(26):
                anstmp = datetime.utcfromtimestamp(response.tx_time+60*60*(i-2)).isoformat(' ')[:13]+self.sbwin.tagger
                ans.append(hashlib.md5(anstmp.encode('utf-8-sig')).hexdigest())
            pw = ''
            # print(ans)
            errchance = 3
            while not pw in ans:
                msg = QtWidgets.QMessageBox()
                msg.setFixedWidth(1000)
                msg.setIcon(QtWidgets.QMessageBox.Warning)                
                msg.setStandardButtons(QtWidgets.QMessageBox.Ok) 
                if errchance > 0:                         
                    msg.setText(f'Plz Key in Password! ({errchance} chances left)')
                    keyin = QtWidgets.QLineEdit(msg)
                    keyin.move(0,60)
                    keyin.setFixedWidth(200)     
                    msg.exec_()
                    # print(keyin.text())
                    pw = keyin.text()
                else:
                    msg.setText("Plz contact info@heroic-faith.com !")
                    msg.exec_()
                    os._exit(0)
                errchance -= 1

